<?php

return [
    'invaildPromo'                  => 'Promo Code is invaild',
    'saveToCart'                    => 'Saved in cart',
    'priceLess'                     => 'can not enter price less than 0',
    'oldDate'                       => 'can not enter old date',
    'wrongDate'                     => 'wrong in dates range',
    'sameDate'                      => 'Can not repeat the same date more than once',
    'blocked'                       => 'This Acount was blocked by admin',
    'needActive'                    => 'This Acount Need Activation ',
    'commentSent'                   => 'Comment send Success ',
    'inCart'                        => 'This Service Is In Cart ',
    'Added'                         => 'Added Success',
    'ordered'                       => 'Order Added Success ',
    'deleted'                       => 'Deleted success ',
    'sendSuccess'                   => 'Message send ',
    'passwordUpdated'               => 'password updated success',
    'wrongPassword'                 => 'Old password is wrong',
    'wrongCode'                     => 'Wrong Code',
    'wrongPhone'                    => 'this phone has no account ',
    'activeCode'                    => 'active code is : ',
    'wrongLogin'                    => 'password or Phone incorrect ',
    'employBusy'                    => 'is busy on this time',
    'Updated'                       => 'updated ',
    'ratedSuccess'                  => 'rated success',
    'activeSuccess'                 => 'Account successfully activated',
    'providerActiveSuccess'         => 'Mobile number confirmed - please contact the administration to activate  your account for users',
    'successfullyRegistered'        => 'successfully registered',
    'loggedInSuccessfully'          => 'Logged in successfully',
    'loggedOutSuccessfully'         => 'Logged out successfully',
    'codeSentToPhoneSuccessfully'   => 'Code sent to phone successfully',
    'usedPhone'                     => 'This phone is used before',
    'save'                          => 'Saved',
    'send'                          => 'Success Sent',
    'delete'                        => 'Success Deleted',
    'needAdminAgree'                => 'You have added the maximum number of services. To add another service a request will be sent to the administration',
    'takenDate'                     => 'This date is not avaliable',
    'newOrder'                      => 'New order',
    'agreeOrder'                    => 'Is agreed',
    'refusedOrder'                  => 'Is refused',
    'deleteOrderByClient'           => 'Is canceled by client',
    'deleteOrderByProvider'         => 'Is canceled by Provider',
    'finishOrder'                   => 'Is finished',
    'enterCode'                     => 'Please Enter Code',
    'rightCode'                     => 'activation code is ok',
    'codeNotSent'                   => 'No activation code has been received',
    'resendCode'                    => 'Resend activation code',
    'confirm'                       => 'Confirm',
    'needLogin'                     => 'you should log in first',
    'mustUser'                      => 'you should be user not provider',
    'usedPriceDates'                => 'used dates, delete old price before',
    'needLoginAsProvider'           => 'Need log in as a provider',
    'reservationStop'               => 'This service has been suspended for the day by the service provider',
    'wrongSection'                  => 'Error in main section ID',
    'unMatchesSectionsTypes'        => 'Unmatched sections in type',
    'needPosition'                  => 'The location on the map must be specified for this type of service',
    'needCities'                    => 'Delivery locations for this type of service must be specified',
    'wrongCity'                     => 'City ID error',
    'orderDelayed'                  => 'Request has been delayed',
    'delayedOrder'                  => 'delayed Order',
    'diffArraysCount'               => 'The number of elements in the two arrays should be equal',
    '' => '',
    '' => '',
    '' => '',
    '' => '',

    'nationalIdRiF'                 => 'National Id required',
    'nationalCopyRiF'               => 'National Copy required',
    'commercialNumRiF'              => 'Commercial license Num required',
    'commercialCopyRiF'             => 'Commercial license Copy required',
    'companyNameRiF'                => 'Company Name required',
    'bankNumRiF'                    => 'Bank Number required',
    'bankImageRiF'                  => 'Bank Image required',

];