<table class="table table-bordered modal-table">
    <thead>
        <th>الحجوزات الجديدة</th>
        <th>الحجوزات الحالية</th>
        <th>الحجوزات الملغاة من المقدم</th>
        <th>الحجوزات الملغاة من العميل</th>
        <th>الحجوزات المنتهية</th>
    </thead>
    <tbody>
        <td>{{$new}}</td>
        <td>{{$current}}</td>
        <td>{{$providerCancel}}</td>
        <td>{{$clientCancel}}</td>
        <td>{{$finish}}</td>
    </tbody>
</table>