<table id="datatable" class="table table-bordered table-responsives">
    <thead>
    <tr>
        <th>الصورة </th>
        <th>الأسم </th>
        <th>السعر</th>
        <th>مقدم الخدمة</th>
        <th>المدينة</th>
        <th>تاريخ التسجيل</th>
    </tr>
    </thead>
    <tbody class="text-center">
    @foreach($data as $item)
        <tr>
            <td><img src="{{url(''.$item->Images->first()->image)}}" alt="user-img" width="100px" height="75px" title="Mat Helme" class="img-thumbnail img-responsive" style="height: 70px !important;"></td>
            <td>{{$item->title}}</td>
            <td>{{$item->price}}</td>
            <td>{{$item->User->name}}</td>
            <td>{{$item->City->name}}</td>
            <td>{{$item->created_at->diffForHumans()}}</td>
        </tr>
    @endforeach
    </tbody>
</table>