@extends('site/layouts/master') 

@section('content')

    <!-- Start Login Form -->

    <section>
            <div class="login wrapper">
                <div class="container">
                    <form class="form" action="{{url('site/forget-password')}}" method="post" id="forgetForm">
                            {{csrf_field()}}
                        <div class="form-txt">
                            <h2 class="wow fadeInDown" data-wow-delay="0.2s">@lang('site.forgetPassword')</h2>
                            <p class="wow fadeInUp" data-wow-delay="0.4s">
                                @lang('site.enterPhoneGetCode')
                            </p>
                        </div>
    
                        <div class="row">
                            <div class="col-sm-4 col-5">
                                <div class="form-group wow flipInX" data-wow-delay="0.6s">
                                        <select id="single" class="select2-single select2-hidden-accessible form-control" name="country_id">
                                            @foreach($countries as $country)
                                                <option value={{$country->id}} {{$country->code == '966'? 'selected' : ''}}>{{$country->code}}</option>
                                            @endforeach
                                            
                                        </select>
                                    <i class="fas fa-sort-down"></i>
                                </div>
                            </div>
    
                            <div class="col-sm-8 col-7">
                                <div class="form-group wow flipInX" data-wow-delay="0.6s">
                                        <input type="tel" class="form-control" placeholder="@lang('site.phoneNumber') 50xxxxxxx" name="phone" />
                                </div>
                            </div>
                        </div>
    
                        <button type="submit" class="login-btn site-btn wow fadeInUp" id="btnSubmitForget" data-wow-delay="1s">@lang('site.send')</button>
                    </form>
                </div>
            </div>
        </section>
    
        <!-- End Login Form -->

@endsection



@section('script')

<script>
        $(document).ready(function () {
            $("#btnSubmitForget").click(function (event) {
    
                //stop submit the form, we will post it manually.
                event.preventDefault();
    
                // Get form
                var form = $('#forgetForm')[0];
    
                // Create an FormData object
                var data = new FormData(form);
    
                var url = $('#forgetForm').attr('action');

                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {

                        if(data.status == 1){
                            window.location.replace("verfication");
                        }else if(data.status == 0){
                            toastr.error(data.message);
                        }
                        console.log("SUCCESS : ", data);

                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                });
    
            });
        });
    </script>

@endsection