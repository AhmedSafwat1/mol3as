@extends('site/layouts/master')
@section('style')
    <style>
        .hidden{
            display: none !important;
        }
    </style>
    @endsection
@section('content')
    <!-- Start user-profile -->

    <section>
        <div class="user-profile N-notify wrapper">
            <div class="container">
                <h2 class="wow fadeInDown">@lang('site.notifications')</h2>
                <div class="N-noty-all">
                    @foreach($notifications as $notify)
                        @if(isset($notify->order_id) && $notify->type == 'order' && isset($notify->from_id) && isset($notify->Order->Service->title_ar))
                            <!-- orders notifications -->
                            <div class="favourites wow fadeInUp NotificationParent{{$notify->id}}" data-wow-delay="0.2s">
                                <div class="fav-block">
                                    <div class="buttons-serv">
                                        <p>@lang('site.did') <b> {{$notify->From->name}} </b> @lang('site.doReserve') <b> {{$notify->Order->Service->title_ar}}</b> </p>
                                        <p>@lang('site.bookingDate') : <b>{{date("d-m-Y", strtotime($notify->Order->start_date))}} </b></p>
                                        <p>@lang('site.bookingEndDate') : <b>{{date("d-m-Y", strtotime($notify->Order->end_date))}} </b></p>
                                        <p>@lang('site.userPhone') : <b> {{$notify->From->phone}} </b></p>
                                    </div>
                                </div>

                                <div class="fav-serv">
                                    <div class="buttons-serv">

                                            <p class="cancel accepted{{$notify->order_id}} {{$notify->Order->status != 1? 'hidden':''}}">
                                                @lang('site.accepted')
                                            </p>

                                            <p class="cancel refused{{$notify->order_id}} {{$notify->Order->status != 2? 'hidden':''}}">
                                                @lang('site.refused')
                                            </p>

                                            <p class="cancel userCancel{{$notify->order_id}} {{$notify->Order->status != 3? 'hidden':''}}">
                                                @lang('site.userCancel')
                                            </p>

                                            <p class="cancel finished{{$notify->order_id}} {{$notify->Order->status != 5? 'hidden':''}}">
                                                @lang('site.finished')
                                            </p>

                                            <p class="site-btn login-btn cancel openAnswerModal pending{{$notify->order_id}} {{$notify->Order->status != 0? 'hidden':''}}" data-id="{{$notify->Order->id}}" data-toggle="modal" data-target="#answerModal">
                                                @lang('site.theAnswer')
                                            </p>
                                    </div>
                                </div>

                                <div class="close-btn delete{{$notify->order_id}} {{$notify->Order->status == 0? 'hidden':''}}">
                                    <i class="fas fa-times openDeleteNotiModal" data-toggle="modal" data-id="{{$notify->id}}" data-target="#removeNoti"></i>
                                </div>

                            </div>
                        @elseif($notify->type == 'replay')
                            <!-- admin replay and pan notifications -->
                            <div class="favourites wow fadeInUp NotificationParent{{$notify->id}}" data-wow-delay="0.2s">
                                <div class="fav-block">
                                    <div class="buttons-serv">
                                        <p>{{$lang == 'en' ? $notify->message_en : $notify->message_ar }}</p>
                                    </div>
                                </div>

                                <div class="close-btn">
                                    <i class="fas fa-times replayNotification" data-id="{{$notify->id}}"></i>
                                </div>

                            </div>
                        @endif
                    @endforeach


                    @if(count($notifications) > 0)
                        <div class="reservation-btn wow fadeInUp" data-wow-delay="0.6s">
                            <p class="site-btn login-btn removeAll" data-toggle="modal" data-target="#remove-all">@lang('site.deleteAll')</p>

                            <!-- Start Remove Modal -->
                            <div class="modal fade" id="remove-all">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <div class="modal-body like">
                                            <p>
                                                @lang('site.confirmDeleteAllNotification')
                                            </p>
                                            <div class="like-btn">
                                                <button class="site-btn" data-dismiss="modal">@lang('site.cancel')</button>
                                                <button class="site-btn login-btn accept-remove-all confirmRemoveAll" data-dismiss="modal">@lang('site.confirm')</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <p class="no-noty-alert">@lang('site.noNotifications')</p>
            </div>
        </div>

        <!-- Start answer Modal -->
        <div class="modal fade" id="answerModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-body like apply">
                        <p>@lang('site.answerOrder')</p>
                        <p class="info">
                            @lang('site.afterAccept')
                        </p>
                        <div class="like-btn">
                            <button class="site-btn" data-dismiss="modal" id="refuseOrder">@lang('site.refuse')</button>
                            <button class="site-btn login-btn" data-dismiss="modal" id="acceptOrder">@lang('site.accept')</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Start Remove Modal -->
        <div class="modal fade" id="removeNoti">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-body like">
                        <p>
                            @lang('site.confirmDeleteNotification')
                        </p>
                        <div class="like-btn">
                            <button class="site-btn" data-dismiss="modal">@lang('site.cancel')</button>
                            <button class="site-btn login-btn accept-remove" id="confirmDeleteNoti" data-dismiss="modal">@lang('site.confirm')</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>

    <!-- End user-profile -->
@endsection
@section('script')
    <script>
        // answer order notification and change status
        $(document).on('click','.openAnswerModal',function(){
            // clear old val from local storage
            localStorage.removeItem('orderId');

            // put order id in local storage
            var orderId = $(this).data('id');
            localStorage.setItem('orderId', orderId);
        });
        $(document).on('click','#refuseOrder',function(){

            // get id from local storage
            var orderId = localStorage.getItem('orderId', orderId);

            // delete from server side
            $.get('/refuse-order/' + orderId,function(data){
                if($('.refused'+ orderId).hasClass('hidden')){
                    $('.refused'+ orderId).removeClass('hidden')
                }
                if(!$('.accepted'+ orderId).hasClass('hidden')){
                    $('.accepted'+ orderId).addClass('hidden')
                }
                if(!$('.pending'+ orderId).hasClass('hidden')){
                    $('.pending'+ orderId).addClass('hidden')
                }
                if($('.delete'+ orderId).hasClass('hidden')){
                    $('.delete'+ orderId).removeClass('hidden')
                }
                console.log(data);
            });

            // clear local storage
            localStorage.removeItem('orderId');


        });
        $(document).on('click','#acceptOrder',function(){

            // get id from local storage
            var orderId = localStorage.getItem('orderId', orderId);

            // delete from server side
            $.get('/accept-order/' + orderId,function(data){
                if($('.accepted'+ orderId).hasClass('hidden')){
                    $('.accepted'+ orderId).removeClass('hidden')
                }
                if(!$('.refused'+ orderId).hasClass('hidden')){
                    $('.refused'+ orderId).addClass('hidden')
                }
                if(!$('.pending'+ orderId).hasClass('hidden')){
                    $('.pending'+ orderId).addClass('hidden')
                }
                if($('.delete'+ orderId).hasClass('hidden')){
                    $('.delete'+ orderId).removeClass('hidden')
                }
                console.log(data);
            });


            // clear local storage
            localStorage.removeItem('orderId');


        });
    </script>
    <script>
        // delete one notification
        $(document).on('click','.openDeleteNotiModal',function(){
            // clear old val from local storage
            localStorage.removeItem('notiId');

            // put order id in local storage
            var notiId = $(this).data('id');
            localStorage.setItem('notiId', notiId);
        });
        $(document).on('click','#confirmDeleteNoti',function(){

            // get id from local storage
            var notiId = localStorage.getItem('notiId', notiId);

            // delete from server side
            $.get('/delete-notification/' + notiId,function(data){

                // hide parent div
                $('.NotificationParent' + notiId ).remove();

                // hide delete all button if no notifications after delete
                if(data.count <= 0){
                    $('.removeAll').addClass('hidden');
                }
                console.log(data);

            });

            // clear local storage
            localStorage.removeItem('notiId');


        });

        // delete replay notification
        $(document).on('click','.replayNotification',function(){

            // get id from local storage
            var notiId = $(this).data('id');

            //delete from server side
            $.get('/delete-notification/' + notiId,function(data){

                // hide parent div
                $('.NotificationParent' + notiId ).remove();

                console.log(data);

            });
        });
    </script>
    <script>
        // delete all notifications
        $(document).on('click','.confirmRemoveAll',function(){
            $.get('/remove_all_notifications',function(data){
               console.log(data);
            });
        })
    </script>
@endsection
