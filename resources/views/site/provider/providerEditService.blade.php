@extends('site/layouts/master')
@section('style')

<style>
    .H-selector {
        position: absolute;
        right: 0;
        bottom: 0;
        z-index: 2;
    }

    .H-selector+.form-group .form-control {
        padding-right: 85px;
    }

    .select2-container .select2-selection--single {
        position: absolute;
        height: 40px;
        bottom: -10px !important;
    }
</style>
@endsection

@section('content')

<!-- Start Login Form -->

<section>
    <div class="add-serv wrapper">
        <div class="container">
            <form class="row form" action="{{url('provider/edit-service')}}" enctype="multipart/form-data" method="post" id="editForm">
                {{csrf_field()}}
                <input type="hidden" value="{{$service->id}}" name="service_id">
                <div class="col-md-4 col-sm-12">
                    <h2 class="wow fadeInUp">@lang('site.editService')</h2>
{{--                    <h6 class="wow fadeInUp">{{$lang =='en'? 'max : 6 images':'الحد الاقصي 6 صور'}}</h6>--}}
                    <div class="office-imgs wow fadeInDown">
                        <div class="default-img">

                            @foreach($service->Images as $image)
                                <div class="oldImages">
                                    <a href="{{url('/')}}/{{$image->image}}">
                                        <img src="{{url('/')}}/{{$image->image}}">
                                        <button class="close">
                                            <i class="fas fa-minus-square deleteImage" data-id="{{$image->id}}"></i>
                                        </button>
                                    </a>

                                </div>
                            @endforeach

                            <div class="img-block">

                                <!-- display old galleries from server -->
                                {{--<div class="oldGallery">--}}

                                {{--</div>--}}

                                <!-- push new galleries -->
                                <div class="gallery">

                                </div>


                                <div class="upload-img">
                                    <input type="file" multiple accept="image/*" id="gallery-photo-add">
                                    <i class="fas fa-image"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-8 col-sm-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group wow fadeInUp">
                                <label>
                                        @lang('site.nameInArabic')
                                    </label>
                                <input type="text" class="form-control" placeholder="@lang('site.nameInArabic')" value="{{$service->title_ar}}" name="title_ar" />
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group wow fadeInUp">
                                <label>
                                        @lang('site.nameInEnglish')
                                    </label>
                                <input type="text" class="form-control" placeholder="@lang('site.nameInEnglish')" value="{{$service->title_en}}" name="title_en" />
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group wow fadeInUp">
                                <label>
                                    @lang('site.basicPrice')
                                </label>
                                <input type="number" class="form-control" placeholder="@lang('site.basicPrice')" value="{{$service->price}}" name="price" />
                                <span class="price">@lang('site.SR')</span>
                            </div>
                        </div>

                        <!-- display old prices -->
                        <div class="old-specify-price col-sm-12">
                            @if(isset($service->Prices) && count($service->Prices) > 0)
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.oldPriceInDate')</label>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <label>@lang('site.from')</label>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <label>@lang('site.to')</label>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @foreach($service->Prices as $price)
                                <div class="row specify-price-content wow fadeInUp">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control"  value="{{$price->price}}" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control"  value="{{date("Y-m-d", strtotime($price->start_date))}}" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control"  value="{{date("Y-m-d", strtotime($price->end_date))}}" readonly>
                                        </div>
                                    </div>
                                    <div class="close-btn">
                                        <i class="fas fa-times deletePrice" data-id="{{$price->id}}"></i>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <!-- add new prices -->
                        <div class="specify-price col-sm-12">
                            <div class="row specify-price-content wow fadeInUp">

                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label>@lang('site.priceInDate')</label>
                                        <input type="number" class="form-control" placeholder="@lang('site.priceInDate')" value="" name="prices[]">
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>@lang('site.from')</label>
                                        <input type="text" data-date-format="YYYY MM DD" name="from[]" class="form-control startDate" placeholder="@lang('site.from')"  onfocus="(this.type='date')" onblur="(this.type='text')" min="{{Carbon\Carbon::now()->format('Y-m-d')}}" value="">
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>@lang('site.to')</label>
                                        <input type="text" data-date-format="YYYY MM DD" name="to[]" class="form-control endDate" placeholder="@lang('site.to')"  onfocus="(this.type='date')" onblur="(this.type='text')" min="{{Carbon\Carbon::now()->format('Y-m-d')}}" value="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group wow fadeInUp">
                                <div class="add-price">
                                    + @lang('site.addPriceInDate')
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group wow fadeInUp">
                                <label>
                                    @lang('site.address')
                                </label>
                                <a type="text" class="form-control" data-toggle="modal" data-target="#map-modal" data-dismiss="modal">
                                    <span id="address" class="">
                                        @lang('site.address')
                                    </span>
                                    <i class="fas fa-map-marker-alt"></i>
                                </a>
                                <input type="hidden" name="lat" id="lat" value="{{$service->lat}}">
                                <input type="hidden" name="lng" id="lng" value="{{$service->lng}}">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12 wow fadeInUp">
                            <div class="col-sm-4 col-5 H-selector">
                                <div class="form-group" data-wow-delay="0.8s">
                                    <select id="single" class="select2-single select2-hidden-accessible form-control" name="country_id">
                                        @foreach($countries as $country)
                                        <option value="{{$country->id}}" {{$service->country_id == $country->id? 'selected' : ''}}>{{$country->code}}</option>
                                        @endforeach
                                    </select>
                                    <i class="fas fa-sort-down"></i>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label>
                                    @lang('site.phone')
                                </label>
                                <input type="number" class="form-control" placeholder="@lang('site.phone')" value="{{$service->main_phone}}" name="phone" />
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group wow fadeInUp">
                                <label>@lang('site.serviceType')</label>
                                <select class="form-control" name="section_id">
                                    @foreach($sections as $section)
                                        <option value="{{$section->id}}" {{$service->section_id == $section->id? 'selected' : ''}} >{{$lang == "en"? $section->title_en:$section->title_ar}}</option>
                                    @endforeach
                                    </select>
                                <i class="fas fa-sort-down"></i>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group wow fadeInUp">
                                <label>
                                        @lang('site.availableQuantity')
                                    </label>
                                <input type="number" class="form-control" placeholder="@lang('site.availableQuantity')" value="{{$service->quantity}}" name="quantity" />
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group wow fadeInUp">
                                <label>@lang('site.arabicDescription')</label>
                                <textarea class="form-control" placeholder="" name="desc_ar">{{$service->desc_ar}}</textarea>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group wow fadeInUp">
                                <label>@lang('site.englishDescription')</label>
                                <textarea class="form-control" placeholder="" name="desc_en" style="text-align: left;">{{$service->desc_en}}</textarea>
                            </div>
                        </div>

                        <div class="col-sm-12 wow fadeInUp">
                            <label>@lang('site.availablePayments')</label>
                            <label class="check">
                                @lang('site.deliveryPay')
                                <input type="radio" name="payment_method" value="0" {{$service->payment_method == 0 ? 'checked' : ''}}>
                                <span class="checkmark"></span>
                            </label>
{{--                            <label class="check">--}}
{{--                                @lang('site.onlinePay')--}}
{{--                                <input type="radio" name="payment_method" value="1" {{$service->payment_method == 1 ? 'checked' : ''}}>--}}
{{--                                <span class="checkmark"></span>--}}
{{--                            </label>--}}
                        </div>

                        <div class="col-sm-12">
                            <div class="reservation-btn wow fadeInUp">
                                <button type="submit" class="site-btn login-btn" id="btnSubmitEdit">@lang('site.edit')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<!-- End Login Form -->

<!--map modal-->

<div class="modal fade log-forms-modal" id="map-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content map-content">
            <button type="button" class="close-btn" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            <div id="map"></div>
            <a href="" class="site-btn login-btn" data-toggle="modal" data-target="#signup-modal" data-dismiss="modal">@lang('site.confirmPosition')</a>
        </div>
    </div>
</div>
@endsection

@section('script')

<!--map-->
<script>
    function initMap() {
            var latlng = { lat: Number("{{$service->lat}}"), lng: Number("{{$service->lng}}") };
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 16,
                disableDefaultUI: true,
                zoomControl: true,
                scaleControl: true,
                mapTypeControl: true,
                scaleControl: true,
                streetViewControl: true,
                rotateControl: true,
                fullscreenControl: true,
                animation: google.maps.Animation.DROP,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable:true,
            });

            // lat lng after drage marker
            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById("lat").value = this.getPosition().lat();
                document.getElementById("lng").value = this.getPosition().lng();
            });

            // show address for register and edit map
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow();

            geocoder.geocode({'latLng': latlng }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').text(results[0].formatted_address);
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });

            google.maps.event.addListener(marker, 'dragend', function() {

                geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').text(results[0].formatted_address);
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });
        }

</script>

@if($lang == 'en')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=en&callback=initMap"
            type="text/javascript"></script>
@else
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=ar&callback=initMap"
            type="text/javascript"></script>
@endif

<!-- Date -->
<script>
    // $('input[type="date"]').daterangepicker();
</script>

<!-- date validation -->
<script>
    // from date vlidation and display in order details
    $(document).on('change','.startDate',function(){
        var lang = {!! json_encode($lang) !!};
        $(this).closest('.endDate').val('');
        var today = new Date().setHours(0,0,0,0);
        var fromDate = new Date($(this).val()).setHours(0,0,0,0);
        //console.log(today , fromDate);
        if(today > fromDate){

            if(lang == 'en'){
                var msg = 'start date should be after  or equal today'
            }else{
                var msg = 'يجب ان يكون تاريخ البداية بعد او يعادل تاريخ اليوم';
            }
            toastr.error(msg);
            $(this).val('');
        }
    });

    // to date vlidation and display in order details
    $(document).on('change','.endDate',function(){
        var lang = {!! json_encode($lang) !!};

        var fromDate = $(this).closest('.row').find('.startDate').val();
        var fromDate = new Date(fromDate);
        var toDate = new Date($(this).val());
        //console.log(fromDate);
        //console.log(toDate);

        if(fromDate == 'Invalid Date'){
            if(lang == 'en'){
                var msg = 'choose start date first'
            }else{
                var msg = 'يجب اختيار تاريخ البداية';
            }
            toastr.error(msg);
            $(this).val('');

        }else if (fromDate >toDate ) {
            if(lang == 'en'){
                var msg = 'end date should be after or equal start date'
            }else{
                var msg = 'يجب ان يلى تاريخ النهاية تاريخ البداية';
            }
            toastr.error(msg);
            $(this).val('');
        }
    });
</script>
<script>
    $("input[name = 'quantity']").change(function(){
        var lang = {!! json_encode($lang) !!};
        var data = $(this).val();
        if(data <=0){
            $(this).val('');
            if(lang == 'en'){
                var msg = 'available quantity should be more than 0'
            }else{
                var msg = 'الكمية المتاحة يجب ان لا تقل عن 1';
            }
            toastr.error(msg);
        }
    });
</script>

<!-- submit form using ajax -->
<script>
    // $(document).ready(function () {
    //     $("#btnSubmitAdd").click(function (event) {

    //         //stop submit the form, we will post it manually.
    //         event.preventDefault();

    //         // console.log($('#addForm').find( "input[name='prices[]']" ).val());


    //         $('input[name^="prices"]').each(function() {
    //             console.log($(this).val());
    //         });

    //         //console.log('1');

    //         // Get form
    //         var form = $('#addForm')[0];

    //         // Create an FormData object
    //         var data = new FormData(form);

    //         // var arrayimages = ['1', '2'];
    //         // for (var i = 0; i < array.length; i++) {
    //         //     formData.append('array_php_side[]', array[i]);
    //         // }

    //         var url = $('#addForm').attr('action');
    //         // $.ajax({
    //         //     type: "POST",
    //         //     enctype: 'multipart/form-data',
    //         //     url: url,
    //         //     data: data,
    //         //     processData: false,
    //         //     contentType: false,
    //         //     cache: false,
    //         //     timeout: 600000,
    //         //     success: function (data) {

    //         //         if(data.status == 1){
    //         //             toastr.success(data.message);
    //         //             // $('#addForm')[0].reset();
    //         //             toastr.success(data.message);
    //         //             // window.location.replace("/");
    //         //         }else if(data.status == 0){
    //         //             toastr.error(data.message);
    //         //         }
    //         //         console.log("SUCCESS : ",data);

    //         //     },
    //         //     error: function (e) {
    //         //         console.log("ERROR : ", e);
    //         //     }
    //         // });
    //     });
    // });

</script>

<!-- delete old image and price -->
<script>
    $('.deleteImage').click(function(){
        var imageId = $(this).data('id');
        console.log(imageId);
        $.get('/delete-image/'+imageId,function(data){
            console.log(data);
        });

    });
    $('.deletePrice').click(function(){
        var priceId = $(this).data('id');
        console.log(priceId);
        $.get('/delete-price/'+priceId,function(data){
            console.log(data);
        });
        $(this).closest('.specify-price-content').remove();

    });
</script>
@endsection