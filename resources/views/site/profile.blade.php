@extends('site/layouts/master') 
@section('content')


<!-- Start profile -->

<section>
    <div class="user-profile wrapper">
        <ul class="nav nav-tabs wow fadeInDown" data-wow-delay="0.2s">
            <li class="nav-item">
                <a class="nav-link 1" data-toggle="tab" href="#account">@lang('site.myAccount')</a>
            </li>

            @if(Auth::check() && Auth::user()->provider == 0)
                <li class="nav-item">
                    <a class="nav-link 2" data-toggle="tab" href="#favourits">@lang('site.favorites')</a>
                </li>
            @elseif(Auth::check() && Auth::user()->provider == 1)
                <li class="nav-item">
                    <a class="nav-link 3" data-toggle="tab" href="#services">@lang('site.myServices')</a>
                </li>
            @endif

            <li class="nav-item">
                <a class="nav-link 4" data-toggle="tab" href="#orders">@lang('site.myOrders')</a>
            </li>
        </ul>

        <div class="container">
            <div class="tab-content">
                <!-- data for all, user and provider -->
                <div class="tab-pane container 1 wow fadeInUp" data-wow-delay="0.4s" id="account">
                    <div class="account-info">
                        <p>@lang('site.name') : <span id="userName"> {{$user->name}} </span></p>
                        <p>@lang('site.phoneNumber') : <span id="userPhone"> {{$user->phone}} </span></p>
                        <p> @lang('site.email') : <span id="userEmail"> {{isset($user->email) ? $user->email : 'user@example.com'}} </span></p>
                        <p>@lang('site.address') : <span id="userAddress"> {{isset($user->address) ? $user->address : trans('site.noAddress') }}  </span></p>
                    </div>

                    <div class="change-password">
                        <span data-toggle="modal" data-target="#changePasswordModal">@lang('site.changePassword')</span>
                    </div>

                    <div class="edit-info">
                        <span class="login-btn site-btn" data-toggle="modal" data-target="#editModal">@lang('site.editProfile')</span>
                    </div>
                </div>

                <!-- favpurite section for user -->
                @if(Auth::check() && Auth::user()->provider == 0)
                    <div class="tab-pane container 2 fade" id="favourits">
                        @foreach($userFavServices as $favService)
                            <div class="favourites wow fadeInUp deleteParent{{$favService->Service->id}}" >
                                <div class="fav-block">
                                    <div class="fav-img">
                                        <img src="{{url('/')}}/{{$favService->Service->Images->first()->image}}" />
                                    </div>

                                    <div class="fav-details">
                                        <div class="block-txt">
                                            <div class="block-head">
                                                <h4>{{$lang == 'en'? $favService->Service->title_en:$favService->Service->title_ar}}</h4>
                                                <span data-toggle="modal" data-target="#myModal3">
                                                    <i class="fas fa-map-marker-alt" onclick="initMap({{$favService->Service->lat}},{{$favService->Service->lng}})"></i>
                                                </span>
                                            </div>

                                            <div class="block-body">
                                                <span>{{$favService->Service->price}} @lang('site.SR')</span>
                                                <p>@lang('site.provider') :
                                                    <a href="{{url('section/' .$favService->Service->section_id .'/services/' .$favService->Service->User->id)}}">
                                                        {{$favService->Service->User->name}}
                                                    </a>
                                                </p>
                                            </div>

                                            <div class="block-footer">
                                                <p>{{$lang == 'en' ? $favService->Service->desc_en:$favService->Service->desc_ar}}</p>
                                                <a href="{{url('show-service/'. $favService->Service->id)}}">@lang('site.more')</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="fav-serv">
                                    <div class="buttons-serv">
                                        <a href="{{url('reserve/' .$favService->Service->id )}}" class="site-btn login-btn">@lang('site.reserve')</a>

                                        <!-- remove button -->
                                        <span class="like" data-toggle="modal" data-target="#myModal4" >
                                            <i class="fas fa-heart openDeleteFavModal" data-id="{{$favService->Service->id}}"></i>
                                        </span>
                                    </div>

                                    <ul class="social">
                                        <li>
                                            <a href="#">
                                                    <i class="far fa-copy" onclick="copyUrlToCliboard({{$favService->Service->id}})"></i>
                                                </a>
                                        </li>


                                        <li>
                                            <a href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?pubid=ra-50c5cca05664cee7&title=A+Diciannoveventitre++-+Coats+-+Antonioli.eu&url=https://happy.aait-sa.com/show-service/{{$favService->Service->id}}"
                                               target="_blank"
                                            >
                                                    <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?pubid=ra-50c5cca05664cee7&title=A+Diciannoveventitre++-+Coats+-+Antonioli.eu&url=https://happy.aait-sa.com/show-service/{{$favService->Service->id}}"
                                               target="_blank"
                                            >
                                                    <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="https://api.whatsapp.com/send?phone={{$favService->Service->phone}}" target="_blank">
                                                    <i class="fab fa-whatsapp"></i>
                                                </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>

                <!-- my services for providers -->
                @elseif(Auth::check() && Auth::user()->provider == 1)
                    <div class="tab-pane container 3 fade" id="services">
                        @foreach(App\Models\Service::where('user_id', Auth::id())->get() as $service)
                        <div class="favourites wow fadeInUp" data-wow-delay="0.2s">
                            <div class="fav-block">
                                <div class="fav-img">
                                    <img src="{{url('/')}}/{{$service->Images->first()->image}}" />
                                </div>

                                <div class="fav-details">
                                    <div class="block-txt">
                                        <div class="block-head">
                                            <h4>{{$lang == 'en'? $service->title_en: $service->title_ar}}</h4>
                                            <span data-toggle="modal" data-target="#myModal3">
                                                <i class="fas fa-map-marker-alt" onclick="initMap({{$service->lat}},{{$service->lng}})"></i>
                                            </span>
                                        </div>

                                        <div class="block-body">
                                            <span>{{$service->price}} @lang('site.SR')</span>
                                            <p>@lang('site.phone') : <span> {{$service->phone}} </span></p>
                                        </div>

                                        <div class="block-footer">
                                            <p>{{$lang == 'en'? $service->desc_en:$service->desc_ar}}</p>
                                            <a href="{{url('show-service/' . $service->id)}}">@lang('site.more')</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="fav-serv">
                                <div class="buttons-serv">
                                    <a href="{{url('provider/edit-service/'. $service->id)}}" class="cancel">
                                        @lang('site.edit')
                                    </a>

                                    <div class="share-icon">
                                        <i class="fas fa-share-alt" onclick="copyUrlToCliboard({{$service->id}})"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="close-btn">
                                <i class="fas fa-times" data-toggle="modal" data-target="#remove{{$service->id}}"></i>
                            </div>

                            <!-- Start Remove Modal -->
                            <div class="modal fade" id="remove{{$service->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <div class="modal-body like">
                                            <p>
                                                @lang('site.confirmDeleteService') {{$lang == 'en' ? '?' : '؟'}}
                                            </p>
                                            <div class="like-btn">
                                                <button class="site-btn" data-dismiss="modal">@lang('site.cancel')</button>
                                            <button class="site-btn login-btn accept-remove deleteService" data-id="{{$service->id}}" data-dismiss="modal">@lang('site.confirm')</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach


                    <div class="reservation-btn">
                        <a href="{{url('provider/add-service')}}" class="site-btn login-btn">@lang('site.addService')</a>
                    </div>
                </div>
                @endif

                <!-- start order tab for both user and provider -->
                <div class="tab-pane container 4 fade" id="orders">
                    @foreach($userOrders as $order)
                        @if(isset($order->service_id))
                        <div class="favourites wow fadeInUp deleteParent{{$order->id}}" data-wow-delay="0.2s">
                            <div class="fav-block">
                                <div class="fav-img">
                                    <img src="{{url('/')}}/{{$order->Service->Images->first()->image}}" />
                                </div>

                                <div class="fav-details">
                                    <div class="block-txt">
                                        <div class="block-head">
                                            <h4>{{$lang == 'en'? $order->Service->title_en:$order->Service->title_ar}}</h4>
                                            <span data-toggle="modal" data-target="#myModal3">
                                                    <i class="fas fa-map-marker-alt" onclick="initMap({{$order->Service->lat}},{{$order->Service->lng}})"></i>
                                                </span>
                                        </div>

                                        <div class="block-body">
                                            <span>{{$order->Service->price}} @lang('site.SR')</span>

                                            <!-- show for user only -->
                                            @if(Auth::check() && Auth::user()->provider == 0)
                                                <p>@lang('site.provider') :
                                                    <a href="{{url('section/' .$order->Service->section_id .'/services/' .$order->Service->User->id)}}">
                                                        {{$order->Service->User->name}}
                                                    </a>
                                                </p>
                                            @endif

                                        </div>

                                        <div class="block-footer">
                                            <p>{{$lang == 'en' ? $order->Service->desc_en:$order->Service->desc_ar}}</p>
                                            <a href="{{url('show-service/'. $order->Service->id)}}">@lang('site.more')</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="fav-serv">
                                <div class="buttons-serv">
                                    <p>@lang('site.bookingDate') : <b> {{date("d-m-Y", strtotime($order->start_date))}} </b></p>
                                    <p>@lang('site.bookingEndDate') : <b> {{date("d-m-Y", strtotime($order->end_date))}} </b></p>


                                    <!-- show for user only -->
                                    @if(Auth::check() && Auth::user()->provider == 0)
                                        <p>@lang('site.providerPhone') : <b> {{$order->Provider->phone}} </b></p>
                                        @if($order->status == 1)
                                            <p class="cancel openCancelOrderModal" data-toggle="modal" data-target="#cancelOrderModal" data-id="{{$order->id}}">
                                                @lang('site.cancelReservation')
                                            </p>
                                        @elseif($order->status == 5)
                                            <a href="{{url('show-service/'. $order->Service->id)}}">
                                                <p class="cancel" >
                                                    @lang('site.reOrder')
                                                </p>
                                            </a>

                                        @endif
                                    @elseif(Auth::check() && Auth::user()->provider == 1)
                                        <p>@lang('site.userPhone') : <b> {{$order->User->phone}} </b></p>
                                    @endif
                                </div>
                            </div>

                            <div class="close-btn">
                                <i class="fas fa-times" data-toggle="modal" data-target="#remove2"></i>
                            </div>

                            <!-- Start Remove Modal -->
                            <div class="modal fade" id="remove2">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <div class="modal-body like">
                                            <p>
                                                @lang('site.confirmHideOrder') {{$lang == 'en' ? '?' : '؟'}}
                                            </p>
                                            <div class="like-btn">
                                                <button class="site-btn" data-dismiss="modal">@lang('site.cancel')</button>
                                                <button class="site-btn login-btn accept-remove hideOrderNoti" data-dismiss="modal" data-id="{{$order->id}}">@lang('site.confirm')</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End profile -->


<!-- Change Password Modal -->
<div class="modal fade conditions" id="changePasswordModal">
    <div class="modal-dialog">
        <div class="modal-content account">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form class="form" action="{{url('site/update-password')}}" method="post" id="changePasswordForm">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>@lang('site.oldPassword')</label>
                        <input type="password" class="form-control" placeholder="@lang('site.oldPassword')" name="old_password" />
                    </div>

                    <div class="form-group">
                        <label>@lang('site.theNewPassword')</label>
                        <input type="password" class="form-control" placeholder="@lang('site.theNewPassword')" name="password" />
                    </div>

                    <div class="form-group">
                        <label>@lang('site.confirmNewPassword')</label>
                        <input type="password" class="form-control" placeholder="@lang('site.confirmNewPassword')" name="password_confirmation" />
                    </div>

                    <div class="reservation-btn">
                        <button type="submit" class="site-btn login-btn"  id="btnSubmitChangePassword">@lang('site.save')</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- Edit Profile Modal -->
<div class="modal fade conditions" id="editModal">
    <div class="modal-dialog">
        <div class="modal-content account">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form class="form" action="{{url('site/profile')}}" method="post" id="profileForm">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>@lang('site.firstName')</label>
                                <input type="text" class="form-control userFirstName" placeholder="@lang('site.firstName')" value="{{$user->first_name}}"
                                    name="first_name" />
                            </div>
                        </div>

                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>@lang('site.lastName')</label>
                                <input type="text" class="form-control userLastName" placeholder="@lang('site.lastName')" value="{{$user->last_name}}" name="last_name"
                                />
                            </div>
                        </div>

                        <div class="col-sm-4 col-5">
                            <div class="form-group">
                                <label>@lang('site.countryCode')</label>
                                <select id="single" class="select2-single select2-hidden-accessible form-control userCountryId" name="country_id">
                                    @foreach($countries as $country)
                                        <option value={{$country->id}} {{$user->country_id == $country->id ? 'selected':''}}>
                                            {{$country->code}}
                                        </option>
                                    @endforeach
                                </select>
                                <i class="fas fa-sort-down"></i>
                            </div>
                        </div>

                        <div class="col-sm-8 col-7">
                            <div class="form-group">
                                <label>@lang('site.phoneNumber')</label>
                                <input type="tel" class="form-control userMainPhone" placeholder="@lang('site.phoneNumber')" value="{{$user->main_phone}}"
                                    name="phone" />
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label>@lang('site.email')</label>
                                <span class="optional">@lang('site.optional')*</span>
                                <input type="text" class="form-control userEmail" placeholder="@lang('site.email')" value="{{$user->email}}" name="email">
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label>@lang('site.address')</label>
                                <span class="optional">@lang('site.optional')*</span>
                                <input type="text" class="form-control userAddress" placeholder="@lang('site.address')" value="{{$user->address}}" name="address">
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="reservation-btn">
                                <button type="submit" class="site-btn login-btn"  id="btnSubmitEdit">@lang('site.save')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- Start Map Modal -->
<div class="modal fade" id="myModal3">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div id="map"></div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <p>@lang('site.address') : <span id="address">  </span></p>
            </div>

        </div>
    </div>
</div>

<!-- Start Favourite Modal -->
<div class="modal fade" id="myModal4">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body like">
                <p>@lang('site.confirmDeleteFav'){{$lang == 'en' ? '?' : '؟'}} </p>
                <div class="like-btn">
                    <button class="site-btn" data-dismiss="modal">@lang('site.cancel')</button>
                    <button class="site-btn login-btn removeFav" data-dismiss="modal" >@lang('site.confirm')</button>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- cancel order Modal -->
<div class="modal fade" id="cancelOrderModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body like">
                <p>@lang('site.confirmCancelOrder'){{$lang == 'en' ? '?' : '؟'}} </p>
                <div class="like-btn">
                    <button class="site-btn" data-dismiss="modal">@lang('site.cancel')</button>
                    <button class="site-btn login-btn cancelOrder" data-dismiss="modal" >@lang('site.confirm')</button>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
 
@section('script')

    <script>
        var lat;
        var lng;
        function initMap(lat,lng) {
            var latlng = new google.maps.LatLng(lat,lng);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 16,
                disableDefaultUI: true,
                animation: google.maps.Animation.DROP,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable: false,
                animation: google.maps.Animation.BOUNCE,
            });

            // show address for register and edit map
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow();

            geocoder.geocode({'latLng': latlng }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').text(results[0].formatted_address);
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });

            google.maps.event.addListener(marker, 'dragend', function() {

                geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').text(results[0].formatted_address);
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });

        }

    </script>

    @if($lang == 'en')
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=en&callback=initMap"
                type="text/javascript"></script>
    @else
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=ar&callback=initMap"
                type="text/javascript"></script>
    @endif

<script>
    // form work change password and edit profile
    $(document).ready(function () {
        $("#btnSubmitChangePassword").click(function (event) {

            //stop submit the form, we will post it manually.
            event.preventDefault();

            // Get form
            var form = $('#changePasswordForm')[0];

            // Create an FormData object
            var data = new FormData(form);

            var url = $('#changePasswordForm').attr('action');
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {

                    if(data.status == 1){
                        $('#changePasswordModal').modal('hide');
                        toastr.success(data.message);
                        $('#changePasswordForm')[0].reset();
                        // window.location.replace("/");
                    }else if(data.status == 0){
                        toastr.error(data.message);
                        $('#changePasswordModal').modal('show');
                    }
                    console.log("SUCCESS : ", data);

                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            });

        });

        $("#btnSubmitEdit").click(function (event) {

            //stop submit the form, we will post it manually.
            event.preventDefault();

            // Get form
            var form = $('#profileForm')[0];

            // Create an FormData object
            var data = new FormData(form);

            var url = $('#profileForm').attr('action');
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {

                    if(data.status == 1){
                        $('#editModal').modal('hide');
                        toastr.success(data.message);
                        // console.log(data);

                        $('#userName').text(data.userData.name);
                        $('#userPhone').text(data.userData.phone);
                        $('#userEmail').text(data.userData.email);
                        $('#userAddress').text(data.userData.address);

                        // window.location.replace("/");
                    }else if(data.status == 0){
                        toastr.error(data.message);
                        $('#editModal').modal('show');
                    }
                    // console.log("SUCCESS : ", data);

                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            });

        });

        $('.deleteService').click(function(){
            var serviceId = $(this).data('id')
            $.get( "/provider/delete-service/" + serviceId , function( data ) {
                if(data.status == 1){
                    toastr.success(data.message);
                }
            });

        });

    });

</script>

<script>
    $(document).on('click','.openDeleteFavModal',function(){
        // clear old val from local storage
        localStorage.removeItem('ServiceId');
        // put service id in local storage
        var ServiceId = $(this).attr('data-id');
        localStorage.setItem('ServiceId', ServiceId);
    });
    $(document).on('click','.removeFav',function(){

        // get id from local storage
        var ServiceId = localStorage.getItem('ServiceId', ServiceId);

        // delete from server side
        $.get('/add/' + ServiceId + '/fav',function(data){
            console.log(data);
        });

        // delete from html
        $('.deleteParent' + ServiceId).remove();

        // clear local storage
        localStorage.removeItem('ServiceId');


        {{--var lang = {!! json_encode($lang) !!};--}}
        {{--if(lang == 'en'){--}}
        {{--    var msg = 'Need login for like'--}}
        {{--}else{--}}
        {{--    var msg = 'يجب تسجيل الدخول'--}}
        {{--}--}}
        {{--toastr.error(msg);--}}
        {{--@endif--}}
    });

    $(document).on('click','.hideOrderNoti',function(){
        var order_id = $(this).data('id');
        $.get('/hide-user-order/' + order_id, function(data){
            console.log(data);
        });


    });

    $(document).on('click','.openCancelOrderModal',function(){
        // clear old val from local storage
        localStorage.removeItem('orderId');
        // put service id in local storage
        var orderId = $(this).data('id');
        localStorage.setItem('orderId', orderId);
    });
    $(document).on('click','.cancelOrder',function(){

        // get id from local storage
        var orderId = localStorage.getItem('orderId', orderId);

        // delete from server side
        $.get('/cancel-user-order/' + orderId,function(data){
            console.log(data);
        });

        // delete from html
        $('.deleteParent' + orderId).remove();

        // clear local storage
        localStorage.removeItem('orderId');


        {{--var lang = {!! json_encode($lang) !!};--}}
        {{--if(lang == 'en'){--}}
        {{--    var msg = 'Need login for like'--}}
        {{--}else{--}}
        {{--    var msg = 'يجب تسجيل الدخول'--}}
        {{--}--}}
        {{--toastr.error(msg);--}}
        {{--@endif--}}
    });
</script>

<script>

    function copyUrlToCliboard(data) {

        var dummyContent = 'https://happy.aait-sa.com/show-service/' + data;
        console.log(dummyContent);
        var dummy = $('<input>').val(dummyContent).appendTo('body').select()
        document.execCommand('copy')
        var msg = "تم النسخ - copied";
        toastr.success(msg);

    }

</script>

<!-- select tab -->
<script>
    $(document).ready(function(){
        $('.{{$id}}').addClass('active');
        $('.{{$id}}').addClass('show');
    });
</script>

@endsection