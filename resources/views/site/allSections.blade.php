@extends('site/layouts/master') 
@section('content')
 <!-- Start Services -->

 <section>
        <div class="services wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-5 col-sm-12">
                        <div class="serv-content wow fadeInRight">
                            <div class="serv-txt">
                                    <h2>@lang('site.ourServices')</h2>
                                    <p>
                                        {{ $lang == 'en' ? settings('our_service_en') : settings('our_service_ar')}}}
                                    </p>
                            </div>
                            <div class="serv-border"></div>
                        </div>
                    </div>

                    <div class="col-lg-9 col-md-7 col-sm-12">
                        <ul class="our-services">
                            @foreach($sections as $section)
                                <li class="our-serv wow fadeInUp" data-wow-delay="0.2s">
                                        <a href="{{url('section/'.$section->id.'/services')}}">
                                            <img src="{{url('' . $section->image)}}" />
                                        </a>

                                    <div class="serv-head">
                                        <p>{{$lang == 'en'? $section->title_en:$section->title_ar}}</p>
                                    </div>
                                </li>
                            @endforeach                           
                        </ul>   
                        
                    </div>  
                </div>
            </div>
        </div>
    </section>

    <!-- End Services -->
@endsection