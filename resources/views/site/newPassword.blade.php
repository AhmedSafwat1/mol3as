@extends('site/layouts/master') 

@section('content')


<!-- Start Login Form -->

<section>
    <div class="login wrapper">
        <div class="container">
            <form class="form" action="{{url('site/new-password')}}" method="post" id="newPasswordForm">
                {{csrf_field()}}
                <input type="hidden" value="{{Session::get('user_id')}}" name="user_id">
                <div class="form-txt">
                    <h2 class="wow fadeInDown" data-wow-delay="0.2s">@lang('site.newPassword')</h2>
                    <p class="wow fadeInUp" data-wow-delay="0.4s">
                        @lang('site.enterNewPassword')
                    </p>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="form-group wow flipInX" data-wow-delay="0.6s">
                            <input type="password" class="form-control" placeholder="@lang('site.theNewPassword')" name="password"/>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-group wow flipInX" data-wow-delay="0.8s">
                            <input type="password" class="form-control" placeholder="@lang('site.confirmPassword')" name="password_confirmation"/>
                        </div>
                    </div>
                </div>

                <button type="submit" class="login-btn site-btn wow fadeInUp" data-wow-delay="1s" id="btnSubmitNewpassword">@lang('site.save')</button>
            </form>
        </div>
    </div>
</section>

<!-- End Login Form -->


@endsection



@section('script')

<script>
        $(document).ready(function () {
            $("#btnSubmitNewpassword").click(function (event) {
    
                //stop submit the form, we will post it manually.
                event.preventDefault();
    
                // Get form
                var form = $('#newPasswordForm')[0];
    
                // Create an FormData object
                var data = new FormData(form);
    
                var url = $('#newPasswordForm').attr('action');
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
    
                        if(data.status == 1){
                            window.location.replace("/");
                        }else if(data.status == 0){
                            toastr.error(data.message);
                        }
                        console.log("SUCCESS : ", data);
    
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                });
    
            });
        });
    </script>

@endsection