@extends('site/layouts/master')

@section('content')
    <!-- Start Login Form -->

    <section>
        <div class="login wrapper">
            <div class="container">
                <form class="form" action="{{url('site/register-verfication')}}" method="post" id="verficationForm">
                    {{csrf_field()}}
                    <div class="form-txt">
                        <h2 class="wow fadeInDown" data-wow-delay="0.2s">@lang('api.code')</h2>
                        <p class="wow fadeInUp" data-wow-delay="0.4s">
                            @lang('api.codeSentToPhoneSuccessfully')
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group wow fadeInUp" data-wow-delay="0.6s">
                                <input type="text" class="form-control" placeholder="@lang('api.code')" name="code"/>
                                <input type="hidden" value="{{Session::get('user_id')}}" name="user_id">
                                <input type="hidden" value="{{Session::get('phone')}}" name="phone">
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="login-btn site-btn wow fadeInUp" data-wow-delay="0.6s" id="btnSubmitVerfication">@lang('api.confirm')</button>

                    <div class="form-txt wow fadeInUp" data-wow-delay="0.8s">
                        <p>
                            @lang('api.codeNotSent')
                        </p>

                        <a href="{{url('site/resend-code')}}">@lang('api.resendCode')</a>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <!-- End Login Form -->

@endsection



@section('script')

    <script>
        $(document).ready(function () {
            $("#btnSubmitVerfication").click(function (event) {

                //stop submit the form, we will post it manually.
                event.preventDefault();

                // Get form
                var form = $('#verficationForm')[0];

                // Create an FormData object
                var data = new FormData(form);

                var url = $('#verficationForm').attr('action');
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {

                        if(data.status == 1){
                            window.location.replace("/");
                        }else if(data.status == 0){
                            toastr.error(data.message);
                        }
                        console.log("SUCCESS : ", data);

                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                });

            });
        });
    </script>

@endsection