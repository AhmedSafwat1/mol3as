@extends('site/layouts/master') 
@section('content')
<!-- Start Login Form -->

<section>
    <div class="login contact wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-12">
                    <div class="map wow fadeInUp" data-wow-delay="0.2s">
                        <div id="map"></div>
                        <p>@lang('site.visitUsIn') : {{settings('address')}}</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-5 col-12">
                    <form class="form wow fadeInUp" action="{{url('contact-us')}}" data-wow-delay="0.4s" method="post" id="contactForm">
                        {{csrf_field()}}
                        <div class="form-txt">
                            <h2>@lang('site.forContact')</h2>
                            <p>
                                @lang('site.sendMessageHere')
                            </p>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="@lang('site.name')" name="name" />
                        </div>

                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="@lang('site.email')" name="email" />
                        </div>

                        <div class="row">
                            <div class="form-group col-sm-4 col-5">
                                <select id="single" class="select2-single select2-hidden-accessible form-control" name="country_id">
                                    @foreach($countries as $country)
                                        <option value={{$country->id}} {{$country->code == '966'? 'selected' : ''}}>{{$country->code}}</option>
                                    @endforeach

                                </select>
                                <i class="fas fa-sort-down" style="left: 20px;"></i>
                            </div>
                            <div class="form-group col-sm-8 col-7">
                                <input type="tel" class="form-control" placeholder="@lang('site.phoneNumber')" name="phone" />
                            </div>
                        </div>


                        <div class="form-group">
                            <textarea class="form-control" placeholder="@lang('site.writeMessageHere')" name="message"></textarea>
                        </div>

                        <button type="submit" class="login-btn site-btn" id="btnSubmitContact">@lang('site.send')</button>

                        <p>@lang('site.orCallThisNumbers')</p>
                        <p class="or">{{settings('phone')}}</p>
                        <p>@lang('site.orSendEmail')</p>
                        <p class="or">{{settings('email')}}</p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End Login Form -->
@endsection
 
@section('script')

<script>
    function initMap() {
            // var latlng = new google.maps.LatLng(31.205753,29.924526);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: { lat: Number("{{settings('lat')}}"), lng: Number("{{settings('lng')}}") },
                zoom: 16,
                disableDefaultUI: true,
                zoomControl: true,
                scaleControl: true,
                mapTypeControl: true,
                scaleControl: true,
                streetViewControl: true,
                rotateControl: true,
                fullscreenControl: true,
                animation: google.maps.Animation.DROP,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                position: { lat: Number("{{settings('lat')}}"), lng: Number("{{settings('lng')}}") },
                map: map,
                draggable: false,
                animation: google.maps.Animation.BOUNCE,
            });
            
        }

</script>

@if($lang == 'en')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=en&callback=initMap"
            type="text/javascript"></script>
@else
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=ar&callback=initMap"
            type="text/javascript"></script>
@endif



<script>
    $(document).ready(function () {
            $("#btnSubmitContact").click(function (event) {
    
                //stop submit the form, we will post it manually.
                event.preventDefault();
    
                // Get form
                var form = $('#contactForm')[0];
    
                // Create an FormData object
                var data = new FormData(form);
    
                var url = $('#contactForm').attr('action');
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
    
                        if(data.status == 1){
                            $('#contactForm')[0].reset();
                            toastr.success(data.message);
                        }else if(data.status == 0){
                            toastr.error(data.message);
                        }
                        console.log("SUCCESS : ", data);
    
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                });
    
            });
        });

</script>
@endsection