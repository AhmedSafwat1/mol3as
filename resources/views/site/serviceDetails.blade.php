@extends('site/layouts/master')

@section('style')
    <style>
        .hidden{
            display: none !important;
        }
    </style>
    @endsection
@section('content')
    <!-- Start Login Form -->

    <section>
        <div class="wrapper">
            <div class="container">

                <div class="N-products">
                    <div class="row">
                          <div class="col-md-5 col-sm-12">
                            <div class="N-slider wow fadeInUp" data-wow-delay="0.2s">
                                <div id="owl-hash-URL-2" class="owl-carousel owl-hash ">
                                    @foreach($service->Images as $image)
                                        <div class="item" data-hash="img{{$image->id}}">
                                            <img src="{{url('/') .'/'. $image->image}}" />
                                        </div>
                                    @endforeach

                                </div>

                                <ul class="owl-hash-nav-2">
                                    @foreach($service->Images as $image)
                                        <li class="item active">
                                            <a href="#img{{$image->id}}">
                                                <img src="{{url('/') .'/'. $image->image}}" />
                                            </a>
                                        </li>
                                    @endforeach


                                </ul>
                            </div>
                        </div>

                        {{--<div class="col-md-7 col-sm-12">--}}

                            {{--<div class="row">--}}
                        <div class="col-md-4 col-sm-12">
                                    <div class="N-details wow fadeInUp" data-wow-delay="0.4s">
                                        <h3>{{$lang == 'en'? $service->title_en:$service->title_ar}}</h3>
                                        <h4>
                                            {{$service->price}} @lang('site.SR')
                                        </h4>
                                        @if(count($service->Prices) > 0)
                                            <div class="new-price">
                                                <h6>@lang('site.seasonPrices')</h6>
                                                @foreach($service->Prices as $price)
                                                    <div>
                                                        <h5> {{$price->price}} @lang('site.SR')</h5>
                                                        <p>
                                                            <span> @lang('site.from') </span>
                                                            <span> {{date("d-m-Y", strtotime($price->start_date))}}</span>
                                                            <span> @lang('site.to') </span>
                                                            <span> {{date("d-m-Y", strtotime($price->end_date))}}</span>


                                                        </p>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif

                                        {{--<p class="N-price"> {{$service->price}} @lang('site.SR') </p>--}}
                                        @if(Auth::check() && Auth::user()->provider == 1)
                                            <p class="new-p">@lang('site.availableQuantity') : <span> {{$service->quantity}}  </span></p>
                                        @else
                                            <a href="{{url('section/' .$service->section_id .'/services/' .$service->User->id)}}">

                                                <p class="new-p">@lang('site.provider') : <span> {{$service->User->name}}  </span></p>
                                            </a>
                                        @endif

                                        {{--<div class="details">--}}
                                            {{--<p>{{$lang == 'en' ? $service->desc_en:$service->desc_ar}}</p>--}}
                                        {{--</div>--}}

                                    </div>
                                </div>

                        <div class="col-md-3 col-sm-12">
                                    <div class="N-share wow fadeInUp" data-wow-delay="0.6s">
                                        <div class="buttons-serv">
                                            @if(Auth::check() && Auth::user()->provider == 1)

                                                <!-- edit service if provider -->
                                                <a href="{{url('provider/edit-service/' .$service->id )}}" class="site-btn login-btn">@lang('site.edit')</a>
                                            @else

                                                <!-- reserve and fav if user -->
                                                <div class="buttons-serv">
                                                    <a href="{{url('reserve/'.$service->id)}}" class="site-btn login-btn">@lang('site.reserve')</a>
                                                    <span id="addFav">@lang('site.addFav')</span>
                                                    <span class="like hidden" id="removeFav">
                                                        <i class="fas fa-heart"></i>
                                                    </span>
                                                </div>
                                            @endif
                                        </div>

                                        <div class="share">
                                            <p>@lang('site.shareOn')</p>
                                            <ul class="social">

                                                <li>
                                                    <a href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?pubid=ra-50c5cca05664cee7&title=A+Diciannoveventitre++-+Coats+-+Antonioli.eu&url=https://happy.aait-sa.com/show-service/{{$service->id}}"
                                                       target="_blank"
                                                    >
                                                        <i class="fab fa-facebook-f"></i>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?pubid=ra-50c5cca05664cee7&title=A+Diciannoveventitre++-+Coats+-+Antonioli.eu&url=https://happy.aait-sa.com/show-service/{{$service->id}}"
                                                       target="_blank"
                                                    >
                                                        <i class="fab fa-twitter"></i>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="https://api.whatsapp.com/send?phone={{$service->phone}}" target="_blank">
                                                        <i class="fab fa-whatsapp"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="copy">
                                            <p>@lang('site.copyURL')</p>
                                            <!-- <i class="far fa-copy"></i> -->

                                            <img src="{{url('/')}}/public/happy/img/link.svg"  onclick="copyUrlToCliboard()" class="btn-copy">
                                        </div>

                                        <div class="map" data-toggle="modal" data-target="#myModal">
                                            <img src="{{url('/')}}/public/happy/img/map.png" />
                                            <p>@lang('site.location')</p>
                                        </div>


                                    </div>
                                </div>
                        <div class="col-md-12">
                            <div class="details">
                                <h5> @lang('site.serviceDetails')</h5>
                                <p>{{$lang == 'en' ? $service->desc_en:$service->desc_ar}}</p>
                                <a href="#" class="p-more"> المزيد</a>
                            </div>
                        </div>
                            {{--</div>--}}

                        {{--</div>--}}
                    </div>
                </div>
                @if(Auth::check() && Auth::user()->provider == 1)
                    <!-- Do nothing -->
                @else
                    @if(Session::has('$searchServices') && count(Session::get('$searchServices')) > 0)

                        <div class="page-links wow fadeIn" data-wow-delay="0.8s">
                            <ul class="links">
                                <li>
                                    <a href="#">@lang('site.similarData')</a>
                                </li>
                            </ul>
                        </div>

                        <div class="serv-blocks wow fadeInUp" data-wow-delay="1s">
                            <div class="row">
                                @foreach(Session::get('$searchServices') as $similarService)
                                    <!-- display search results for same section -->
                                    @if($similarService->id != $service->id && $similarService->section_id == $service->section_id)
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="block">
                                                <a href="{{url('show-service/'. $similarService->id)}}">
                                                    <img src="{{url('/')}}/{{$similarService->Images->first()->image}}" />
                                                </a>
                                                <div class="block-txt">
                                                    <div class="block-head">
                                                        <h4>{{$lang == 'en'? $similarService->title_en:$similarService->title_ar}}</h4>
                                                        <span>{{$service->price}} @lang('site.SR')</span>
                                                    </div>

                                                    <div class="block-body">
                                                        <p>@lang('site.availableQuantity') : <span> {{$similarService->quantity}} </span></p>
                                                    </div>

                                                    <div class="block-footer">
                                                        <p>{{$lang == 'en' ? $similarService->desc_en:$similarService->desc_ar}}</p>
                                                        <a href="{{url('reserve/'. $similarService->id)}}">@lang('site.reserve')</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach

                                <!-- more -> services page -->
                                <div class="col-12">
                                    <a href="{{url('section/'.$service->section_id.'/services/')}}" class="site-btn">@lang('site.more')</a>
                                </div>

                            </div>

                        </div>
                    @endif
                @endif

                @include('site.parts.footerSlider')
            </div>
        </div>
    </section>

    <!-- End Login Form -->

    <!-- Start Map Modal -->

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div id="map"></div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <p>@lang('site.address') : <span id="address">  </span></p>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function initMap() {
            var latlng = { lat: Number("{{$service->lat}}"), lng: Number("{{$service->lng}}") };
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 16,
                disableDefaultUI: true,
                zoomControl: true,
                scaleControl: true,
                mapTypeControl: true,
                scaleControl: true,
                streetViewControl: true,
                rotateControl: true,
                fullscreenControl: true,
                animation: google.maps.Animation.DROP,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable: false,
                animation: google.maps.Animation.BOUNCE,
            });

            // show address for register and edit map
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow();

            geocoder.geocode({'latLng': latlng }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').text(results[0].formatted_address);
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });

            google.maps.event.addListener(marker, 'dragend', function() {

                geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').text(results[0].formatted_address);
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });

        }

    </script>

    @if($lang == 'en')
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=en&callback=initMap"
                type="text/javascript"></script>
    @else
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=ar&callback=initMap"
                type="text/javascript"></script>
    @endif

    <script>

        function copyUrlToCliboard() {

            var dummyContent = window.location;
            var dummy = $('<input>').val(dummyContent).appendTo('body').select()
            document.execCommand('copy')
            var msg = "تم النسخ - copied";
            toastr.success(msg);

        }

    </script>
    <script>
        $(document).on('click',('#addFav,#removeFav'),function(){
            @if(Auth::check())
                $.get('/add/' + "{{$service->id}}" + '/fav',function(data){
                    console.log(data);
                });
                $('#addFav').toggleClass('hidden');
                $('#removeFav').toggleClass('hidden');
            @else
                var lang = {!! json_encode($lang) !!};
                if(lang == 'en'){
                    var msg = 'Need login for like'
                }else{
                    var msg = 'يجب تسجيل الدخول'
                }
                toastr.error(msg);
            @endif
        });
    </script>
    <script>
        $(document).ready(function(){
            @if(Auth::check() && Auth::user()->provider ==0 && Auth::user()->checked ==1)
                $.get('/user-fav',function(data){
                    var ids = data.data;
                    if(ids.indexOf(parseInt("{{$service->id}}")) != -1){
                        $('#addFav').toggleClass('hidden');
                        $('#removeFav').toggleClass('hidden');
                        console.log('in fav');
                    }
                });
            @endif
        });
    </script>
@endsection