<!-- Start Navbar -->

<div class="menu">
    <div class="container">
        <div class="menu-content">
            <div class="logo">
                <a href="{{url('/')}}">
                                <img src="{{url('/')}}/public/happy/img/logo.png" class="img-responsive" />
                            </a>
            </div>

            <div class="nav-links">
                <ul>
                    <li class="active">
                        <a href="{{url('/')}}">
                            @lang('site.home')
                        </a>
                    </li>

                    <li class="dropdown-click">
                        <a href="#">
                            @lang('site.services')     
                            <i class="fas fa-sort-down"></i>
                        </a>
                        <div class="dropdown-content">
                            <ul>
                                @foreach (App\Models\Section::activeSection() as $section)
                                <li>
                                    <a href="{{url('section/'. $section->id .'/services')}}">
                                        {{$lang == 'en'? $section->title_en : $section->title_ar}}
                                    </a>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="{{url('about-us')}}">
                            @lang('site.aboutUs')
                        </a>
                    </li>

                    <li>
                        <a href="{{url('contact-us')}}">
                            @lang('site.contactUs')
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="mob-collaps">
    <span></span>
    <span></span>
    <span></span>
</div>

<!-- End Navbar -->

</header>

<!-- End Header -->