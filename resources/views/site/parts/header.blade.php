<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Happy</title>
    <link rel="shortcut icon" href="{{url('/')}}/public/happy/img/logo.png">

    <link rel="stylesheet" href="{{url('/')}}/public/happy/css/font-awesome-5all.css">
    <link rel="stylesheet" href="{{url('/')}}/public/happy/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/')}}/public/happy/css/owl.carousel.css">
    <!-- select2 -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css">
    <link href="{{url('/')}}/public/happy/css/animate.css" rel="stylesheet" />
    <link href="{{url('/')}}/public/happy/css/style.css" rel="stylesheet" />
    @if($lang == 'en')
        <link href="{{url('/')}}/public/happy/css/en.css" rel="stylesheet" />
    @endif @yield('style')

{{--    <link rel="stylesheet" href="css/select2-bootstrap.css">--}}


</head>

<body>

    <!-- Start Loading Page -->

    <div class="overlay"></div>

    <div class="loading">
        <img class="wave-1" src="{{url('/')}}/public/happy/img/wave1.png">
        <img class="wave-2" src="{{url('/')}}/public/happy/img/wave2.png">
        <div class="loader">
            <img src="{{url('/')}}/public/happy/img/logo.gif" class="img-responsive center-block" />
        </div>
    </div>

    <!-- End Loading Page -->

    <!-- Start Header -->

    <header>

        <!-- Start Top Header -->

        <div class="top-header-content">
            <div class="container">
                <div class="top-header">
                    <div class="language">
                        <div class="dropdown-click">
                            @if($lang == 'en') English @else العربية @endif
                            <i class="fas fa-sort-down"></i>

                            <div class="dropdown-content">
                                <ul>
                                    <li>
                                        <a href="{{url('/language/ar')}}">العربية</a>
                                    </li>

                                    <li>
                                        <a href="{{url('/language/en')}}">English</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="user-tools">
                        <ul>
                            <li class="favourite dropdown-click notification">

                                @if(Auth::check() && Auth::user()->provider == 0)
                                    <a href="{{url('site/profile/'. 2)}}">
                                        <i class="fas fa-heart"></i>
                                        @lang('site.favourite')
                                    </a> 
                                @elseif(Auth::check() && Auth::user()->provider == 1)
                                    <a href="{{url('provider/show-notifications')}}">
                                        <i class="fas fa-bell"></i>
                                        <span>@lang('site.notifications')</span>
                                        @if(App\Models\Notification::where('to_id',Auth::id())->where('seen',0)->count() > 0)
                                            <span class="noti">{{App\Models\Notification::where('to_id',Auth::id())->where('seen',0)->count()}}</span>
                                        @endif
                                    </a> 
                                @endif
                                
                            </li>

                            <li class="login">
                                <div class="dropdown-click">
                                    <i class="fas fa-user"></i> 
                                    @if(Auth::check())
                                        {{Auth::user()->name}} 
                                    @else
                                        @lang('site.signIn')
                                    @endif
                                    <i class="fas fa-sort-down"></i>

                                    <div class="dropdown-content">
                                        <ul>
                                            @if(!Auth::check())
                                                <li>
                                                    <a href="{{url('site/login')}}">@lang('site.login')</a>
                                                </li>

                                                <li>
                                                    <a href="{{url('site/register')}}">@lang('site.newRegistration')</a>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="{{url('site/profile/'. 1)}}">@lang('site.profileSettings')</a>
                                                </li>

                                                <li>
                                                    <a href="{{url('site/profile/' . 4)}}">@lang('site.myOrdrs')</a>
                                                </li>

                                                @if(Auth::check() && isset(Auth::user()->Role->role))
                                                    <li><a target="_blank" href="{{url('admin')}}">@lang('site.dashboard')</a></li>
                                                @endif

                                                @if(Auth::user()->provider == 1)
                                                    <li>
                                                        <a href="{{url('site/profile/' . 3)}}">@lang('site.myServices')</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{url('provider/add-service')}}">@lang('site.addServices')</a>
                                                    </li>
                                                @endif

                                                    <li>
                                                        <a href="{{url('site/logout')}}">@lang('site.logOut')</a>
                                                    </li>

                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Top Header -->