<!-- Start Footer -->

<footer>

    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 logo wow fadeIn" data-wow-delay="0.2s">
                    <img src="{{url('/')}}/public/happy/img/logo-footer.png" />
                </div>

                <div class="col-2 links wow fadeIn" data-wow-delay="0.4s">
                    <ul>
                        <li>
                            <a href="{{url('/')}}">
                                        @lang('site.home')
                                    </a>
                        </li>
                        <li>
                            <a href="{{url('about-us')}}">
                                @lang('site.aboutUs')
                            </a>
                        </li>
                        <li>
                            <a href="{{url('contact-us')}}">
                                @lang('site.contactUs')
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="col-2 links wow fadeIn" data-wow-delay="0.6s">
                    <ul>
                        @if(Auth::check())
                            <li>
                                <a href="{{url('site/profile/' . 4)}}">
                                    @lang('site.myOrdrs')
                                </a>
                            </li>
                            @if(Auth::user()->provider == 0)
                                <li>
                                    <a href="{{url('site/profile/'. 2)}}">
                                        @lang('site.favourite')
                                    </a>
                                </li>
                            @elseif(Auth::user()->provider == 1)
                                <li>
                                    <a href="{{url('site/profile/' . 3)}}">
                                        @lang('site.myServices')
                                    </a>
                                </li>
                            @endif
                        @endif
                    </ul>
                </div>

                <div class="col-4 contact wow fadeIn" data-wow-delay="0.8s">
                    <p>
                        {{settings('phone')}}
                    </p>

                    <ul class="social">
                        <li>
                            <a href="{{App\Models\Social::find(4)->url}}">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{App\Models\Social::find(2)->url}}">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{App\Models\Social::find(3)->url}}">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{App\Models\Social::find(5)->url}}">
                                <i class="fab fa-whatsapp"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="col-4 reserve wow fadeIn" data-wow-delay="1s">
                    <p>
                         @lang('site.siteCopyright') <a href="{{url('/')}}"> @lang('site.happy') </a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Fixed Footer -->

    <div class="fixed-footer">
        <ul>
            <li>
                <a href="{{url('/')}}" class="active">
                    <i class="fas fa-home"></i>
                    <span>@lang('site.home')</span>
                </a>
            </li>
            @if(Auth::check())
                @if(Auth::user()->provider == 0)
                    <li>
                        <a href="{{url('site/profile/'. 2)}}">
                            <i class="fas fa-heart"></i>
                            <span>@lang('site.favourite')</span>
                        </a>
                    </li>
                @else
                    <li>
                        <a href="{{url('provider/show-notifications')}}">
                            <i class="fas fa-heart"></i>
                            <span>@lang('site.notifications')</span>
                        </a>
                    </li>
                @endif
                    <li class="prof-links">
                        <i class="fas fa-user"></i>
                        <span>{{Auth::user()->name}}</span>
                    </li>
            @else
                <li>
                    <a href="{{url('site/login')}}">
                                <i class="fas fa-user"></i>
                                <span>@lang('site.login')</span>
                            </a>
                </li>
            @endif
        </ul>
    </div>

    <!-- End Fixed Footer -->

    <!-- Start Mobile Profile Links -->

    <div class="profile-links">
        <div class="links">
            <ul>
                <li>
                    <a href="{{url('site/profile/'. 1)}}">@lang('site.profileSettings')</a>
                </li>

                <li>
                    <a href="{{url('site/profile/' . 4)}}">@lang('site.myOrdrs')</a>
                </li>
                @if(Auth::check() && Auth::user()->provider == 1)
                    <li>
                        <a href="{{url('site/profile/' . 3)}}">@lang('site.myServices')</a>
                    </li>
                    <li>
                        <a href="{{url('provider/add-service')}}">@lang('site.addServices')</a>
                    </li>
                @endif

                <li>
                    <a href="{{url('site/logout')}}">@lang('site.logOut')</a>
                </li>
            </ul>
        </div>

        <div class="N-close">
            <i class="fas fa-times"></i>
        </div>
    </div>

    <!-- End Mobile Profile Links -->

</footer>

<!-- End Footer -->


<script src="{{url('/')}}/public/happy/js/jquery-3.2.1.min.js"></script>
<script src="{{url('/')}}/public/happy/js/popper.min.js"></script>
<script src="{{url('/')}}/public/happy/js/bootstrap.min.js"></script>
<script src="{{url('/')}}/public/happy/js/owl.carousel.min.js"></script>
<script src="{{url('/')}}/public/happy/js/wow.min.js"></script>

<script src="{{url('/')}}/public/happy/js/moment.min.js"></script>
<script src="{{url('/')}}/public/happy/js/daterangepicker.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>
{{--<script src="https://select2.github.io/select2-bootstrap-theme/js/anchor.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>--}}
<script src="{{url('/')}}/public/happy/js/scripts.js"></script>

<script>
    // anchors.options.placement = 'left';
    // anchors.add('.container h1, .container h2, .container h3, .container h4, .container h5');

    // Set the "bootstrap" theme as the default theme for all Select2
    // widgets.
    //
    // @see https://github.com/select2/select2/issues/2927
    $.fn.select2.defaults.set( "theme", "bootstrap" );

    var placeholder = "Select a State";

    $( ".select2-single, .select2-multiple" ).select2( {
        placeholder: placeholder,
        width: null,
        containerCssClass: ':all:'
    } );

    $( ".select2-allow-clear" ).select2( {
        allowClear: true,
        placeholder: placeholder,
        width: null,
        containerCssClass: ':all:'
    } );

    // @see https://select2.github.io/examples.html#data-ajax
    function formatRepo( repo ) {
        if (repo.loading) return repo.text;

        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if ( repo.description ) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
            "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
            "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
            "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
            "</div>" +
            "</div></div>";

        return markup;
    }

    function formatRepoSelection( repo ) {
        return repo.full_name || repo.text;
    }

    $( ".js-data-example-ajax" ).select2({
        width : null,
        containerCssClass: ':all:',
        ajax: {
            url: "https://api.github.com/search/repositories",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });

    $( "button[data-select2-open]" ).click( function() {
        $( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
    });

    $( ":checkbox" ).on( "click", function() {
        $( this ).parent().nextAll( "select" ).prop( "disabled", !this.checked );
    });

    // copy Bootstrap validation states to Select2 dropdown
    //
    // add .has-waring, .has-error, .has-succes to the Select2 dropdown
    // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
    // body > .select2-container) if _any_ of the opened Select2's parents
    // has one of these forementioned classes (YUCK! ;-))
    $( ".select2-single, .select2-multiple, .select2-allow-clear, .js-data-example-ajax" ).on( "select2:open", function() {
        if ( $( this ).parents( "[class*='has-']" ).length ) {
            var classNames = $( this ).parents( "[class*='has-']" )[ 0 ].className.split( /\s+/ );

            for ( var i = 0; i < classNames.length; ++i ) {
                if ( classNames[ i ].match( "has-" ) ) {
                    $( "body > .select2-container" ).addClass( classNames[ i ] );
                }
            }
        }
    });
</script>


@yield('script')

</body>

</html>