<!-- adds slider -->
<div class="bottom-slider owl-carousel">
    @foreach(App\Models\Ad::activeAd() as $ad)
        <div class="item space wow fadeInUp" data-wow-delay="0.4s">
                <img src="{{url('/')}}/{{$ad->image}}" />
        </div>
    @endforeach
</div>