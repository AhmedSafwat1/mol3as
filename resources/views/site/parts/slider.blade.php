<!-- Start Slider -->

<section>
    <div class="container">
        <div class="slider">
            <div class="slider-content">
                @if(Auth::check() && Auth::user()->provider == 1)
                @else
                    <form class="form" action="{{url('search')}}" method="post">
                        {{csrf_field()}}
                        <p>
                            @lang('site.searchFor')
                        </p>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="@lang('site.serviceName')"
                                   name="title"/>
                        </div>

                        <div class="form-group" data-toggle="modal" data-target="#myModal">
                            <input type="text" class="form-control" placeholder="@lang('site.city')"
                                   id="searchAddress"/>
                            <input type="hidden" name="lat" id="lat" value="">
                            <input type="hidden" name="lng" id="lng" value="">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>

                        <div class="form-group">
                            <select class="form-control" name="section_id">
                                <option value="">@lang('site.service')</option>
                                @foreach (App\Models\Section::activeSection() as $section)
                                    <option value="{{$section->id}}">{{$lang == 'en'? $section->title_en : $section->title_ar }}</option>
                                @endforeach
                            </select>
                            <i class="fas fa-sort-down"></i>
                        </div>

                        <button type="submit" class="site-btn">@lang('site.search')</button>
                    </form>
                @endif

                <div id="index-slider" class="owl-carousel">
                    @foreach(App\Models\Slider::activeSlider() as $item)
                        <div class="item">
                            <img src="{{url('/')}}/{{$item->image}}"/>
                            <div class="slider-text">
                                <p>
                                    {{$item->desc}}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="slider-border">
                    @foreach(App\Models\Slider::activeSlider() as $item)
                        <img src="{{url('/')}}/public/happy/img/slider-border.png"/>
                    @endforeach

                </div>
            </div>
        </div>
    </div>

    <!-- Start Map Modal -->

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div id="map"></div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <p>@lang('site.address') : <span id="address">  </span></p>
                </div>

            </div>
        </div>
    </div>
    <script>
        function initMap() {
            var latlng = new google.maps.LatLng(24.7136, 46.6753);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 16,
                disableDefaultUI: true,
                zoomControl: true,
                scaleControl: true,
                mapTypeControl: true,
                scaleControl: true,
                streetViewControl: true,
                rotateControl: true,
                fullscreenControl: true,
                animation: google.maps.Animation.DROP,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable: true,
                //animation: google.maps.Animation.BOUNCE,
            });
            // lat lng after drage marker
            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById("lat").value = this.getPosition().lat();
                document.getElementById("lng").value = this.getPosition().lng();
            });

            // show address for register and edit map
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow();

            geocoder.geocode({'latLng': latlng}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').text(results[0].formatted_address);
                        //$('#searchAddress').attr('placeholder', results[0].formatted_address);
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });

            google.maps.event.addListener(marker, 'dragend', function () {

                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').text(results[0].formatted_address);
                            $('#searchAddress').attr('placeholder', results[0].formatted_address);
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });

        }

    </script>

    @if($lang == 'en')
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=en&callback=initMap"
                type="text/javascript"></script>
    @else
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=ar&callback=initMap"
                type="text/javascript"></script>
    @endif
</section>

<!-- End Slider -->