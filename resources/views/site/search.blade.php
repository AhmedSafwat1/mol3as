@extends('site/layouts/master')
@section('style')
    <style>
        .hidden {
            display: none;
        }
    </style>
@endsection
@section('content')
    <!-- Start Login Form -->

    <section>
        <div id="app">
            <div class="wrapper">
                <div class="container">

                    <div class="search-result">
                        <form class="form" id="searchFilterForm" action="{{url('search')}}" method="post">
                            {{csrf_field()}}
                            <div class="row wow fadeInDown" data-wow-delay="0.2s">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="@lang('site.serviceName')"
                                               name="title"/>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group" data-toggle="modal" data-target="#myModal">
                                        <input type="text" class="form-control" placeholder="@lang('site.city')"
                                               id="searchAddress"/>
                                        <input type="hidden" name="lat" id="lat" value="">
                                        <input type="hidden" name="lng" id="lng" value="">
                                        <i class="fas fa-map-marker-alt"></i>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select class="form-control" name="section_id">
                                            <option value="">@lang('site.service')</option>
                                            @foreach (App\Models\Section::activeSection() as $section)
                                                <option value="{{$section->id}}">{{$lang == 'en'? $section->title_en : $section->title_ar }}</option>
                                            @endforeach
                                        </select>
                                        <i class="fas fa-sort-down"></i>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button type="submit" class="site-btn login-btn"
                                                id="searchFilterBtn">@lang('site.search')</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="page-links wow fadeIn" data-wow-delay="0.4s">
                        <ul class="links">
                            <li>
                                <a href="#">@lang('site.searchResults')</a>
                            </li>
                        </ul>
                    </div>
                    <!-- start services -->
                    <div class="serv-blocks wow fadeInUp" data-wow-delay="0.6s">
                        <div class="row" id="indexSearchResult">
                            @foreach($searchServices as $service)
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="block">
                                        <a href="{{url('show-service/'. $service->id)}}">
                                            <img src="{{url('/')}}/{{$service->Images->first()->image}}"/>
                                        </a>
                                        <div class="block-txt">
                                            <div class="block-head">
                                                <h4>{{$lang == 'en'? $service->title_en:$service->title_ar}}</h4>
                                                <span>{{$service->price}} @lang('site.SR')</span>
                                            </div>

                                            <div class="block-body">
                                                <p>@lang('site.availableQuantity'): <span> {{$service->quantity}}</span>
                                                </p>
                                            </div>

                                            <div class="block-footer">
                                                <p>{{$lang == 'en' ? $service->desc_en:$service->desc_ar}}</p>
                                                <a href="{{url('show-service/'. $service->id)}}">@lang('site.reserve')</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                        </div>
                    </div>

                    @include('site.parts.footerSlider')
                </div>
            </div>

            <!-- The Modal -->

            <div class="modal fade" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div id="map"></div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <p>@lang('site.address') : <span id="address">  </span></p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- End Login Form -->


@endsection
@section('script')
    <script>
        function initMap() {
            var latlng = new google.maps.LatLng(24.7136, 46.6753);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 16,
                disableDefaultUI: true,
                zoomControl: true,
                scaleControl: true,
                mapTypeControl: true,
                scaleControl: true,
                streetViewControl: true,
                rotateControl: true,
                fullscreenControl: true,
                animation: google.maps.Animation.DROP,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable: true,
                //animation: google.maps.Animation.BOUNCE,
            });
            // lat lng after drage marker
            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById("lat").value = this.getPosition().lat();
                document.getElementById("lng").value = this.getPosition().lng();
            });

            // show address for register and edit map
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow();

            geocoder.geocode({'latLng': latlng}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').text(results[0].formatted_address);
                        //$('#searchAddress').attr('placeholder', results[0].formatted_address);
                        //console.log(results[0].address_components[2]);
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });

            google.maps.event.addListener(marker, 'dragend', function () {

                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').text(results[0].formatted_address);
                            $('#searchAddress').attr('placeholder', results[0].formatted_address);
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });

        }

    </script>

    @if($lang == 'en')
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=en&callback=initMap"
                type="text/javascript"></script>
    @else
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&language=ar&callback=initMap"
                type="text/javascript"></script>
    @endif

    <script>

        // $(document).on('click','#searchFilterBtn',function(event){
        //     event.preventDefault();
        //     console.log('clicked');
        //
        //     if(!$('#indexSearchResult').hasClass('hidden')){
        //         $('#indexSearchResult').addClass('hidden');
        //     }
        // })

    </script>
    {{--    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>--}}
    {{--    <script>--}}
    {{--        var app = new Vue({--}}
    {{--            el: '#app',--}}
    {{--            data: {--}}
    {{--                allData: [],--}}
    {{--                filteredData: [],--}}
    {{--            },--}}
    {{--            created(){--}}
    {{--                var A = this;--}}
    {{--                $.get('all-services',function(data){--}}
    {{--                    //console.log(data);--}}
    {{--                    A.allData = data.data;--}}
    {{--                })--}}
    {{--            }--}}
    {{--        })--}}
    {{--    </script>--}}
@endsection
