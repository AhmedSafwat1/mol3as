@extends('site/layouts/master') 

@section('content')
<!-- Start Login Form -->

    <section>
        <div class="login wrapper">
            <div class="container">
                <form class="form" action="{{url('site/register')}}" method="post" id="registerForm">
                    {{csrf_field()}}
                    <div class="form-txt">
                        <h2 class="wow fadeInDown" data-wow-delay="0.2s">@lang('site.newRegistration')</h2>
                        <p class="wow fadeInUp" data-wow-delay="0.4s">
                            @lang('site.logMobPass')
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-12">
                            <div class="form-group wow fadeInUp" data-wow-delay="0.6s">
                                <input type="text" class="form-control" placeholder="@lang('site.firstName')" name="first_name" />
                            </div>
                        </div>

                        <div class="col-sm-6 col-12">
                            <div class="form-group wow fadeInUp" data-wow-delay="0.6s">
                                <input type="text" class="form-control" placeholder="@lang('site.lastName')" name="last_name"/>
                            </div>
                        </div>

                        <div class="col-sm-4 col-5">
                            <div class="form-group wow fadeInUp" data-wow-delay="0.8s">
                                <select id="single" class="select2-single select2-hidden-accessible form-control" name="country_id">
                                    @foreach($countries as $country)
                                    <option value={{$country->id}} {{$country->code == '966'? 'selected' : ''}}>{{$country->code}}</option>
                                    @endforeach
                                    
                                </select>
                                <i class="fas fa-sort-down"></i>
                            </div>
                        </div>

                        <div class="col-sm-8 col-7">
                            <div class="form-group wow fadeInUp" data-wow-delay="0.8s">
                                <input type="tel" class="form-control" placeholder="@lang('site.phoneNumber') 50xxxxxxx" name="phone" />
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group wow fadeInUp" data-wow-delay="1s">
                                <input type="password" class="form-control" placeholder="@lang('site.password')" name="password"/>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group wow fadeInUp" data-wow-delay="1s">
                                <input type="password" class="form-control" placeholder="@lang('site.confirmPassword')" name="password_confirmation"/>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group wow fadeInUp" data-wow-delay="1s">
                                <select class="form-control" name="provider">
                                    <option value="0">@lang('site.normalUser')</option>
                                    <option value="1">@lang('site.provider')</option>
                                </select>
                                <i class="fas fa-sort-down"></i>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group wow fadeInUp" data-wow-delay="1s">
                                <div class="checkbox">
                                    <!-- <label> -->
                                        <input type="checkbox" value="1" name="terms">
                                        <span data-toggle="modal" data-target="#myModal">
                                            @lang('site.confirmTerms')
                                        </span>
                                    <!-- </label> -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="login-btn site-btn wow fadeInUp" data-wow-delay="1s" id="btnSubmitRegister">@lang('site.record')</button>

                    <p class="or wow fadeInUp" data-wow-delay="1s">
                        <span>@lang('site.or')</span>
                    </p>

                    <div class="register-btn wow fadeInUp" data-wow-delay="1s">
                        <a href="{{url('site/login')}}" class="site-btn">@lang('site.login')</a>
                        <a href="{{url('/')}}" class="site-btn">@lang('site.asGuest')</a>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <!-- End Login Form -->

<!-- The Modal -->
<div class="modal fade conditions" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <h5 class="condition-title">@lang('site.terms')</h5>
                <p>

                    {{$lang == 'en'? settings('condition_en') : settings('condition_ar') }}
                </p>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="site-btn login-btn" data-dismiss="modal">@lang('site.accept')</button>
            </div>

        </div>
    </div>
</div>
    @endsection

    @section('script')

    <script>
            $(document).ready(function () {
                $("#btnSubmitRegister").click(function (event) {
        
                    //stop submit the form, we will post it manually.
                    event.preventDefault();
        
                    // Get form
                    var form = $('#registerForm')[0];
        
                    // Create an FormData object
                    var data = new FormData(form);
        
                    var url = $('#registerForm').attr('action');
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: url,
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        timeout: 600000,
                        success: function (data) {
        
                            if(data.status == 1){
                                window.location.replace("register-verfication");
                            }else if(data.status == 0){
                                toastr.error(data.message);
                            }
                            console.log("SUCCESS : ", data);
        
                        },
                        error: function (e) {
                            console.log("ERROR : ", e);
                        }
                    });
        
                });
            });
        </script>

    @endsection