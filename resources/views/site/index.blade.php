@extends('site/layouts/master') 

@section('content')

@include('site/parts/slider')
<!-- Start Services -->

<section>
    <div class="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-5 col-sm-12">
                    <div class="serv-content wow fadeInRight">
                        <div class="serv-txt">
                            <h2>@lang('site.ourServices')</h2>
                            <p>
                                {{ $lang == 'en' ? settings('our_service_en') : settings('our_service_ar')}}
                            </p>
                        </div>
                        <div class="serv-border"></div>
                    </div>
                </div>

                <div class="col-lg-9 col-md-7 col-sm-12">
                    <ul class="our-services">
                        @foreach(App\Models\Section::activeSection()->take(4) as $section)
                            <li class="our-serv wow fadeInUp" data-wow-delay="0.2s">
                                    <a href="{{url('section/'.$section->id.'/services')}}">
                                        <img src="{{url('' . $section->image)}}" />
                                    </a>

                                <div class="serv-head">
                                    <p>{{$lang == 'en'? $section->title_en:$section->title_ar}}</p>
                                </div>
                            </li>
                        @endforeach

                        
                    </ul>

                    <a href="{{url('all-sections')}}" class="site-btn wow fadeInUp" data-wow-delay="0.8s">@lang('site.more')</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End Services -->

<!-- Start Who -->

<section>
    <div class="who">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6 col-sm-7 whos">
                    <div class="about wow fadeInUp" data-wow-delay="0.2s">
                        <div class="about-head">
                            <h4>@lang('site.happy')</h4>
                        </div>
                        <div class="about-content">
                                <p>
                                    {!! $lang == 'en' ? settings('about_us_en') : settings('about_us_ar') !!}
                                </p>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-md-6 col-sm-7 whos">
                    <div class="about wow fadeInUp" data-wow-delay="0.4s">
                        <div class="about-head">
                            <h4> @lang('site.aboutUs')</h4>
                        </div>
                        <div class="about-content">
                            <p>
                                {!! $lang == 'en' ? settings('who_us_en') : settings('who_us_ar') !!}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-md-6 col-sm-7 whos">
                    <div class="about wow fadeInUp" data-wow-delay="0.6s">
                        <div class="about-head">
                            <h4>@lang('site.whyHappy')</h4>
                        </div>
                        <div class="about-content">
                            <p>
                                {!! $lang == 'en' ? settings('why_us_en') : settings('why_us_ar') !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End Who -->

<!-- Start App -->

<section>
    <div class="app">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 wow fadeInDown">
                    <h4>
                            @lang('site.application') <span>@lang('site.happy')</span> @lang('site.availableNowOnStore')
                    </h4>
                    <p>
                        @lang('site.downloadHappy')
                    </p>

                    <ul class="app-icon">
                        @if(Auth::check() && Auth::user()->provider == 1)
                            <li>
                                <a href="{{settings('provider_ios_url')}}">
                                    <i class="fab fa-app-store"></i>
                                    <p>app Store</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{settings('provider_android_url')}}">
                                    <i class="fab fa-google-play"></i>
                                    <p>Play Store</p>
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="{{settings('client_ios_url')}}">
                                    <i class="fab fa-app-store"></i>
                                    <p>app Store</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{settings('client_android_url')}}">
                                    <i class="fab fa-google-play"></i>
                                    <p>Play Store</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>

                <div class="col-md-6 col-sm-12 wow fadeInUp">
                    <img src="public/happy/img/app.png" />
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End App -->
@endsection