@extends('site/layouts/master') 
@section('content')

<!-- Start Who -->

<section>
    <div class="who wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6 col-sm-7 whos">
                    <div class="about wow fadeIn" data-wow-delay="0.2s">
                        <div class="about-head">
                            <h4>@lang('site.happy')</h4>
                        </div>
                        <div class="about-content">
                            <p>
                                {!! $lang == 'en' ? settings('about_us_en') : settings('about_us_ar') !!}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-md-6 col-sm-7 whos">
                    <div class="about wow fadeIn" data-wow-delay="0.4s">
                        <div class="about-head">
                            <h4> @lang('site.aboutUs')</h4>
                        </div>
                        <div class="about-content">
                            <p>
                                {!! $lang == 'en' ? settings('who_us_en') : settings('who_us_ar') !!}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-md-6 col-sm-7 whos">
                    <div class="about wow fadeIn" data-wow-delay="0.6s">
                        <div class="about-head">
                            <h4>@lang('site.whyHappy')</h4>
                        </div>
                        <div class="about-content">
                            <p>
                                {!! $lang == 'en' ? settings('why_us_en') : settings('why_us_ar') !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End Who -->
@endsection