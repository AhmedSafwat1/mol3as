@extends('site/layouts/master')
@section('style')
    <style>
        .hidden{
            display: none;
        }
    </style>
    @endsection
@section('content')
    <!-- Start Login Form -->

    <section>
        <div class="wrapper">
            <div class="container">

                <!-- if provider -->
                @if(Auth::check() && Auth::user()->provider == 1)
                    <div class="page-links wow fadeInDown">
                        <ul class="links">
                            <li>
                                <a href="#">{{$lang == 'en'? $section->title_en : $section->title_ar}}  </a>
                            </li>

                            <li>
                                - {{Auth::user()->name}}
                            </li>

                        </ul>

                        <div class="add-service">
                            <a href="{{url('provider/add-service')}}" class="site-btn login-btn">
                                + @lang('site.addService')
                            </a>
                        </div>
                    </div>

                <!-- if user or visitor -->
                @else
                    <div class="services-slider owl-carousel">
                        @foreach(App\Models\Service::specialService($section->id) as $VipService)
                            <div class="item service-img wow fadeInDown" data-wow-delay="0.2s">
                            <img src="{{url('public/happy')}}/img/serv-img.png" />
                            <div class="serv-top-content">
                                <h2>{{$lang == 'en'? $VipService->title_en:$VipService->title_ar}}</h2>
                                <p>{{$lang == 'en' ? $VipService->desc_en:$VipService->desc_ar}}</p>
                                <a href="{{url('show-service/'. $VipService->id)}}" class="site-btn login-btn">@lang('site.reserveNow')</a>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="page-links wow fadeIn" data-wow-delay="0.4s">
                        <ul class="links">
                            <li>
                                <a href="#">{{$lang == 'en'? $section->title_en : $section->title_ar}}  {{$providerName}}</a>
                            </li>
                        </ul>

                        <div class="filter">
                            <form>
                                <div class="dropdown-click">
                                    <span id="mostUse">@lang('site.mostUse')</span>
                                    <i class="fas fa-sort-down"></i>

                                    <div class="dropdown-content">
                                        <ul>
{{--                                            <li>--}}
{{--                                                <a href="#" id="mostUse">@lang('site.mostUse')</a>--}}
{{--                                            </li>--}}

                                            <li>
                                                <a href="#" id="highPrice">@lang('site.highPrice')</a>
                                            </li>

                                            <li>
                                                <a href="#" id="lowPrice">@lang('site.lowPrice')</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif

                <!-- start services -->
                <div class="serv-blocks wow fadeInUp" data-wow-delay="0.2s">
                    <div class="row mostUse">
                        @foreach($services as $service)
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="block">
                                    <a href="{{url('show-service/'. $service->id)}}">
                                        <img src="{{url('/')}}/{{$service->Images->first()->image}}" />
                                    </a>
                                    <div class="block-txt">
                                        <div class="block-head">
                                            <h4><a href="{{url('show-service/'. $service->id)}}">{{$lang == 'en'? $service->title_en:$service->title_ar}}</a></h4>
                                            <span>{{$service->price}} @lang('site.SR')</span>
                                        </div>

                                        <div class="block-body">
                                            <p>@lang('site.availableQuantity') : <span> {{$service->quantity}} </span></p>
                                            <p>@lang('site.provider') : <span> {{$service->User->name}} </span></p>
                                        </div>

                                        <div class="block-footer">
                                            <p>{{$lang == 'en' ? $service->desc_en:$service->desc_ar}}</p>
                                            @if(Auth::check() && Auth::user()->provider == 1)
                                                <a href="{{url('provider/edit-service/' .$service->id )}}">@lang('site.edit')</a>
                                            @else
                                                <a href="{{url('reserve/'. $service->id)}}">@lang('site.reserve')</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach


                        <div class="col-12">
                            {{ $services->links() }}
                        </div>

                    </div>
                    <div class="row highPrice hidden">
                        @foreach($servicesByHighPrice as $service)
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="block">
                                    <a href="{{url('show-service/'. $service->id)}}">
                                        <img src="{{url('/')}}/{{$service->Images->first()->image}}" />
                                    </a>
                                    <div class="block-txt">
                                        <div class="block-head">
                                            <h4><a href="{{url('show-service/'. $service->id)}}">{{$lang == 'en'? $service->title_en:$service->title_ar}}</a></h4>
                                            <span>{{$service->price}} @lang('site.SR')</span>
                                        </div>

                                        <div class="block-body">
                                            <p>@lang('site.availableQuantity') : <span> {{$service->quantity}} </span></p>
                                            <p>@lang('site.provider') : <span> {{$service->User->name}} </span></p>
                                        </div>

                                        <div class="block-footer">
                                            <p>{{$lang == 'en' ? $service->desc_en:$service->desc_ar}}</p>
                                            @if(Auth::check() && Auth::user()->provider == 1)
                                                <a href="{{url('provider/edit-service/' .$service->id )}}">@lang('site.edit')</a>
                                            @else
                                                <a href="{{url('reserve/'. $service->id)}}">@lang('site.reserve')</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach


                        <div class="col-12">
                            {{ $servicesByHighPrice->links() }}
                        </div>

                    </div>
                    <div class="row lowPrice hidden">
                        @foreach($servicesByLowPrice as $service)
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="block">
                                    <a href="{{url('show-service/'. $service->id)}}">
                                        <img src="{{url('/')}}/{{$service->Images->first()->image}}" />
                                    </a>
                                    <div class="block-txt">
                                        <div class="block-head">
                                            <h4><a href="{{url('show-service/'. $service->id)}}">{{$lang == 'en'? $service->title_en:$service->title_ar}}</a></h4>
                                            <span>{{$service->price}} @lang('site.SR')</span>
                                        </div>

                                        <div class="block-body">
                                            <p>@lang('site.availableQuantity') : <span> {{$service->quantity}} </span></p>
                                            <p>@lang('site.provider') : <span> {{$service->User->name}} </span></p>
                                        </div>

                                        <div class="block-footer">
                                            <p>{{$lang == 'en' ? $service->desc_en:$service->desc_ar}}</p>
                                            @if(Auth::check() && Auth::user()->provider == 1)
                                                <a href="{{url('provider/edit-service/' .$service->id )}}">@lang('site.edit')</a>
                                            @else
                                                <a href="{{url('reserve/'. $service->id)}}">@lang('site.reserve')</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach


                        <div class="col-12">
                            {{ $servicesByLowPrice->links() }}
                        </div>

                    </div>
                </div>

            @include('site.parts.footerSlider')
            </div>
        </div>
    </section>

    <!-- End Login Form -->
@endsection

@section('script')
    <script>
        // show and hide filter data
        $(document).on('click','#mostUse', function () {
            if($('.mostUse').hasClass('hidden')){
                $('.mostUse').removeClass('hidden');
            }
            if(!$('.highPrice').hasClass('hidden')){
                $('.highPrice').addClass('hidden');
            }
            if(!$('.lowPrice').hasClass('hidden')){
                $('.lowPrice').addClass('hidden');
            }
        });
        $(document).on('click','#highPrice', function () {
            if($('.highPrice').hasClass('hidden')){
                $('.highPrice').removeClass('hidden');
            }
            if(!$('.mostUse').hasClass('hidden')){
                $('.mostUse').addClass('hidden');
            }
            if(!$('.lowPrice').hasClass('hidden')){
                $('.lowPrice').addClass('hidden');
            }
        });
        $(document).on('click','#lowPrice', function () {
            if($('.lowPrice').hasClass('hidden')){
                $('.lowPrice').removeClass('hidden');
            }
            if(!$('.mostUse').hasClass('hidden')){
                $('.mostUse').addClass('hidden');
            }
            if(!$('.highPrice').hasClass('hidden')){
                $('.highPrice').addClass('hidden');
            }
        });

    </script>
@endsection