@extends('site/layouts/master')
@section('style')
    <style>
        .hidden{
            display: none;
        }
    </style>
@endsection
@section('content')
    <!-- Start Reservation -->

    <section>
        <div class="reservation wrapper">
            <div class="container">
                <h5 class="wow fadeInDown">@lang('site.reservation')</h5>
                <form action="{{url('reserve')}}" method="post" id="reserveForm" class="form">
                    {{csrf_field()}}
                    <div class="date wow fadeInUp" data-wow-delay="0.2s">
                        <h5>@lang('site.bookingDate')</h5>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>@lang('site.from')</label>
                                    <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
{{--                                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4" data-toggle="datetimepicker" placeholder="04/14/2019"/>--}}
                                        <input type="text" data-date-format="YYYY MM DD" name="from" class="form-control" placeholder="@lang('site.from')"  onfocus="(this.type='date')" onblur="(this.type='text')" min="{{Carbon\Carbon::now()->addDay()->format('Y-m-d')}}" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>@lang('site.to')</label>
                                    <div class="input-group date" id="datetimepicker5" data-target-input="nearest">
{{--                                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker5" data-toggle="datetimepicker" placeholder="04/14/2019"/>--}}
                                        <input type="text" data-date-format="YYYY MM DD" name="to" class="form-control" placeholder="@lang('site.to')" onfocus="(this.type='date')" onblur="(this.type='text')" min="{{Carbon\Carbon::now()->addDay()->format('Y-m-d')}}" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="details wow fadeInUp" data-wow-delay="0.4s">
                        <h5>@lang('site.reservationDetails')</h5>
                        <div class="details-txt">
                            <p>{{$lang == 'en'? $service->title_en:$service->title_ar}}</p>
{{--                            <p>@lang('site.price') : <span> {{$service->price}} @lang('site.SR') </span></p>--}}
                            <p>@lang('site.price') : <span id="orderPrice"> {{$service->price}}</span> <span> @lang('site.SR') </span></p>
                            <p>@lang('site.bookingDate')@lang('site.from') : <span id="fromDate">--/--/----</span> @lang('site.to') <span id="toDate">--/--/----</span></p>
                        </div>
                    </div>
                    <input type="hidden" name="service_id" value="{{$service->id}}">
                    <input type="hidden" name="payment_method" value="{{$service->payment_method}}">

                    @if($service->payment_method == 0)
                        <div class="payment-way wow fadeInUp" data-wow-delay="0.6s">
                            <h5>@lang('site.paymentType')</h5>
                            <p>@lang('site.deliveryPay')</p>
                        </div>
                    @else
                        <div class="payment-way wow fadeInUp" data-wow-delay="0.6s">
                            <h5>@lang('site.paymentType')</h5>
                            <p>@lang('site.onlinePay')</p>
                        </div>

                        <div class="payment-form wow fadeInUp" data-wow-delay="0.8s">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>@lang('site.idNumber')</label>
                                    <input type="number" class="form-control" placeholder="0000 0000 0000" name="id_num"/>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>@lang('site.expireDate')</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" placeholder="MM" name="id_mm"/>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" placeholder="YY" name="id_yy"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>CVV</label>
                                    <input type="number" class="form-control" placeholder="000" name="id_cvv"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="reservation-btn wow fadeInUp" data-wow-delay="1s">
                        <button type="submit" class="login-btn site-btn" id="btnSubmitReserve">@lang('site.confirmReservation')</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <!-- End Reservation -->
@endsection
@section('script')
    <script>
        // from date vlidation and display in order details
        $('input[name=from]').change(function(){
            $('input[name=to]').val('');
            $('#toDate').text('dd/mm/yyyy');
            $('#orderPrice').text("{{$service->price}}");
            var today = new Date();
            var fromDate = new Date($(this).val());
            if(today >= fromDate){
                var lang = {!! json_encode($lang) !!};
                if(lang == 'en'){
                    var msg = 'start date should be after  or equal today'
                }else{
                    var msg = 'يجب ان يكون تاريخ البداية بعد او يعادل تاريخ اليوم';
                }
                toastr.error(msg);
                $(this).val('');
            }else{
                // display in order details
                $('#fromDate').text($(this).val());
            }
        });

        // to date vlidation and display in order details
        $('input[name=to]').change(function(){
            var lang = {!! json_encode($lang) !!};

            var fromDate = new Date($('input[name=from]').val());
            var toDate = new Date($('input[name=to]').val());

            if(fromDate == 'Invalid Date'){
                if(lang == 'en'){
                    var msg = 'choose start date first'
                }else{
                    var msg = 'يجب اختيار تاريخ البداية';
                }
                toastr.error(msg);
                $(this).val('');
            }else if (fromDate >toDate ) {
                if(lang == 'en'){
                    var msg = 'end date should be after or equal start date'
                }else{
                    var msg = 'يجب ان يلى تاريخ النهاية تاريخ البداية';
                }
                toastr.error(msg);
                $(this).val('');
            }else{
                // display in order details
                $('#toDate').text($(this).val());

                // get price updated in order details
                var service_id = "{{$service->id}}";
                var from = $('input[name=from]').val();
                var to = $('input[name=to]').val();
                $.get('/reserve-price/' + service_id +'/'+ from+ '/' + to ,function(data){
                    $('#orderPrice').text(data.price);
                    //console.log(data);
                });
            }
        });

        // form submit
        $(document).ready(function () {
            $("#btnSubmitReserve").click(function (event) {

                //stop submit the form, we will post it manually.
                event.preventDefault();

                // Get form
                var form = $('#reserveForm')[0];

                // Create an FormData object
                var data = new FormData(form);

                var url = $('#reserveForm').attr('action');
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {

                        if(data.status == 1){
                            window.location.replace("/site/profile");
                        }else if(data.status == 0){
                            toastr.error(data.message);
                        }
                        console.log("SUCCESS : ", data);

                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                });

            });
        });

    </script>

    {{--    <script>--}}

{{--        $(function () {--}}
{{--            $('#datetimepicker4').datetimepicker({--}}
{{--                format: 'L'--}}
{{--            });--}}
{{--        });--}}

{{--        $(function () {--}}
{{--            $('#datetimepicker5').datetimepicker({--}}
{{--                format: 'L'--}}
{{--            });--}}
{{--        });--}}

{{--    </script>--}}
@endsection
