@extends('dashboard.index')
@section('title')
    الرئيسية
@endsection
@section('content')
    <section>
        <!-- new div-->
        <div class="new-title">
            <h5>
                احصائيات عامة
            </h5>
        </div>
        <div class="row statistic">
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('admins') }}" class=""> عدد المديرين  </a>
                    <div class="number counter-value" data-count="{{ App\User::where('role','>',0)->count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('users') }}" class=""> عدد المستخدمين  </a>
                    <div class="number counter-value" data-count="{{ App\User::where('role',0)->where('provider','0')->count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('packages') }}" class=""> عدد الباقات  </a>
                    <div class="number counter-value" data-count="{{ App\Models\Package::count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('cities') }}" class=""> عدد المدن  </a>
                    <div class="number counter-value" data-count="{{ App\Models\City::count() }}">0</div>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ url('admin/providers') }}" class=""> عدد الاطباء  </a>
                    <div class="number counter-value" data-count="{{ App\User::where('role',0)->where('provider','1')->count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ url('admin/providers?gender=male') }}" class=""> عدد الاطباء ( الذكور)  </a>
                    <div class="number counter-value" data-count="{{ App\User::where('role',0)->where('provider','1')->where('gender','male')->count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ url('admin/providers?gender=female') }}" class=""> عدد الأطباء (الاناث)  </a>
                    <div class="number counter-value" data-count="{{ App\User::where('role',0)->where('provider','1')->where('gender','female')->count() }}">0</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="block" id="counter">
                    <a  href="{{ route('specialists') }}" class=""> عدد التخصصات  </a>
                    <div class="number counter-value" data-count="{{ App\Models\Specialist::count() }}">0</div>
                </div>
            </div>
        </div>
        <!-- end div-->

        

    </section>
    <!-- End Statistics-->
@endsection
