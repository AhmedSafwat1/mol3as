@section('styles')

    <style>

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

        .dropify-wrapper{
            height: 150px !important;
        }

        .custombox-modal-container.custombox-modal-container-fadein {
            width: 950px !important;
        }

        a#rmvImage {
            display: list-item;
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    تعديل
@endsection
@section('content')

    <div class="row">

        

        <div class="col-sm-12">
            <div class="card-box table-responsive boxes">

                <!-- /*** start multiable form HTML ***/ -->
                <form action="{{route('updateservice')}}" method="POST" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <input  autocomplete="false" type="hidden" name="id" value="{{$data->id}}">
                    <input  autocomplete="false" type="hidden" name="section_id" value="{{$data->section_id}}">
                            
                    <!-- basic data -->
                    <div class="row tab">
                        <h4 class="custom-modal-title" style="background-color: #4c739a;width:50%;margin-bottom: 15px;margin-top: 15px">
                           بيانات أساسية
                        </h4>
                        
                        <div class="col-xs-6">
                            <p><input  autocomplete="false" required class="form-control " value="{{$data->title_ar}}" name="title_ar" placeholder="الاسم بالعربية "></p>
                        </div>

                        <div class="col-xs-6">
                            <p><input  autocomplete="false" required class="form-control " value="{{$data->title_en}}" name="title_en" placeholder="الاسم بالانجليزية "></p>
                        </div>
                        
                        <div class="col-xs-6">
                            <p><input  autocomplete="false" required class="form-control phone" value="{{$data->price}}" name="price" placeholder="السعر لكل كيلو "></p>
                        </div>

                        <div class="col-xs-6">
                            <p><input  autocomplete="false" class="form-control phone" value="{{$data->discount}}" name="discount" placeholder="نسبة الخصم في حالة العروض"></p>
                        </div>

                        <div class="col-xs-12">
                            <p>
                                <textarea name="short_desc_ar" id="short_desc_ar" required class="form-control " cols="30" rows="1" placeholder="وصف مختصر بالعربية">{{$data->short_desc_ar}}</textarea>
                            </p>
                        </div>
                        <div class="col-xs-12">
                            <p>
                                <textarea name="short_desc_en" id="short_desc_en" required class="form-control " cols="30" rows="1" placeholder="وصف مختصر بالانجليزية">{{$data->short_desc_en}}</textarea>
                            </p>
                        </div>
                        <div class="col-xs-12">
                            <p>
                                <textarea name="desc_ar" id="desc_ar" required class="form-control " cols="30" rows="5" placeholder="وصف كامل بالعربية">{{$data->desc_ar}}</textarea>
                            </p>
                        </div>
                        <div class="col-xs-12">
                            <p>
                                <textarea name="desc_en" id="desc_en" required class="form-control " cols="30" rows="5" placeholder="وصف كامل بالانجليزية">{{$data->desc_en}}</textarea>
                            </p>
                        </div>
                    </div>

                    <!-- images -->
                    <div class="row tab">
                        <h4 class="custom-modal-title" style="background-color: #4c739a;width:50%;margin-bottom: 15px;margin-top: 15px">
                           الصور
                        </h4>
                        
                        @foreach ($data->Images as $image)
                            <div class="col-sm-4" id="image{{$image->id}}" style="margin-bottom: 10px">
                                <img src="{{url(''.$image->image)}}" style="width: 100%;height: 150px;">
                                <a onclick="rmvImage('{{$image->id}}')" class="btn btn-danger" id="rmvImage">حذف</a>
                            </div>    
                        @endforeach

                        <div class="col-xs-12"><hr></div>
                        
                        <div class="col-sm-4" style="margin-bottom: 10px">
                            <input type="file" name="images[]" multiple>
                        </div>
                    </div>

                    <!-- board data -->
                    <div class="row tab">
                        <h4 class="custom-modal-title" style="background-color: #4c739a;width:50%;margin-bottom: 15px;margin-top: 15px">
                           طرق التقطيع
                        </h4>
                        
                        @foreach (getCut() as $item)
                            <div class="col-xs-2">
                                <p>{{$item->title_ar}}</p>
                                <div class="checkbox checkbox-custom">
                                    @if(in_array($item->id , $cut_ids)) 
                                        <input  autocomplete="false" checked type="checkbox" name="cut_ids[]" value="{{$item->id}}" data-plugin="switchery" data-color="#08c" data-switchery="true" style="display: none;">
                                    @else
                                        <input  autocomplete="false" type="checkbox" name="cut_ids[]" value="{{$item->id}}" data-plugin="switchery" data-color="#08c" data-switchery="true" style="display: none;">
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="row" style="overflow:auto;">
                        <div class="col-xs-12" style="text-align:center;">
                            <button class="btn btn-success" type="submit" style="width: 50%">حفظ</button>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->

    </div>

@endsection

@section('script')
    <script>
        function rmvImage(id) {
            if(id == '') return false;
            var token = '{{csrf_token()}}';
            $.ajax({
                type     : 'POST',
                url      : '{{ route('rmvImage') }}' ,
                datatype : 'json' ,
                data     : {
                    'id'         :  parseInt(id) ,
                    '_token'     :  token
                }, success   : function(msg){
                    if(msg=='err'){
                        $('#rmvImage').html('لا يمكن حذف اخر صورة');
                        return false;
                    }
                    $('#image'+id).fadeOut();
                }
            });
        }
    </script>
@endsection