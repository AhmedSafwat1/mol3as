@section('styles')

    <style>

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

        .dropify-wrapper{
            height: 150px !important;
        }

        .custombox-modal-container.custombox-modal-container-fadein {
            width: 950px !important;
        }

        a#rmvImage {
            display: list-item;
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    اضافة
@endsection
@section('content')

    <div class="row">

        

        <div class="col-sm-12">
            <div class="card-box table-responsive boxes">

                <!-- /*** start multiable form HTML ***/ -->
                <form action="{{route('addservice')}}" method="POST" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <input  autocomplete="false" type="hidden" name="section_id" value="{{$id}}">
                            
                    <!-- basic data -->
                    <div class="row tab">
                        <h4 class="custom-modal-title" style="background-color: #4c739a;width:50%;margin-bottom: 15px;margin-top: 15px">
                           بيانات أساسية
                        </h4>
                        
                        <div class="col-xs-6">
                            <p><input  autocomplete="false" required class="form-control " value="{{old('title_ar')}}" name="title_ar" placeholder="الاسم بالعربية "></p>
                        </div>

                        <div class="col-xs-6">
                            <p><input  autocomplete="false" required class="form-control" value="{{old('title_en')}}" name="title_en" placeholder="الاسم بالانجليزية "></p>
                        </div>
                        
                        <div class="col-xs-6">
                            <p><input  autocomplete="false" required class="form-control phone" value="{{old('price')}}" name="price" placeholder="السعر لكل كيلو "></p>
                        </div>

                        <div class="col-xs-6">
                            <p><input  autocomplete="false" class="form-control phone" value="{{old('discount')}}" name="discount" placeholder="نسبة الخصم في حالة العروض"></p>
                        </div>

                        <div class="col-xs-12">
                            <p>
                                <textarea name="short_desc_ar" id="short_desc_ar" required class="form-control " cols="30" rows="1" placeholder="وصف مختصر بالعربية">{{old('short_desc_ar')}}</textarea>
                            </p>
                        </div>
                        <div class="col-xs-12">
                            <p>
                                <textarea name="short_desc_en" id="short_desc_en" required class="form-control " cols="30" rows="1" placeholder="وصف مختصر بالانجليزية">{{old('short_desc_en')}}</textarea>
                            </p>
                        </div>
                        <div class="col-xs-12">
                            <p>
                                <textarea name="desc_ar" id="desc_ar" required class="form-control " cols="30" rows="5" placeholder="وصف كامل بالعربية">{{old('desc_ar')}}</textarea>
                            </p>
                        </div>
                        <div class="col-xs-12">
                            <p>
                                <textarea name="desc_en" id="desc_en" required class="form-control " cols="30" rows="5" placeholder="وصف كامل بالانجليزية">{{old('desc_en')}}</textarea>
                            </p>
                        </div>
                    </div>

                    <!-- images -->
                    <div class="row tab">
                        <h4 class="custom-modal-title" style="background-color: #4c739a;width:50%;margin-bottom: 15px;margin-top: 15px">
                           الصور
                        </h4>
                        
                        <div class="col-sm-4" style="margin-bottom: 10px">
                            <input  autocomplete="false" type="file" name="images[]" multiple>
                        </div>
                    </div>

                    <!-- board data -->
                    <div class="row tab">
                        <h4 class="custom-modal-title" style="background-color: #4c739a;width:50%;margin-bottom: 15px;margin-top: 15px">
                           طرق التقطيع
                        </h4>

                        @foreach (getCut() as $item)
                            <div class="col-xs-2">
                                <p>{{$item->title_ar}}</p>
                                <div class="checkbox checkbox-custom">
                                    <input  autocomplete="false" type="checkbox" name="cut_ids[]" value="{{$item->id}}" data-plugin="switchery" data-color="#08c" data-switchery="true" style="display: none;">
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="row" style="overflow:auto;">
                        <div class="col-xs-12" style="text-align:center;">
                            <button class="btn btn-success" type="submit" style="width: 50%">حفظ</button>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->

    </div>

@endsection

@section('script')
    <script>
    </script>
@endsection