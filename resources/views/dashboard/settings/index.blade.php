@section('style')
    <style>
        div.cke_contents {
            height: 300px;
        }
        #map {
            height: 300px;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }

        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        #map #infowindow-content {
            display: inline;
        }

        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }

        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
            margin-bottom: 5px;
            position: absolute;
            top: 40px;
            z-index: 9;
            left: 0;
            right: 0;
            margin: auto;
            border-radius: 3px;
            box-shadow: none;
            border: 1px solid #ddd;
            height: 35px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 25px;
            font-weight: 500;
            padding: 6px 12px;
        }
        #target {
            width: 345px;
        }
    </style>
@endsection
@extends('dashboard.index')

@section('title')
    الإعدادات
@endsection
@section('content')
    
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box card-tabs">
                <ul class="nav nav-pills pull-right">
                    <li class="active">
                        <a href="#site" data-toggle="tab" aria-expanded="true">إعدادات الموقع</a>
                    </li>
                    <li class="">
                        <a href="#social" data-toggle="tab" aria-expanded="true">مواقع التواصل</a>
                    </li>
                    <li class="">
                        <a href="#appLinks" data-toggle="tab" aria-expanded="true">مقدمة التطبيق</a>
                    </li>
                    <li class="">
                        <a href="#about-us" data-toggle="tab" aria-expanded="true">الصفحات الثابتة</a>
                    </li>
                    {{--<li class="">
                        <a href="#SEO" data-toggle="tab" aria-expanded="true">CEO</a>
                    </li>--}}
                </ul>
                <h4 class="header-title m-b-30">الاعدادات</h4>

                <div class="tab-content">
                    <div id="site" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-custom panel-border">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">اعدادت عامة</h3>
                                    </div>
                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post" action="{{route('sitesetting')}}">
                                            {{csrf_field()}}

                                            {{--<div class="form-group">
                                                <label class="col-md-2 control-label" for="example-email">اسم الموقع</label>
                                                <div class="col-md-10">
                                                    <input type="text" id="example-email" value="{{settings('site_name')}}" name="site_name" class="form-control">
                                                </div>
                                            </div>--}}
                                            <div class="form-group">
                                                <label class="col-md-2 control-label" for="example-email">الجوال</label>
                                                <div class="col-md-10">
                                                    <input type="text" id="example-email" value="{{settings('mobile')}}" name="mobile" class="phone form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label" for="example-email">الهاتف</label>
                                                <div class="col-md-10">
                                                    <input type="text" id="example-email" value="{{settings('phone')}}" name="phone" class="phone form-control">
                                                </div>
                                            </div>

                                            {{--  <div class="form-group">
                                                <label class="col-md-2 control-label" for="example-email">الحد الادني للطلب بالكيلو جرام</label>
                                                <div class="col-md-10">
                                                    <input type="text" id="example-email" value="{{settings('limit_kg')}}" name="limit_kg" class="phone form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label" for="example-email">القيمة المضافة لكل طلب</label>
                                                <div class="col-md-10">
                                                    <input type="text" id="example-email" value="{{settings('value_added')}}" name="value_added" class="phone form-control">
                                                </div>
                                            </div>  --}}
                                            
                                            {{--  <div class="form-group">
                                                <label class="col-md-2 control-label" for="example-email">لوجو الموقع</label>
                                                <div class="col-md-10">
                                                    <input type="file" name="site_logo" data-default-file="{{appPath()}}/public/images/site/logo.png" class="dropify photo" data-max-file-size="1M" />
                                                </div>
                                            </div>  --}}
                                            {{--<div class="form-group" style="position: relative;">
                                                <label class="col-md-2 control-label" for="example-email" style="text-align: right;">الموقع على الخريطة</label>
                                                --}}{{-- <div class="col-sm-12">
                                                    
                                                </div> --}}{{--

                                                <input
                                                            style="margin-bottom: 0;
                                                                    position: absolute;
                                                                    top: 67px;
                                                                    left: 0;
                                                                    right: 0;
                                                                    margin: auto;
                                                                    z-index: 9;
                                                                    height: 40px;
                                                                    border-radius: 3px;
                                                                    max-width: 500px;
                                                                    width: 250px;
                                                                    border: 1px solid #4CAF50;
                                                                    text-align: right;"
                                                            class="controls"
                                                            id="pac-input"
                                                            name="pac-input"
                                                            value=""
                                                            placeholder="اكتب اسم المدينة هنا"
                                                    />

                                                <input type="hidden" name="lat" id="lat" value="{{settings('lat')}}" readonly />

                                                <input type="hidden" name="lng" id="lng" value="{{settings('lng')}}" readonly />

                                                <div
                                                        class="col-sm-12"
                                                        style="width: 100%; height: 300px;margin-top: 40px;border: 1px solid #ccc"
                                                        id="add_map"
                                                ></div>
                                            </div>--}}
                                            <button type="submit" class="btn btn-success btn-rounded w-md waves-effect waves-light m-b-5">حفظ</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="social" class="tab-pane fade in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-custom panel-border">
                                            <div class="panel-heading">
                                                <h3 style="display: inline-block;" class="panel-title">مواقع التواصل</h3>
                                                {{-- <button type="button" class="btn btn-custom btn-rounded waves-effect waves-light w-md m-b-5 pull-right" id="openSocialForm">اضافة</button> --}}
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped m-0">
                                                                <thead>
                                                                <tr>
                                                                    <th>الشعار</th>
                                                                    <th>اسم الموقع</th>
                                                                    <th>الرابط</th>
                                                                    <th>التحكم</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr id="addSocial" class="hidden">
                                                                    <form action="{{route('add-social')}}" method="post">
                                                                        {{csrf_field()}}
                                                                        <td>
                                                                            <input required maxlength="190" minlength="2" type="text" name="icon" placeholder="ex" class="form-control" style="width: 189px;">
                                                                        </td>
                                                                        <td>
                                                                            <input required maxlength="190" minlength="2" type="text" name="site_name" placeholder="ex" class="form-control" style="width: 189px;">
                                                                        </td>
                                                                        <td>
                                                                            <input required maxlength="190" minlength="2" type="text" name="url" placeholder="http://www.ex.com" class="form-control" style="width: 189px;">
                                                                        </td>
                                                                        <td>
                                                                            <div class="row">
                                                                                <button type="submit" style="color: #3fb614;background-color: transparent;border: none;"><i class="fa fa-save"></i></button>
                                                                                <button type="button" id="cancel" style="color: #b62626;background-color: transparent;border: none;"><i class="fa fa-close"></i></button>
                                                                            </div>
                                                                        </td>
                                                                    </form>
                                                                </tr>
                                                                <tr id="editSocial" class="hidden">
                                                                    <form action="{{route('update-social')}}" method="post">
                                                                        {{csrf_field()}}
                                                                        <input type="hidden" name="id" value="">
                                                                        <td>
                                                                            <input required maxlength="190" value="" minlength="2" type="text" name="edit_icon" placeholder="ex" class="form-control" style="width: 189px;" readonly>
                                                                        </td>
                                                                        <td>
                                                                            <input required maxlength="190" value="" minlength="2" type="text" name="edit_name" placeholder="ex" class="form-control" style="width: 189px;" readonly>
                                                                        </td>
                                                                        <td>
                                                                            <input required maxlength="190" value="" minlength="2" type="text" name="edit_url" placeholder="http://www.ex.com or user-id" class="form-control" style="width: 189px;">
                                                                        </td>
                                                                        <td>
                                                                            <div class="row">
                                                                                <button type="submit" style="color: #3fb614;background-color: transparent;border: none;"><i class="fa fa-save"></i></button>
                                                                                <button type="button" id="cancelEdit" style="color: #b62626;background-color: transparent;border: none;"><i class="fa fa-close"></i></button>
                                                                            </div>
                                                                        </td>
                                                                    </form>
                                                                </tr>
                                                                @foreach($socials as $social)
                                                                    <tr>
                                                                        <th scope="row"><a href="{{$social->url}}" class="btn btn-{{$social->icon}} btn-rounded btn-small"><i class="fa fa-{{$social->icon}}"></i></a></th>
                                                                        <td>{{$social->site_name}}</td>
                                                                        <td>{{$social->url}}</td>
                                                                        <td>
                                                                            <div class="row">
                                                                                <button type="button" class="editSocialForm" style="color: #3fb614;background-color: transparent;border: none;"
                                                                                        data-id     = "{{$social->id}}"
                                                                                        data-name   = "{{$social->site_name}}"
                                                                                        data-ics   = "{{$social->icon}}"
                                                                                        data-url    = "{{$social->url}}"
                                                                                ><i class="fa fa-edit"></i></button>
                                                                                {{-- <a href="{{route('delete-social', $social->id )}}" style="color: #b62626;background-color: transparent;border: none;"><i class="fa fa-trash"></i></a> --}}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="appLinks" class="tab-pane fade in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-custom panel-border">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">روابط التطبيق</h3>
                                    </div>
                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post" action="{{route('intro')}}">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">المقدمة الاولى بالعربية</label>
                                                <div class="col-md-12">
                                                    <textarea name="first_intro_ar" class="form-control" id="first_intro_ar" cols="30" rows="10">{{settings( 'first_intro_ar')}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">المقدمة الاولى بالانجليزية</label>
                                                <div class="col-md-12">
                                                    <textarea name="first_intro_en" class="form-control" id="first_intro_en" cols="30" rows="10">{{settings( 'first_intro_en')}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">المقدمة الثانية بالعربية</label>
                                                <div class="col-md-12">
                                                    <textarea name="second_intro_ar" class="form-control" id="second_intro_ar" cols="30" rows="10">{{settings( 'second_intro_ar')}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">المقدمة الثانية بالانجليزية</label>
                                                <div class="col-md-12">
                                                    <textarea name="second_intro_en" class="form-control" id="second_intro_en" cols="30" rows="10">{{settings( 'second_intro_en')}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">المقدمة الثالثة بالعربية</label>
                                                <div class="col-md-12">
                                                    <textarea name="third_intro_ar" class="form-control" id="third_intro_ar" cols="30" rows="10">{{settings( 'third_intro_ar')}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">المقدمة الثالثة بالانجليزية</label>
                                                <div class="col-md-12">
                                                    <textarea name="third_intro_en" class="form-control" id="third_intro_en" cols="30" rows="10">{{settings( 'third_intro_en')}}</textarea>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-success btn-rounded w-md waves-effect waves-light m-b-5">حفظ</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="about-us" class="tab-pane fade in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-custom panel-border">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">الصفحات الثابتة</h3>
                                    </div>
                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post" action="{{route('pages')}}">
                                            {{csrf_field()}}

                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">صفحة الشروط والاحكام بالعربية</label>
                                                <div class="col-md-12">
                                                    <textarea name="condition_ar" class="form-control" id="condition_ar" cols="30" rows="10">{{settings( 'condition_ar')}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">صفحة الشروط والاحكام بالانجليزية</label>
                                                <div class="col-md-12">
                                                    <textarea name="condition_en" class="form-control" id="condition_en" cols="30" rows="10">{{settings( 'condition_en')}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">صفحة عن التطبيق بالعربية</label>
                                                <div class="col-md-12">
                                                    <textarea name="about_us_ar" class="form-control" id="about_us_ar" cols="30" rows="10">{{settings( 'about_us_ar')}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">صفحة عن التطبيق بالانجليزية</label>
                                                <div class="col-md-12">
                                                    <textarea name="about_us_en" class="form-control" id="about_us_en" cols="30" rows="10">{{settings( 'about_us_en')}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">سياسة الخصوصية بالعربية</label>
                                                <div class="col-md-12">
                                                    <textarea name="policy_ar" class="form-control" id="policy_ar" cols="30" rows="10">{{settings( 'policy_ar')}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="example-email">سياسة الخصوصية بالانجليزية</label>
                                                <div class="col-md-12">
                                                    <textarea name="policy_en" class="form-control" id="policy_en" cols="30" rows="10">{{settings( 'policy_en')}}</textarea>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-success btn-rounded w-md waves-effect waves-light m-b-5">حفظ</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SEO" class="tab-pane fade in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-custom panel-border">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">SEO</h3>
                                    </div>
                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post" action="{{route('SEO')}}">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <label class="col-md-2 control-label" for="example-email">تفاصيل الموقع</label>
                                                <div class="col-md-10">
                                                    <textarea name="description" class="form-control" id="" cols="30" rows="10">{{settings( 'description')}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label" for="example-email">الكلمات المفتاحية</label>
                                                <div class="col-md-10">
                                                    <textarea name="key_words" class="form-control" id="" cols="30" rows="10">{{settings( 'key_words')}}</textarea>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-success btn-rounded w-md waves-effect waves-light m-b-5">حفظ</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

@endsection

@section('script')
    {{-- Maps --}}
    <script>
        // This example adds a search box to a map, using the Google Place Autocomplete
        // feature. People can enter geographical searches. The search box will return a
        // pick list containing a mix of places and predicted search terms.

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
        function initialize() {
            var lat = parseFloat($('#lat').val());
            var lng = parseFloat($('#lng').val());
            var myLatlng = new google.maps.LatLng(lat, lng);

            var myOptions = {
                zoom: 7,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };

            var map = new google.maps.Map(
                document.getElementById("add_map"),
                myOptions
            );

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                draggable: true,
            });

            var searchBox = new google.maps.places.SearchBox(
                document.getElementById("pac-input")
            );
            google.maps.event.addListener(searchBox, "places_changed", function() {
                var places = searchBox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i, place;
                for (i = 0; (place = places[i]); i++) {
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location);
                }
                map.fitBounds(bounds);
                map.setZoom(12);
            });

            google.maps.event.addListener(marker, "position_changed", function() {
                var lat = marker.getPosition().lat();
                var lng = marker.getPosition().lng();
                $("#lat").val(lat);
                $("#lng").val(lng);
            });
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNm7VC4eQsCZcny5cVteIkg_SMJpc2G7Y&libraries=places&callback=initialize"
            async defer></script>
    <script>
        function checkLength(){
            if(!this.val().length < 280)
                return false;
        }
        // $( function () {
        //     CKEDITOR.replace('who_us_ar');
        // });
        // $( function () {
        //     CKEDITOR.replace('who_us_en');
        // });
        //
        // $( function () {
        //     CKEDITOR.replace('why_us_ar');
        // });
        // $( function () {
        //     CKEDITOR.replace('why_us_en');
        // });
        //
        // $( function () {
        //     CKEDITOR.replace('about_us_ar');
        // });
        // $( function () {
        //     CKEDITOR.replace('about_us_en');
        // });
        //
        // $( function () {
        //     CKEDITOR.replace('condition_ar');
        // });
        // $( function () {
        //     CKEDITOR.replace('condition_en');
        // });
    </script>
@endsection