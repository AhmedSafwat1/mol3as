@section('styles')

    <style>

        .btn-group-justified {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            min-height: 550px;
        }

        .btn-group-justified > .btn, .btn-group-justified > .btn-group {
            display: inline-block;
            width: 30% !important;
            height: 130px!important;
            line-height: 100px!important;
            margin: 0 15px;
            font-size: 18px;
            border-radius: 5px;
        }


        a.btn.waves-effect.btn-lg.waves-light {
            border-color: rgba(0,0,0,.075);
        }

        .waves-light{
            color:white;
        }

        .waves-light:hover{
            color:white !important;
        }

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    الطلبات
@endsection

@section('content')

    <div class="row">

        <div class=" btn-group-justified m-b-10">
            <a href="{{route('neworder')}}" class="btn waves-effect btn-lg waves-light" style="background: #daa21c !important">الطلبات الجديدة</a>
            <a href="{{route('currentorder')}}" class="btn waves-effect btn-lg waves-light" style="background: #10c469 !important">الطلبات الحالية</a>
            <a href="{{route('finishorder')}}" class="btn waves-effect btn-lg waves-light" style="background: #35b8e0 !important">الطلبات المنتهية</a>
            <a href="{{route('todayorder')}}" class="btn waves-effect btn-lg waves-light" style="background: #ed683d !important">طلبات اليوم</a>
            <a href="{{route('tomorroworder')}}" class="btn waves-effect btn-lg waves-light" style="background: #de4d4d !important">طلبات الغد</a>
        </div>
    </div>

@endsection

@section('script')

@endsection