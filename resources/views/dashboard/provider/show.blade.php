@extends('dashboard.index')
@section('title')
عرض طبيب
@endsection

@section('styles')
<style>
    .rates .name-rate {
        display: flex;
        align-items: center;
    }
    .rates .name-rate h6{
        font-weight: bold;
        font-size: 18px;
        margin: 0;
    }
    .rates .name-rate ul{
        padding-right: 10px; 
        display: flex;
        list-style: none;
        margin: 0;
    }
    .yellow{color:#eab204}
    .rates .name-rate ul li{
        margin: 0 5px;
        font-size: 18px;
    }
    .date span{
        color:#989898;
        font-size: 15px;30px
    }
    .rates p{
        font-size: 17px;
        line-height: 30px;
        margin-top: 10px;
    }
    .profile-page {
        background: #fff;
    }

    .user-profile {
        display: flex;
        align-items: center;
    }

    .user-profile .img-profile {
        width: 155px;
        height: 155px;
    }

    .user-profile .img-profile img {
        width: 100%;
        border-radius: 50%;
        height: 100%;
    }

    .user-info {
        text-align: right;
        margin-right: 15px;
    }

    .user-info h4 {
        font-size: 22px;
        font-weight: bold;
    }

    .user-info h4 span:first-of-type {
        color: #3c8dbc;
    }

    .user-info p {
        font-size: 20px;
    }

    .user-info p:last-of-type {
        color: #000;
    }

    .user-info p:last-of-type span:nth-of-type(2) {
        color: #3c8dbc;
    }

    .nav.nav-tabs {
        border-top: 1px solid #dadada;
        display: flex;
        justify-content: space-around;
        border-bottom: 1px solid #dadada;
    }

    .nav.nav-tabs .active a {
        color: #3c8dbc;
        border: none !important;
    }

    .blue-text {
        color: #3c8dbc;
    }

    .price-item {
        display: flex;
        justify-content: space-between;
        align-items: center;
        font-size: 17px;
        border-bottom: 1px solid #dadada;
        padding: 5px 10px;
    }

    .price-item:nth-of-type(odd) {
        background: #f7f7f7;
    }

    .nav.nav-tabs .active a:hover,
    .nav.nav-tabs .active a:focus {
        color: #3c8dbc;
        border: none !important;
    }

    .details {
        list-style: none;
        font-size: 20px;
    }

    .details li {
        display: flex;
        margin-bottom: 10px;
    }

    .details li i {
        padding-left: 10px;
        font-size: 20px;
        width: 40px;
        height: 40p .active ax;
        text-align: .active a center;
    }

    .active a ul {
        list-style: none;
        padding: 0
    }
    .rates .rate-item{
        border-bottom: 1px solid #dadada;
    }
    .rates .rate-item:last-of-type{
        border: none;
    }
    .rates .rate-item {
        border-bottom: 1px solid #dadada;
        padding-top: 10px;
    }

    .rates .rate-item:last-of-type {
        border: none;
    }
</style>
@endsection

@section('content')
<div class="profile-page">
    <div class="user-profile">
        <div class="img-profile">
            <img src="{{appPath()}}/public/images/users/{{$user->avatar}}" alt="">
        </div>
        <div class="user-info">
            <h4>
                <span>{{$user->gender == 'female' ? 'دكتوره' : 'دكتور'}}</span>
                <span>{{$user->name_ar}}</span>
            </h4>
            @if(!is_null($user->Specialist))
                <p>{{$user->Specialist->title_ar}}</p>
            @endif
            <p>
                <span> سعر الكشف : </span>
                <span>{{$user->price}}</span>
                <span>ريال</span>
            </p>
        </div>
    </div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab"
                data-toggle="tab">التفاصيل</a></li>
        <li role="presentation"><a href="#price" aria-controls="price" role="tab" data-toggle="tab">الاسعار</a>
        </li>
        <li role="presentation"><a href="#rate" aria-controls="rate" role="tab" data-toggle="tab">التقييم</a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="details">
            <ul class="details">
                <li>
                    <i class="fa fa-map-marker blue-text" aria-hidden="true"></i>
                    <span>{{$user->address_ar}}</span>
                </li>
                <li>
                    <i class="fa fa-calendar  blue-text" aria-hidden="true"></i>
                    <span>
                        متاح ايام  
                        <ul>
                            @foreach($user->Days as $i=>$day)
                                <li>
                                    <span class="blue-text">
                                        {{$day->Day->title_ar}}
                                    </span> &nbsp;
                                    <span class="blue-text">
                                        من
                                    </span> &nbsp;
                                        {{Carbon\Carbon::parse($day->start_at)->format('h:i')}} &nbsp;
                                        {{Carbon\Carbon::parse($day->start_at)->format('a') == 'am' ? 'صباحا' : 'مساءا'}} &nbsp;
                                    <span class="blue-text">
                                        الى
                                    </span> &nbsp;
                                        {{Carbon\Carbon::parse($day->end_at)->format('h:i')}} &nbsp;
                                        {{Carbon\Carbon::parse($day->end_at)->format('a') == 'am' ? 'صباحا' : 'مساءا'}}
                                </li>
                            @endforeach
                        </ul>
                    </span>
                </li>
                @if(!is_null($user->desc))
                    <li>
                        <i class="fa fa-info-circle  blue-text" aria-hidden="true"></i>
                        <span>
                            {{$user->desc}}
                        </span>
                    </li>
                @endif
            </ul>
        </div>
        <div role="tabpanel" class="tab-pane" id="price">
            <div class="price-tab">
                @foreach ($user->Prices as $item)
                    <div class="price-item d-flex justify-content-between">
                        <p>{{$item->title_ar}}</p>
                        <p class="blue-text">
                            {{$item->price}} ريال
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="rate">
            <div class="rates">
                @foreach ($user->Rates as $rate)
                    <div class="rate-item">
                        <div class="name-rate d-flex">
                            <h6>{{is_null($rate->From)?'تم حذفه' : $rate->From->name_ar}}</h6>
                            <?php 
                                $yellow = $rate->rate;
                                $gray   = 5 - $yellow;
                            ?>
                            <ul class="d-flex">
                                @for ($i = 1; $i <= $yellow; $i++)
                                    <li>
                                        <i class="fa fa-star yellow"></i>
                                    </li>
                                @endfor

                                @for ($i = 1; $i <= $gray; $i++)
                                    <li>
                                        <i class="fa fa-star gray-star"></i>
                                    </li>
                                @endfor
                            </ul>
                        </div>
                        <div class="date">
                            <span>{{Carbon\Carbon::parse($rate->created_at)->format('Y-m-d')}}</span>
                        </div>
                        <p>
                            {{$rate->comment}}
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection