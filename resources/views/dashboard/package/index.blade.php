@section('styles')

    <style>

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    الباقات
@endsection
@section('content')

    <div class="row">

        <div class=" btn-group-justified m-b-10">
            <a href="#add" class="btn btn-success waves-effect btn-lg waves-light" data-animation="fadein" data-plugin="custommodal"
                data-overlaySpeed="100" data-overlayColor="#36404a">اضافة باقة <i class="fa fa-plus"></i> </a>
            <a href="#deleteAll" class="btn btn-danger waves-effect btn-lg waves-light delete-all" data-animation="blur" data-plugin="custommodal"
                data-overlaySpeed="100" data-overlayColor="#36404a">حذف المحدد <i class="fa fa-trash"></i> </a>
            <a class="btn btn-primary waves-effect btn-lg waves-light" onclick="window.location.reload()" role="button">تحديث الصفحة <i class="fa fa-refresh"></i> </a>
        </div>

        <div class="col-sm-12">
            <div class="card-box table-responsive boxes">

                <table id="datatable" class="table table-bordered table-responsives">
                    <thead>
                    <tr>
                        <th>
                            تحديد
                            <input type="checkbox" id="checkedAll" style="margin-right: 10px">
                        </th>
                        <th> عنوان الباقة</th>
                        <th> سعر الباقة</th>
                        <th> مدة الباقة</th>
                        <th>تاريخ التسجيل</th>
                        <th>التحكم</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">
                    @foreach($data as $item)
                        <tr>
                            <td>
                                <input type="checkbox" class="form-check-label checkSingle" id="{{$item->id}}">
                            </td>

                            <td>{{ $item->title_ar }}</td>
                            <td>{{ $item->price }}</td>
                            <td>{{ $item->day}} يوم  {{ $item->month}} شهر  {{ $item->year}} سنة </td>
                            <td>{{$item->created_at->diffForHumans()}}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="{{route('edit_package',$item->id)}}" class="btn btn-success"{{-- data-animation="fadein" data-plugin="custommodal"
                                        data-overlaySpeed="100" data-overlayColor="#36404a" style="color: #c89e28; font-weight: bold;"
                                        data-id = "{{$item->id}}"
                                        data-title_ar = "{{$item->title_ar}}"
                                        data-title_en = "{{$item->title_en}}"
                                        data-desc_ar = "{{$item->desc_ar}}"
                                        data-desc_en = "{{$item->desc_en}}"
                                        data-price = "{{$item->price}}"
                                        data-year = "{{$item->year}}"
                                        data-month = "{{$item->month}}"
                                        data-day = "{{$item->day}}"
                                        data-font_color = "{{$item->font_color}}"
                                        data-background_color = "{{$item->background_color}}"--}}
                                    >
                                        <i class="fa fa-cogs"></i>
                                    </a>
                                    <a href="#delete" class="delete btn btn-danger" style="color: #c83338; font-weight: bold;" data-animation="blur" data-plugin="custommodal"
                                        data-overlaySpeed="100" data-overlayColor="#36404a"
                                        data-id = "{{$item->id}}"
                                    >
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- end col -->

    </div>

    <!-- add item modal -->
    <div id="add" class="modal-demo" >
        <button type="button" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title" style="background-color: #36404a">
            باقة جديدة
        </h4>
        <form action="{{route('addpackage')}}" method="post" autocomplete="off" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="modal-body">
                <div class="row" style="margin-top: 15px;">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">العنوان بالعربية</label>
                            <input type="text" autocomplete="nope" id="" name="title_ar" required class="form-control" 
                            oninvalid="this.setCustomValidity('قم بادخال العنوان بالعربية')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">العنوان بالانجليزية</label>
                            <input type="text" autocomplete="nope" id="" name="title_en" required class="form-control" 
                            oninvalid="this.setCustomValidity('قم بادخال العنوان بالانجليزية')" oninput="setCustomValidity('')">
                        </div>
                    </div>  

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">السعر</label>
                            <input type="text" autocomplete="nope" id="" name="price" required class="form-control phone" 
                            oninvalid="this.setCustomValidity('قم بادخال السعر')" oninput="setCustomValidity('')">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">يوم</label>
                            <input type="number" min="0" max="31" value="0" autocomplete="nope" id="" name="day" required class="form-control" 
                            oninvalid="this.setCustomValidity('يجب ادخال عدد الايام من 0 الى 31')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">شهر</label>
                            <input type="number" min="0" max="12" value="0" autocomplete="nope" id="" name="month" required class="form-control" 
                            oninvalid="this.setCustomValidity('يجب ادخال عدد الشهور من 0 الى 12')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">سنة</label>
                            <input type="number" min="0" max="100" value="0" autocomplete="nope" id="" name="year" required class="form-control" 
                            oninvalid="this.setCustomValidity('ارجو ادخال عدد السنوات')" oninput="setCustomValidity('')">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">لون الخط</label>
                            <input type="color" autocomplete="nope" id="font_color" name="font_color" required class="form-control"
                                   oninvalid="this.setCustomValidity('يجب اختيار لون الخط')" oninput="setCustomValidity('')" onchange="change_style('color')">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-4 control-label">لون الخلفية</label>
                            <input type="color" autocomplete="nope" id="background_color" name="background_color" required class="form-control"
                                   oninvalid="this.setCustomValidity('يجب اختيار لون الخلفية')" oninput="setCustomValidity('')" onchange="change_style('background')">
                        </div>
                    </div>

                    <div class="col-md-6" style="">
                        <label for=""></label>
                        <div class="form-group" id="test-div" style="padding: 10px;border: 1px solid #dadada">
                            <p style="text-align: center;margin: 0 !important;font-size: 16px;font-weight: bold"> Test Color</p>
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-top: 10px">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-sm-12 control-label">التفاصيل بالعربية</label>
                                <textarea name="desc_ar" class="form-control" id="desc_ar" cols="30" rows="10" required
                                          oninvalid="this.setCustomValidity('قم بادخال التفاصيل بالعربية')" oninput="setCustomValidity('')">       </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-sm-12 control-label">التفاصيل بالانجليزية</label>
                                <textarea name="desc_en" class="form-control" id="desc_en" cols="30" rows="10" required
                                          oninvalid="this.setCustomValidity('قم بادخال التفاصيل بالانجليزية')" oninput="setCustomValidity('')">       </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect waves-light">اضافة</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal" onclick="Custombox.close();">رجوع</button>
            </div>
        </form>
    </div>

   <div id="delete" class="modal-demo" style="position:relative; right: 32%">
        <button type="button" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title">حذف باقة</h4>
        <div class="custombox-modal-container" style=" height: 160px;">
            <div class="row">
                <div class="col-sm-12">
                    <h3 style="margin-top: 35px">
                        هل تريد مواصلة عملية الحذف ؟
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form action="{{route('deletepackage')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="delete_id" value="">
                        <button style="margin-top: 35px" type="submit" class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5 send-delete-all"  style="margin-top: 19px">حـذف</button>
                    </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div>

    <div id="deleteAll" class="modal-demo" style="position:relative; right: 32%">
        <button type="button" id="close-deleteAll" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title">حذف المحدد</h4>
        <div class="custombox-modal-container" style=" height: 160px;">
            <div class="row">
                <div class="col-sm-12">
                    <h3 style="margin-top: 35px">
                        هل تريد مواصلة عملية الحذف ؟
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button style="margin-top: 35px" type="submit" class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5 send-delete-all" style="margin-top: 19px">حـذف</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div>

@endsection

@section('script')
    <script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#desc_ar' ) )
            .catch( error => {
                console.error( error );
            } );

        ClassicEditor
            .create( document.querySelector( '#desc_en' ) )
            .catch( error => {
                console.error( error );
            } );

        function change_style() {
            var font_color = $('#font_color').val();
            var background_color = $('#background_color').val();
            // alert()
            $('#test-div').attr('style' , 'padding: 10px;border: 1px solid #dadada;color:'+font_color+';background:'+background_color);
        }

        $('.delete').on('click',function(){

            let id         = $(this).data('id');

            $("input[name='delete_id']").val(id);

        });

        $("#checkedAll").change(function(){
            if(this.checked){
                $(".checkSingle").each(function(){
                    this.checked=true;
                })
            }else{
                $(".checkSingle").each(function(){
                    this.checked=false;
                })
            }
        });

        $(".checkSingle").click(function () {
            if ($(this).is(":checked")){
                var isAllChecked = 0;
                $(".checkSingle").each(function(){
                    if(!this.checked)
                        isAllChecked = 1;
                })
                if(isAllChecked == 0){ $("#checkedAll").prop("checked", true); }
            }else {
                $("#checkedAll").prop("checked", false);
            }
        });

        $('.send-delete-all').on('click', function (e) {
            var itemsIds = [];
            $('.checkSingle:checked').each(function () {
                var id = $(this).attr('id');
                itemsIds.push({
                    id: id,
                });
            });
            var requestData = JSON.stringify(itemsIds);
            // console.log(requestData);
            if (itemsIds.length > 0) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{{route('deletepackages')}}",
                    data: {data: requestData, _token: '{{csrf_token()}}'},
                    success: function( msg ) {
                        if (msg == 'success') {
                            location.reload();
                        }else{
                            $('#close-deleteAll').trigger('click');
                        }
                    }
                });
            }else{
                $('#close-deleteAll').trigger('click');
            }
        });
    </script>

@endsection