@section('styles')

    <style>

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    تعديل باقة
@endsection
@section('content')

    <div class="row">

        <div class="col-sm-12">
            <div class="card-box table-responsive boxes">

                <h4 class="custom-modal-title" style="background-color: #36404a">
                    تعديل باقة
                </h4>
                <form action="{{route('updatepackage')}}" method="post" autocomplete="off" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row" style="margin-top: 15px;">
                        <input type="hidden" name="id" value="{{$data->id}}">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-4 control-label">العنوان بالعربية</label>
                                <input type="text" autocomplete="nope" id="" name="title_ar" required class="form-control"
                                       oninvalid="this.setCustomValidity('قم بادخال العنوان بالعربية')" oninput="setCustomValidity('')" value="{{$data->title_ar}}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-4 control-label">العنوان بالانجليزية</label>
                                <input type="text" autocomplete="nope" id="" name="title_en" value="{{$data->title_en}}" required class="form-control"
                                       oninvalid="this.setCustomValidity('قم بادخال العنوان بالانجليزية')" oninput="setCustomValidity('')">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-4 control-label">السعر</label>
                                <input type="text" autocomplete="nope" id="" name="price" value="{{$data->price}}" required class="form-control phone"
                                       oninvalid="this.setCustomValidity('قم بادخال السعر')" oninput="setCustomValidity('')">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-4 control-label">يوم</label>
                                <input type="number" min="0" max="31" autocomplete="nope" id="" name="day" value="{{(int)$data->day}}" required class="form-control"
                                       oninvalid="this.setCustomValidity('يجب ادخال عدد الايام من 0 الى 31')" oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-4 control-label">شهر</label>
                                <input type="number" min="0" max="12" autocomplete="nope" id="" name="month" value="{{(int)$data->month}}" required class="form-control"
                                       oninvalid="this.setCustomValidity('يجب ادخال عدد الشهور من 0 الى 12')" oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-4 control-label">سنة</label>
                                <input type="number" min="0" max="100" autocomplete="nope" id="" name="year" value="{{(int)$data->year}}" required class="form-control"
                                       oninvalid="this.setCustomValidity('ارجو ادخال عدد السنوات')" oninput="setCustomValidity('')">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-4 control-label">لون الخط</label>
                                <input type="color" autocomplete="nope" id="font_color" name="font_color" required class="form-control"
                                       oninvalid="this.setCustomValidity('يجب اختيار لون الخط')" oninput="setCustomValidity('')" value="{{$data->font_color}}" onchange="change_style('color')">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="field-1" class="col-sm-4 control-label">لون الخلفية</label>
                                <input type="color" autocomplete="nope" id="background_color" name="background_color" required class="form-control"
                                       oninvalid="this.setCustomValidity('يجب اختيار لون الخلفية')" oninput="setCustomValidity('')" value="{{$data->background_color}}" onchange="change_style('background')">
                            </div>
                        </div>

                        <div class="col-md-6" style="">
                            <label for=""></label>
                            <div class="form-group" id="test-div" style="padding: 10px;border: 1px solid #dadada;color:{{$data->font_color}};background:{{$data->background_color}}">
                                <p style="text-align: center;margin: 0 !important;font-size: 16px;font-weight: bold"> Test Color</p>
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="col-sm-12 control-label">التفاصيل بالعربية</label>
                                    <textarea name="desc_ar" class="form-control" id="desc_ar" cols="30" rows="10" required
                                              oninvalid="this.setCustomValidity('قم بادخال التفاصيل بالعربية')" oninput="setCustomValidity('')">{{$data->desc_ar}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="col-sm-12 control-label">التفاصيل بالانجليزية</label>
                                    <textarea name="desc_en" class="form-control" id="desc_en" cols="30" rows="10" required
                                              oninvalid="this.setCustomValidity('قم بادخال التفاصيل بالانجليزية')" oninput="setCustomValidity('')">{{$data->desc_en}}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 20px">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success form-control">تعديل</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->

    </div>

@endsection

@section('script')

    <script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#desc_ar' ) )
            .catch( error => {
                console.error( error );
            } );

        ClassicEditor
            .create( document.querySelector( '#desc_en' ) )
            .catch( error => {
                console.error( error );
            } );

        function change_style() {
            var font_color = $('#font_color').val();
            var background_color = $('#background_color').val();
            // alert()
            $('#test-div').attr('style' , 'padding: 10px;border: 1px solid #dadada;color:'+font_color+';background:'+background_color);
        }

        function changeChecked(id) {
            var tokenv  = "{{csrf_token()}}";
            $.ajax({
                type     : 'POST',
                url      : '{{ route('change-activation') }}' ,
                datatype : 'json' ,
                data     : {
                    'id'         :  id ,
                    '_token'     :  tokenv
                }, success   : function(msg){
                    //success here
                    if(msg == 0)
                        return false;
                }
            });
        }

        $('.edit').on('click',function(){
            //get valus
            let id              = $(this).data('id');
            let title_ar        = $(this).data('title_ar');
            let title_en        = $(this).data('title_en');
            let desc_ar         = $(this).data('desc_ar');
            let desc_en         = $(this).data('desc_en');
            let price           = $(this).data('price');
            let year            = $(this).data('year');
            let month           = $(this).data('month');
            let day             = $(this).data('day');

            $("input[name='id']").val(id);
            $("#title_ar").val(title_ar);
            $("#title_en").val(title_en);
            $("#desc_ar").html(desc_ar);
            $("#desc_en").html(desc_en);
            $("#price").val(price);
            $("#year").val(year);
            $("#month").val(month);
            $("#day").val(day);
        });

        $('.delete').on('click',function(){

            let id         = $(this).data('id');

            $("input[name='delete_id']").val(id);

        });

        $("#checkedAll").change(function(){
            if(this.checked){
                $(".checkSingle").each(function(){
                    this.checked=true;
                })
            }else{
                $(".checkSingle").each(function(){
                    this.checked=false;
                })
            }
        });

        $(".checkSingle").click(function () {
            if ($(this).is(":checked")){
                var isAllChecked = 0;
                $(".checkSingle").each(function(){
                    if(!this.checked)
                        isAllChecked = 1;
                })
                if(isAllChecked == 0){ $("#checkedAll").prop("checked", true); }
            }else {
                $("#checkedAll").prop("checked", false);
            }
        });

        $('.send-delete-all').on('click', function (e) {
            var itemsIds = [];
            $('.checkSingle:checked').each(function () {
                var id = $(this).attr('id');
                itemsIds.push({
                    id: id,
                });
            });
            var requestData = JSON.stringify(itemsIds);
            // console.log(requestData);
            if (itemsIds.length > 0) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{{route('deletepackages')}}",
                    data: {data: requestData, _token: '{{csrf_token()}}'},
                    success: function( msg ) {
                        if (msg == 'success') {
                            location.reload();
                        }else{
                            $('#close-deleteAll').trigger('click');
                        }
                    }
                });
            }else{
                $('#close-deleteAll').trigger('click');
            }
        });
    </script>

@endsection