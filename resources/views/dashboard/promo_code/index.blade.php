@section('styles')

    <style>

        @media (max-width: 475.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: none;
                text-align: center;
            }

            .boxes .col-sm-6 {
                float:  none;
                text-align: center;
                display:  inline-block;
                width:  10px;
            }
        }

        @media (min-width: 476px) and (max-width: 767.98px) {
            .boxes .col-sm-6 div#datatable_filter {
                float: right;
            }

            .boxes .col-sm-6 {
                float:  right;
                display:  inline-block;
                width:  50%;
            }
        }

    </style>
@endsection

@extends('dashboard.index')
@section('title')
    اكواد الخصم
@endsection
@section('content')

    <div class="row">

        <div class=" btn-group-justified m-b-10">
            <a href="#add" class="btn btn-success waves-effect btn-lg waves-light" data-animation="fadein" data-plugin="custommodal"
                data-overlaySpeed="100" data-overlayColor="#36404a">اضافة كود <i class="fa fa-plus"></i> </a>
            <a href="#deleteAll" class="btn btn-danger waves-effect btn-lg waves-light delete-all" data-animation="blur" data-plugin="custommodal"
                data-overlaySpeed="100" data-overlayColor="#36404a">حذف المحدد <i class="fa fa-trash"></i> </a>
            <a class="btn btn-primary waves-effect btn-lg waves-light" onclick="window.location.reload()" role="button">تحديث الصفحة <i class="fa fa-refresh"></i> </a>
        </div>

        <div class="col-sm-12">
            <div class="card-box table-responsive boxes">

                <table id="datatable" class="table table-bordered table-responsives">
                    <thead>
                    <tr>
                        <th>
                            تحديد
                            <input type="checkbox" id="checkedAll" style="margin-right: 10px">
                        </th>
                        <th>الكود</th>
                        <th>نسبة الخصم</th>
                        <th>الصلاحية</th>
                        <th>تاريخ التسجيل</th>
                        <th>التحكم</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">
                    @foreach($data as $item)
                        <tr>
                            <td>
                                <input type="checkbox" class="form-check-label checkSingle" id="{{$item->id}}">
                            </td>
                            
                            <td>{{$item->code}}</td>
                            <td>{{$item->discount}}</td>
                            <td>
                                @if ($item->type==0)
                                    الاستخدام مرة واحدة فقظ
                                @elseif ($item->type==1)
                                    الاستخدام مرة واحدة لكل عضو
                                @elseif($item->type==2)
                                    الاستخدام بدون حدود
                                @endif
                            </td>
                            <td>{{$item->created_at->diffForHumans()}}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="#edit" class="edit btn btn-success" data-animation="fadein" data-plugin="custommodal"
                                        data-overlaySpeed="100" data-overlayColor="#36404a" style="color: #c89e28; font-weight: bold;"
                                        data-id         = "{{$item->id}}"
                                        data-code       = "{{$item->code}}"
                                        data-discount   = "{{$item->discount}}"
                                        data-type       = "{{$item->type}}"
                                        data-section_id = "{{$item->section_id}}"
                                    >
                                        <i class="fa fa-cogs"></i>
                                    </a>
                                    <a href="#delete" class="delete btn btn-danger" style="color: #c83338; font-weight: bold;" data-animation="blur" data-plugin="custommodal"
                                        data-overlaySpeed="100" data-overlayColor="#36404a"
                                        data-id = "{{$item->id}}"
                                    >
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- end col -->

    </div>

    <!-- add item modal -->
    <div id="add" class="modal-demo" >
        <button type="button" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title" style="background-color: #36404a">
            كود جديدة
        </h4>
        <form action="{{route('addpromo_code')}}" method="post" autocomplete="off" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="modal-body">

                <div class="row">
                    {{--  <div class="col-md-6">
                        <div class="form-group" style="position: relative;">
                            <label for="field-2" class="control-label">الكود</label>
                            <input type="text" autocomplete="nope" value="" name="code" id="" required class="form-control" placeholder="الكود" style="padding-right: 125px;">
                        </div>
                    </div>  --}}
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="field-1" class="control-label">نسبة الخصم %</label>
                            <input type="name" autocomplete="nope" name="discount" id="" required class="form-control phone" placeholder="نسبة الخصم">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" >
                        <div class="form-group" style="display: flex">
                            <label>اختار صلاحية الكود</label>
                        </div>
                    </div>

                    <div class="col-md-4" >
                        <div class="form-group" style="display: flex">
                            <input type="radio" name="type" id="type_00" value="0" class="" style="margin-left:10px">
                            <label for="type_00" class="radio-inline" >الاستخدام مرة واحدة فقظ</label>
                        </div>
                    </div>

                    <div class="col-md-4" >
                        <div class="form-group" style="display: flex">
                            <input type="radio" name="type" id="type_11" value="1" class="" style="margin-left:10px">
                            <label for="type_11" class="radio-inline" >الاستخدام مرة واحدة لكل عضو</label>
                        </div>
                    </div>

                    <div class="col-md-4" >
                        <div class="form-group" style="display: flex">
                            <input type="radio" name="type" id="type_22" value="2" class="" style="margin-left:10px">
                            <label for="type_22" class="radio-inline" >الاستخدام بدون حدود</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" >
                        <div class="form-group" style="display: flex">
                            <label>اختار قسم خاص بهذا الكود -- اختياري</label>
                        </div>
                    </div>

                    @foreach (getSection() as $item)
                        <div class="col-md-3" >
                            <div class="form-group" style="display: flex">
                                <input type="radio" name="section_id" id="services_{{$item->id}}x" value="{{$item->id}}" class="" style="margin-left:10px">
                                <label for="services_{{$item->id}}x" class="radio-inline" >{{$item->title_ar}}</label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect waves-light">اضافة</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal" onclick="Custombox.close();">رجوع</button>
            </div>
        </form>
    </div>

    <!-- edit item modal -->
    <div id="edit" class="modal-demo">
        <button type="button" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title" style="background-color: #36404a">
            تعديل <span id="itemname"></span>
        </h4>
        <form action="{{route('updatepromo_code')}}" method="post" autocomplete="off" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="id" value="">
            <div class="modal-body">
                <div class="row">
                    {{--  <div class="col-md-6">
                        <div class="form-group" style="position: relative;">
                            <label for="field-2" class="control-label">الكود</label>
                            <input type="text" autocomplete="nope" value="" name="code" id="code" required class="form-control" placeholder="رقم الهاتف" style="padding-right: 125px;">
                        </div>
                    </div>  --}}
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="field-1" class="control-label">نسبة الخصم</label>
                            <input type="name" autocomplete="nope" name="discount" id="discount" required class="form-control" placeholder="العنوان">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" >
                        <div class="form-group" style="display: flex">
                            <label>اختار صلاحية الكود</label>
                        </div>
                    </div>

                    <div class="col-md-4" >
                        <div class="form-group" style="display: flex">
                            <input type="radio" name="type" id="services_0" value="0" class="" style="margin-left:10px">
                            <label for="services_0" class="radio-inline" >الاستخدام مرة واحدة فقظ</label>
                        </div>
                    </div>

                    <div class="col-md-4" >
                        <div class="form-group" style="display: flex">
                            <input type="radio" name="type" id="services_1" value="1" class="" style="margin-left:10px">
                            <label for="services_1" class="radio-inline" >الاستخدام مرة واحدة لكل عضو</label>
                        </div>
                    </div>

                    <div class="col-md-4" >
                        <div class="form-group" style="display: flex">
                            <input type="radio" name="type" id="services_2" value="2" class="" style="margin-left:10px">
                            <label for="services_2" class="radio-inline" >الاستخدام بدون حدود</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" >
                        <div class="form-group" style="display: flex">
                            <label>اختار قسم خاص بهذا الكود -- اختياري</label>
                        </div>
                    </div>

                    @foreach (getSection() as $item)
                        <div class="col-md-3">
                            <div class="form-group" style="display: flex">
                                <input type="radio" name="section_id" id="section_{{$item->id}}" value="{{$item->id}}" class="days_chbox" style="margin-left:10px">
                                <label for="section_{{$item->id}}" class="radio-inline" >{{$item->title_ar}}</label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect waves-light">تعديل</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal" onclick="Custombox.close();">رجوع</button>
            </div>
        </form>
    </div>

   <div id="delete" class="modal-demo" style="position:relative; right: 32%">
        <button type="button" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title">حذف كود</h4>
        <div class="custombox-modal-container" style=" height: 160px;">
            <div class="row">
                <div class="col-sm-12">
                    <h3 style="margin-top: 35px">
                        هل تريد مواصلة عملية الحذف ؟
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form action="{{route('deletepromo_code')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="delete_id" value="">
                        <button style="margin-top: 35px" type="submit" class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5 send-delete-all"  style="margin-top: 19px">حـذف</button>
                    </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div>

    <div id="deleteAll" class="modal-demo" style="position:relative; right: 32%">
        <button type="button" id="close-deleteAll" class="close" onclick="Custombox.close();" style="opacity: 1">
            <span>&times</span><span class="sr-only" style="color: #f7f7f7">Close</span>
        </button>
        <h4 class="custom-modal-title">حذف المحدد</h4>
        <div class="custombox-modal-container" style=" height: 160px;">
            <div class="row">
                <div class="col-sm-12">
                    <h3 style="margin-top: 35px">
                        هل تريد مواصلة عملية الحذف ؟
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button style="margin-top: 35px" type="submit" class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5 send-delete-all" style="margin-top: 19px">حـذف</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div>

@endsection

@section('script')

    <script>
        $('.edit').on('click',function(){
            //get valus
            let id          = $(this).data('id');
            let code        = $(this).data('code');
            let discount    = $(this).data('discount');
            let type        = $(this).data('type');
            let section_id  = $(this).data('section_id');

            $("input[name='id']").val(id);
            $("#code").val(code);
            $("#discount").val(discount);
            $("#services_"+type).prop("checked", true);
            $("#section_"+section_id).prop("checked", true);
        });

        $('.delete').on('click',function(){

            let id         = $(this).data('id');

            $("input[name='delete_id']").val(id);

        });

        $("#checkedAll").change(function(){
            if(this.checked){
                $(".checkSingle").each(function(){
                    this.checked=true;
                })
            }else{
                $(".checkSingle").each(function(){
                    this.checked=false;
                })
            }
        });

        $(".checkSingle").click(function () {
            if ($(this).is(":checked")){
                var isAllChecked = 0;
                $(".checkSingle").each(function(){
                    if(!this.checked)
                        isAllChecked = 1;
                })
                if(isAllChecked == 0){ $("#checkedAll").prop("checked", true); }
            }else {
                $("#checkedAll").prop("checked", false);
            }
        });

        $('.send-delete-all').on('click', function (e) {
            var itemsIds = [];
            $('.checkSingle:checked').each(function () {
                var id = $(this).attr('id');
                itemsIds.push({
                    id: id,
                });
            });
            var requestData = JSON.stringify(itemsIds);
            // console.log(requestData);
            if (itemsIds.length > 0) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{{route('deletepromo_codes')}}",
                    data: {data: requestData, _token: '{{csrf_token()}}'},
                    success: function( msg ) {
                        if (msg == 'success') {
                            location.reload();
                        }else{
                            $('#close-deleteAll').trigger('click');
                        }
                    }
                });
            }else{
                $('#close-deleteAll').trigger('click');
            }
        });
    </script>

@endsection