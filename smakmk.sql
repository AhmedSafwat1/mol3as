-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2019 at 05:07 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smakmk`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_settings`
--

INSERT INTO `app_settings` (`id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 'site_name', 'Happy', '2019-03-17 17:12:52', '2019-03-28 23:25:51'),
(2, 'address', '', NULL, '2019-04-18 14:20:29'),
(3, 'phone', '0546589050', NULL, '2019-03-19 00:54:35'),
(4, 'email', 'smakmk@info.com', NULL, '2019-03-19 00:54:35'),
(5, 'about_us_ar', '<p>&nbsp; &nbsp; يعتبر التطبيق واحد من التطبيقات الرائدة في مجال الخدمات المقدمة من خلال الطلب من اكثر من مقدم خدمة نعمل في هذا التطبيق علي تلبية&nbsp;إحتياجات&nbsp;العملاء بمختلف شرائحهم بحيث نقدم لعملائنا ما يتوافق مع تطلعاتهم و يحقق أهدافهم من خلال مجموعة من المحترفين من مقدمين الخدمات و موظفين مختصين لخدمة العملاء ليلا نهارا.</p>', NULL, '2019-04-18 14:40:33'),
(6, 'description', '', NULL, '2019-04-18 14:37:59'),
(7, 'key_words', '', NULL, '2019-04-18 14:37:59'),
(8, 'lat', '21.485811', NULL, '2019-04-18 14:17:57'),
(9, 'lng', '39.19250480000005', NULL, '2019-04-18 14:17:57'),
(10, 'about_us_en', '<p>&nbsp; &nbsp; يعتبر التطبيق واحد من التطبيقات الرائدة في مجال الخدمات المقدمة من خلال الطلب من اكثر من مقدم خدمة نعمل في هذا التطبيق علي تلبية&nbsp;إحتياجات&nbsp;العملاء بمختلف شرائحهم بحيث نقدم لعملائنا ما يتوافق مع تطلعاتهم و يحقق أهدافهم من خلال مجموعة من المحترفين من مقدمين الخدمات و موظفين مختصين لخدمة العملاء ليلا نهارا.</p>', NULL, '2019-04-18 14:40:33'),
(11, 'site_logo', 'logo.png', '2019-03-28 23:25:26', '2019-03-28 23:25:26'),
(16, 'condition_ar', '<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br />\r\nإذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br />\r\nومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.<br />\r\nهذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>', NULL, '2019-04-18 14:40:33'),
(17, 'condition_en', '<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.<br />\r\nإذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.<br />\r\nومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.<br />\r\nهذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>', NULL, '2019-04-18 14:40:33'),
(18, 'limit_kg', '2', NULL, '2019-04-18 14:17:57'),
(19, 'value_added', '10', NULL, '2019-04-18 14:17:57'),
(24, 'mobile', '0546589060', NULL, NULL),
(25, 'policy_ar', '', NULL, NULL),
(26, 'policy_en', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `plate_number` varchar(100) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `cut_id` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT 'kg',
  `price` double NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `delivery` double NOT NULL DEFAULT '0',
  `country_id` int(11) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `title_ar`, `title_en`, `delivery`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'الرياض', 'reyad', 0, 1, '2019-04-17 12:03:21', '2019-04-17 10:03:21'),
(2, 'جدة', 'geda', 0, 1, '2019-04-17 12:03:50', '2019-04-17 10:03:50'),
(3, 'القاهرة', 'cairo', 0, 2, '2019-04-17 12:03:38', '2019-04-17 10:03:38');

-- --------------------------------------------------------

--
-- Table structure for table `city_days`
--

CREATE TABLE `city_days` (
  `id` int(11) NOT NULL,
  `available_at` time DEFAULT NULL,
  `day_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `city_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` text,
  `file` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `title`, `message`, `file`, `type`, `seen`, `created_at`, `updated_at`) VALUES
(1, 'test name', 'test@info.com', '966123456789', 'test title', 'test the first msg from DB to adminPanel and i hope its be secure and success !!', NULL, 0, 1, '2019-03-20 22:00:00', '2019-03-21 14:23:50'),
(2, 'abdelrahman101', 'ar@info.com', '96698765432101', NULL, 'test send msg from post man to admin panel', NULL, 0, 1, '2019-04-21 11:16:01', '2019-04-21 11:17:10');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `code` varchar(100) DEFAULT '966',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `title_ar`, `title_en`, `flag`, `code`, `created_at`, `updated_at`) VALUES
(1, 'السعودية', 'saudi arabia', NULL, '966', '2019-03-20 16:50:32', '2019-04-17 10:00:44'),
(2, 'مصر', 'Egypt', NULL, '02', '2019-03-20 17:16:39', '2019-04-17 10:00:23');

-- --------------------------------------------------------

--
-- Table structure for table `cuts`
--

CREATE TABLE `cuts` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `price` double NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(100) DEFAULT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `iso_ar` varchar(50) DEFAULT NULL,
  `iso_en` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `title_ar`, `title_en`, `iso_ar`, `iso_en`, `created_at`, `updated_at`) VALUES
(1, 'السبت', 'saturday', 'اس', 'st', '2019-07-14 14:30:00', '2019-07-14 14:30:00'),
(2, 'الاحد', 'sunday', 'اح', 'su', '2019-07-14 14:30:00', '2019-07-14 14:30:00'),
(3, 'الاثنين', 'monday', 'اث', 'mo', '2019-07-14 14:30:00', '2019-07-14 14:30:00'),
(4, 'الثلاثاء', 'tuesday', 'ثل', 'tu', '2019-07-14 14:30:00', '2019-07-14 14:30:00'),
(5, 'الاربعاء', 'wednesday', 'ار', 'we', '2019-07-14 14:30:00', '2019-07-14 14:30:00'),
(6, 'الخميس', 'thursday', 'خم', 'th', '2019-07-14 14:30:00', '2019-07-14 14:30:00'),
(7, 'الجمعة', 'friday', 'جم', 'fr', '2019-07-14 14:30:00', '2019-07-14 14:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `dealers`
--

CREATE TABLE `dealers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `total_sales` double DEFAULT '0',
  `pay_done` double NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `device_type` varchar(50) DEFAULT NULL COMMENT 'android , ios',
  `device_id` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_06_26_110013_create_roles_table', 1),
(4, '2018_06_26_110120_create_permissions_table', 1),
(5, '2018_07_01_104552_create_reports_table', 1),
(6, '2018_07_01_123905_create_app_seetings_table', 1),
(7, '2018_07_02_074616_create_socials_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `neighborhoods`
--

CREATE TABLE `neighborhoods` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `neighborhoods`
--

INSERT INTO `neighborhoods` (`id`, `title_ar`, `title_en`, `country_id`, `city_id`, `created_at`, `updated_at`) VALUES
(1, 'حي العليا', NULL, 1, 1, '2019-03-20 18:24:58', '2019-03-20 18:24:58'),
(2, 'الشرق', NULL, 1, 1, '2019-03-20 18:25:17', '2019-03-20 18:25:17'),
(3, 'النور', NULL, 1, 1, '2019-03-20 18:25:28', '2019-03-20 18:25:28'),
(4, 'الروابي', NULL, 1, 2, '2019-03-20 18:25:53', '2019-03-20 18:25:53'),
(5, 'حي العضيان', NULL, 1, 2, '2019-03-20 18:26:10', '2019-03-20 18:26:10'),
(6, 'جاردن سيتي', NULL, 2, 3, '2019-03-20 18:27:03', '2019-03-20 18:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `from_id` int(10) UNSIGNED DEFAULT NULL,
  `to_id` int(10) UNSIGNED DEFAULT NULL,
  `message_ar` text,
  `message_en` text,
  `order_id` int(11) DEFAULT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `confirm` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(100) NOT NULL DEFAULT 'order' COMMENT 'order , admin',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `desc_ar` text,
  `desc_en` text,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=offer , 1=competion',
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=disactive , 1=active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_number` varchar(255) DEFAULT NULL,
  `promo_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `provider_id` int(10) UNSIGNED DEFAULT NULL,
  `neighborhood_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `month` varchar(100) DEFAULT NULL,
  `year` varchar(100) DEFAULT NULL,
  `day_id` int(11) DEFAULT NULL,
  `other_data` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `pay_done` tinyint(4) NOT NULL DEFAULT '0',
  `price` varchar(255) DEFAULT '0',
  `cut` double DEFAULT '0',
  `delivery` int(11) NOT NULL,
  `value_added` int(11) NOT NULL,
  `total_before_promo` int(11) NOT NULL,
  `total_after_promo` int(11) NOT NULL,
  `lat` varchar(100) DEFAULT NULL,
  `lng` varchar(100) DEFAULT NULL,
  `payment_method` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=cash , 1=onLine',
  `status` int(1) DEFAULT '0' COMMENT '0=new , 1=agree , 2=refused , 3=hasProvider , 4=in-way , 5=finishByClient , 6=finishByProvider',
  `cancel` tinyint(1) NOT NULL DEFAULT '0',
  `user_seen` tinyint(1) NOT NULL DEFAULT '1',
  `provider_seen` tinyint(1) NOT NULL DEFAULT '1',
  `admin_seen` tinyint(1) NOT NULL DEFAULT '1',
  `user_finish` tinyint(1) DEFAULT '0',
  `provider_finish` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_options`
--

CREATE TABLE `order_options` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `cut_id` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT 'kg',
  `price` double NOT NULL DEFAULT '0',
  `dealer_id` int(11) UNSIGNED DEFAULT NULL,
  `buy_total` double NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `permissions` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permissions`, `role_id`, `created_at`, `updated_at`) VALUES
(722, 'dashboard', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(723, 'permissionslist', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(724, 'addpermissionspage', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(725, 'addpermission', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(726, 'editpermissionpage', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(727, 'updatepermission', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(728, 'deletepermission', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(729, 'admins', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(730, 'addadmin', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(731, 'updateadmin', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(732, 'deleteadmin', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(733, 'deleteadmins', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(734, 'users', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(735, 'adduser', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(736, 'updateuser', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(737, 'deleteuser', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(738, 'deleteusers', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(739, 'providers', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(740, 'addprovider', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(741, 'updateprovider', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(742, 'deleteprovider', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(743, 'deleteproviders', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(744, 'settings', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(745, 'sitesetting', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(746, 'add-social', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(747, 'update-social', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(748, 'delete-social', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(749, 'app_links', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(750, 'pages', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(751, 'SEO', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(752, 'sections', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(753, 'addsection', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(754, 'updatesection', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(755, 'deletesection', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(756, 'deletesections', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(757, 'showservices', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(758, 'countries', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(759, 'addcountry', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(760, 'updatecountry', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(761, 'deletecountry', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(762, 'deletecountries', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(763, 'cities', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(764, 'addcity', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(765, 'updatecity', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(766, 'deletecity', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(767, 'deletecities', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(768, 'orders', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(769, 'neworder', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(770, 'refusedorder', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(771, 'finishorder', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(772, 'deleteorder', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(773, 'contacts', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(774, 'showcontact', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(775, 'sendcontact', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(776, 'deletecontact', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(777, 'deletecontacts', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(778, 'reports', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13'),
(779, 'deletereports', 1, '2019-04-21 07:07:13', '2019-04-21 07:07:13');

-- --------------------------------------------------------

--
-- Table structure for table `promo_codes`
--

CREATE TABLE `promo_codes` (
  `id` int(11) NOT NULL,
  `code` varchar(100) NOT NULL,
  `discount` double NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=oneUseForOneUser , 1=oneUseForAllUsers',
  `section_id` int(11) DEFAULT NULL,
  `used_by` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `desc_ar` text,
  `desc_en` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` int(11) NOT NULL,
  `from_id` int(10) UNSIGNED DEFAULT NULL,
  `to_id` int(10) UNSIGNED DEFAULT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `comment` text,
  `show` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'show in says about us',
  `report` tinyint(4) NOT NULL DEFAULT '0',
  `reson_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `from_id`, `to_id`, `rate`, `comment`, `show`, `report`, `reson_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 5, 'test', 0, 0, NULL, '2019-07-16 22:54:09', '2019-07-16 22:54:09');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `event` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supervisor` int(11) NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `event`, `supervisor`, `ip`, `country`, `city`, `area`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'قام admin بتحديث روابط التطبيق', 1, '::1', '', '', '', 1, '2019-04-21 07:07:37', '2019-04-21 07:07:37'),
(2, 'قام admin بالسماح لعضو بأضافة خدمات', 1, '::1', '', '', '', 1, '2019-04-22 08:01:22', '2019-04-22 08:01:22'),
(3, 'قام admin بمنع مزود بأضافة خدمات', 1, '::1', '', '', '', 1, '2019-04-22 08:01:22', '2019-04-22 08:01:22');

-- --------------------------------------------------------

--
-- Table structure for table `report_resons`
--

CREATE TABLE `report_resons` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'مدير عام', '2019-03-17 17:12:51', '2019-03-17 17:12:51');

-- --------------------------------------------------------

--
-- Table structure for table `says`
--

CREATE TABLE `says` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `comment` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `title_ar`, `title_en`, `image`, `active`, `created_at`, `updated_at`) VALUES
(3, 'ألعاب', 'Games', '/public/images/section/18-04-1915555979731094596823.gif', 1, '2019-04-18 12:32:53', '2019-04-18 12:32:53');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `short_desc_ar` text,
  `short_desc_en` text,
  `desc_en` text,
  `desc_ar` text,
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT 'kg',
  `price` double DEFAULT '0',
  `discount` double NOT NULL DEFAULT '0',
  `special` tinyint(4) NOT NULL DEFAULT '0',
  `new` tinyint(4) NOT NULL DEFAULT '0',
  `rate` varchar(100) NOT NULL DEFAULT '0',
  `lat` varchar(100) DEFAULT NULL,
  `lng` varchar(100) DEFAULT NULL,
  `request_number` int(11) DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `section_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `service_cuts`
--

CREATE TABLE `service_cuts` (
  `id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `cut_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `service_dealers`
--

CREATE TABLE `service_dealers` (
  `id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `dealer_id` int(10) UNSIGNED DEFAULT NULL,
  `amount` int(11) NOT NULL DEFAULT '1' COMMENT 'kg',
  `price` double DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `service_images`
--

CREATE TABLE `service_images` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `service_options`
--

CREATE TABLE `service_options` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `service_prices`
--

CREATE TABLE `service_prices` (
  `id` int(11) NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `price` varchar(255) NOT NULL DEFAULT '0',
  `service_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

CREATE TABLE `socials` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socials`
--

INSERT INTO `socials` (`id`, `site_name`, `url`, `icon`, `created_at`, `updated_at`) VALUES
(2, 'facebook', 'http://www.facebook.com', 'facebook', '2019-03-17 18:24:15', '2019-03-17 18:24:15'),
(3, 'twitter', 'http://www.twitter.com', 'twitter', '2019-03-17 18:24:54', '2019-03-17 18:24:54'),
(4, 'instagram', 'http://www.instagram.com', 'instagram', '2019-03-17 18:25:17', '2019-03-17 18:26:47'),
(5, 'whatsapp', 'http://www.whatsapp.com', 'whatsapp', '2019-04-18 12:51:24', '2019-04-18 12:51:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `provider` tinyint(1) NOT NULL DEFAULT '0',
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `checked` int(11) NOT NULL DEFAULT '0',
  `activation` tinyint(4) NOT NULL DEFAULT '1',
  `notify_send` tinyint(4) NOT NULL DEFAULT '1',
  `city_id` int(11) DEFAULT NULL,
  `neighborhood_id` int(11) DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `lat` decimal(16,14) DEFAULT NULL,
  `lng` decimal(16,14) DEFAULT NULL,
  `device_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'android',
  `lang` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ar',
  `total_sales` double DEFAULT '0',
  `pay_done` double DEFAULT '0',
  `total_profit` double NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `code`, `avatar`, `provider`, `device_id`, `active`, `checked`, `activation`, `notify_send`, `city_id`, `neighborhood_id`, `role`, `lat`, `lng`, `device_type`, `lang`, `total_sales`, `pay_done`, `total_profit`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'aait@aait.sa', '$2y$10$nQ3urUipUw7pT6LFBhd1peWg4XIygCMY.ihro7CnJ/wVAZZlY9O92', '96612345678', '4347', 'default.png', 0, '11111', 1, 1, 1, 1, NULL, NULL, 1, NULL, NULL, 'android', 'ar', 0, 0, 0, 'L3jUCOuLBnbdemWmJBvNLe80abpeqG1lO9CkBoQYqVt67wNgylwXbmrfqUFe', '2019-03-16 22:00:00', '2019-03-30 06:44:51'),
(24, 'no3man', NULL, '$2y$10$S5dH.Ay4IbvvcTC4I0GIru/WAFbtVp2ZD/AtCFtU.cUv3kQ.0p3v6', '0546589050', '1234', 'default.png', 0, NULL, 0, 1, 0, 1, NULL, NULL, 0, '12.02500000000000', '11.52100000000000', 'android', 'ar', 0, 0, 0, NULL, '2019-07-16 15:31:26', '2019-07-16 15:31:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_days`
--
ALTER TABLE `city_days`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_id` (`city_id`),
  ADD KEY `day_id` (`day_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuts`
--
ALTER TABLE `cuts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dealers`
--
ALTER TABLE `dealers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neighborhoods`
--
ALTER TABLE `neighborhoods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_id` (`city_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `to_id` (`to_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `city_id` (`city_id`),
  ADD KEY `neighborhood_id` (`neighborhood_id`);

--
-- Indexes for table `order_options`
--
ALTER TABLE `order_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `service_id` (`service_id`) USING BTREE,
  ADD KEY `dealer_id` (`dealer_id`),
  ADD KEY `cut_id` (`cut_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `promo_codes`
--
ALTER TABLE `promo_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `to_id` (`to_id`),
  ADD KEY `reson_id` (`reson_id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_user_id_foreign` (`user_id`);

--
-- Indexes for table `report_resons`
--
ALTER TABLE `report_resons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `says`
--
ALTER TABLE `says`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `section_id` (`section_id`);

--
-- Indexes for table `service_cuts`
--
ALTER TABLE `service_cuts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_dealers`
--
ALTER TABLE `service_dealers`
  ADD KEY `dealer_id` (`dealer_id`);

--
-- Indexes for table `service_images`
--
ALTER TABLE `service_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `service_options`
--
ALTER TABLE `service_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `service_prices`
--
ALTER TABLE `service_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_id` (`city_id`),
  ADD KEY `neighborhood_id` (`neighborhood_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `city_days`
--
ALTER TABLE `city_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cuts`
--
ALTER TABLE `cuts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `dealers`
--
ALTER TABLE `dealers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `neighborhoods`
--
ALTER TABLE `neighborhoods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_options`
--
ALTER TABLE `order_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=780;

--
-- AUTO_INCREMENT for table `promo_codes`
--
ALTER TABLE `promo_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `report_resons`
--
ALTER TABLE `report_resons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `says`
--
ALTER TABLE `says`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service_cuts`
--
ALTER TABLE `service_cuts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service_images`
--
ALTER TABLE `service_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service_options`
--
ALTER TABLE `service_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service_prices`
--
ALTER TABLE `service_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `socials`
--
ALTER TABLE `socials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `city_days`
--
ALTER TABLE `city_days`
  ADD CONSTRAINT `city_days_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `city_days_ibfk_2` FOREIGN KEY (`day_id`) REFERENCES `days` (`id`);

--
-- Constraints for table `favourites`
--
ALTER TABLE `favourites`
  ADD CONSTRAINT `favourites_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `favourites_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `neighborhoods`
--
ALTER TABLE `neighborhoods`
  ADD CONSTRAINT `neighborhoods_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`from_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`to_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notifications_ibfk_3` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `offers`
--
ALTER TABLE `offers`
  ADD CONSTRAINT `offers_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `orders_ibfk_4` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `orders_ibfk_5` FOREIGN KEY (`neighborhood_id`) REFERENCES `neighborhoods` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `order_options`
--
ALTER TABLE `order_options`
  ADD CONSTRAINT `order_options_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `order_options_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `order_options_ibfk_3` FOREIGN KEY (`dealer_id`) REFERENCES `dealers` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `order_options_ibfk_4` FOREIGN KEY (`cut_id`) REFERENCES `cuts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rates`
--
ALTER TABLE `rates`
  ADD CONSTRAINT `rates_ibfk_1` FOREIGN KEY (`from_id`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `rates_ibfk_2` FOREIGN KEY (`to_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rates_ibfk_3` FOREIGN KEY (`reson_id`) REFERENCES `report_resons` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_ibfk_2` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_dealers`
--
ALTER TABLE `service_dealers`
  ADD CONSTRAINT `service_dealers_ibfk_1` FOREIGN KEY (`dealer_id`) REFERENCES `dealers` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `service_images`
--
ALTER TABLE `service_images`
  ADD CONSTRAINT `service_images_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_options`
--
ALTER TABLE `service_options`
  ADD CONSTRAINT `service_options_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_prices`
--
ALTER TABLE `service_prices`
  ADD CONSTRAINT `service_prices_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`neighborhood_id`) REFERENCES `neighborhoods` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
