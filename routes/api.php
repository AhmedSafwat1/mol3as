<?php

use Illuminate\Support\Facades\Route;

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

Route::get('/load-users', 'Admin\UsersController@loadUsers')->name('api.load-users');

/*
|--------------------------------------------------------------------------
| API Routes - 8/2019 - AbdelRahman Mohammed
|--------------------------------------------------------------------------
*/

/***************************** Commen Page Start *****************************/

/* Start AuthController */

#sign-up & Login &logout
Route::any('register', 'AuthController@Register');
Route::any('login', 'AuthController@Login');
Route::any('logout', 'AuthController@logout'); // user and provider
#Code For Active User & resend
Route::any('active_code', 'AuthController@ActiveCode');
Route::any('resend_active_code', 'AuthController@resendActiveCode');
#Forget & Reset Password 
Route::any('check_phone', 'AuthController@checkPhone'); // user and provider
Route::any('check_code', 'AuthController@checkCode');

/* End AuthController */


/* Start UserController */

#User_profile
Route::any('User_profile', 'UserController@showProfile');
Route::any('User_profile/update', 'UserController@updateProfile');
#Reset_new_password
Route::any('update_password', 'UserController@updatePassword');
#update lang & notify_allow
Route::any('user-setting', 'UserController@editLang');

/* End UserController */


/* Start ApiController */

#Update Device Id
Route::any('update/device_id', 'ApiController@updateDeviceId');
#questions
Route::any('questions', 'ApiController@questions');
#intros
Route::any('intros', 'ApiController@intros');
#about_app
Route::any('about-app', 'ApiController@about_app');
#condition
Route::any('condition', 'ApiController@condition');
#policy
Route::any('policy', 'ApiController@policy');
#contact
Route::any('contact-info', 'ApiController@Contact_Info');
Route::any('contact/send', 'ApiController@Contact_Send');

/****************************** Commen Page End *****************************/


/****************************** Logic Page Start ***************************/

################################data start###############################
#show days
Route::any('days/show', 'ApiController@show_all_days');
#show packages
Route::any('packages/show', 'ApiController@show_all_packages');
# all cities
Route::any('cities/show', 'ApiController@show_all_cities');
# all specialists
Route::any('specialists/show', 'ApiController@show_all_specialists');
# all specialists
Route::any('specialists-and-cities/show', 'ApiController@show_all_cities_and_specialists');
################################data start###############################



######################################################################## 
##############################Doctor start############################## 

##############################profile
#show doctor profile
Route::any('show-doctor-profile', 'ApiController@show_doctor_profile');
#show doctor rate
Route::any('show-doctor-rate', 'ApiController@show_doctor_rate');
#complete doctor data
Route::any('complete-doctor-data', 'ApiController@store_doctor');
#update doctor data
Route::any('update-doctor-data', 'ApiController@update_doctor_data');
#update doctor package
Route::any('update-doctor-package', 'ApiController@update_doctor_package');
#update doctor days
Route::any('update-doctor-days', 'ApiController@update_doctor_days');
#update doctor socials
Route::any('update-doctor-socials', 'ApiController@update_doctor_socials');
#store doctor prices
Route::any('store-doctor-prices', 'ApiController@store_doctor_prices');
#update doctor prices
Route::any('update-doctor-prices', 'ApiController@update_doctor_prices');
#delete doctor prices
Route::any('delete-doctor-prices', 'ApiController@delete_doctor_prices');

##############################Offer
#show doctor offer
Route::any('show-doctor-offer', 'ApiController@show_doctor_offer');
#store doctor offer
Route::any('store-doctor-offer', 'ApiController@store_doctor_offer');
#update doctor offer
Route::any('update-doctor-offer', 'ApiController@update_doctor_offer');
#delete doctor offer
Route::any('delete-doctor-offer', 'ApiController@delete_doctor_offer');

##############################report rate and comment
Route::any('report-comment', 'ApiController@report_comment');

##############################Doctor End############################### 
####################################################################### 



##############################Client Start############################# 
#######################################################################

##############################Doctor
#home
Route::any('client-home', 'ApiController@home');
#search about doctor by name
Route::any('client-search-doctor', 'ApiController@searchDoctors');
#filter doctors
Route::any('client-filter-doctor', 'ApiController@filterDoctors');
#show doctor profile
Route::any('client-show-doctor', 'ApiController@showDoctor');
#favoutite or remove favourite about Doctor
Route::any('client-favourite-doctor', 'ApiController@favoutiteDoctor');
#rate doctor
Route::any('client-rate-doctor', 'ApiController@rateDoctor');

##############################Offer
#show doctor offer
Route::any('show-doctor-offers', 'ApiController@show_doctor_offers');
#show offers
Route::any('client-show-offer', 'ApiController@showOffers');
#search about offer by title
Route::any('client-search-offer', 'ApiController@searchOffers');
#filter offers
Route::any('client-filter-offer', 'ApiController@filterOffers');
#show offer
Route::any('client-show-offer-details', 'ApiController@showOffer');
#rate offer
Route::any('client-rate-offer', 'ApiController@rateOffer');

##############################favourite
#show favourites
Route::any('client-show-favourites', 'ApiController@showFavourites');

##############################Client End############################### 
####################################################################### 

##############################chat start########################## 
#show all chats
Route::any('show-all-chats', 'ApiController@show_all_chats');
#show chat
Route::any('show-chat', 'ApiController@show_chat');
#store chat
Route::any('store-chat', 'ApiController@store_chat');
##############################chat end##############################

##############################notification start########################## 
#show all notification
Route::any('all-notification/show', 'ApiController@show_all_notification');
#delete notification
Route::any('notification/delete', 'ApiController@delete_notification');
##############################notification end##############################

/***************************** Logic Page End *****************************/
/* End ApiController */
