<?php

use Illuminate\Support\Facades\Route;
use function GuzzleHttp\json_encode;

//Genrate bcrypt 123456

Route::get('/hash', function () {
    return bcrypt(123456);
});

Route::get('/print', function () {
    return checkAvaliable(1, 10);
});



//test some code
Route::get('/test-code', function () {
    $data = [
        [
            'title' => 'test title 1',
            'price' => '20'
        ], [
            'title' => 'test title 2',
            'price' => '15'
        ]
    ];

    $data = json_encode($data);
    return $data;
});



// Site Index
Route::get('/', function () {
    return redirect('/admin');
});



// Dashboard
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

    Route::get('/login', 'AuthController@loginForm')->name('loginForm');

    Route::post('/login', 'AuthController@login')->name('login');

    /**** ajax routes ****/
    #change-checked
    Route::post('change-checked', 'UsersController@change_checked')->name('change-checked');
    Route::post('change-special', 'sectionController@change_special')->name('change-special');
    Route::post('change-addService', 'providersController@change_addService')->name('change-addService');
    Route::post('change-addService-notfiy', 'providersController@change_addService_notfiy')->name('change-addService-notfiy');
    Route::post('change-activation', 'sliderController@change_activation')->name('change-activation');
    Route::post('change-ad-activation', 'adController@change_ad_activation')->name('change-ad-activation');
    Route::post('change-active', 'sectionController@change_active')->name('change-active');
    Route::post('showProviderDetails', 'providersController@showProviderDetails')->name('showProviderDetails');
    Route::post('showProviderBill', 'providersController@showProviderBill')->name('showProviderBill');
    Route::post('rmvImage', 'serviceController@rmvImage')->name('rmvImage');
    Route::post('change-special-service', 'serviceController@change_special_service')->name('change-special-service');
    Route::post('change-new-service', 'serviceController@change_new_service')->name('change-new-service');
    Route::post('showOrderPayDetails', 'finicial_accountController@showOrderPayDetails')->name('showOrderPayDetails');

    // Auth user only
    Route::group(['middleware' => ['admin', 'check_role']], function () {

        Route::get('/', [
            'uses' => 'AuthController@dashboard',
            'as' => 'dashboard',
            'icon' => '<i class="fa fa-dashboard"></i>',
            'title' => 'الرئيسيه'
        ]);

        // ============= Permission ==============
        Route::get('permissions-list', [
            'uses' => 'PermissionController@index',
            'as' => 'permissionslist',
            'title' => 'قائمة الصلاحيات',
            'icon' => '<i class="fa fa-eye"></i>',
            'child' => [
                'addpermissionspage',
                'addpermission',
                'editpermissionpage',
                'updatepermission',
                'deletepermission',

            ]
        ]);

        Route::get('permissions', [
            'uses' => 'PermissionController@create',
            'as' => 'addpermissionspage',
            'title' => 'اضافة صلاحيه',
        ]);

        Route::post('add-permission', [
            'uses' => 'PermissionController@store',
            'as' => 'addpermission',
            'title' => 'تمكين اضافة صلاحيه'
        ]);

        #edit permissions page
        Route::get('edit-permissions/{id}', [
            'uses' => 'PermissionController@edit',
            'as' => 'editpermissionpage',
            'title' => 'تعديل صلاحيه'
        ]);

        #update permission
        Route::post('update-permission', [
            'uses' => 'PermissionController@update',
            'as' => 'updatepermission',
            'title' => 'تمكين تعديل صلاحيه'
        ]);

        #delete permission
        Route::post('delete-permission', [
            'uses' => 'PermissionController@destroy',
            'as' => 'deletepermission',
            'title' => 'حذف صلاحيه'
        ]);

        // ========== Settings

        Route::get('settings', [
            'uses' => 'SettingController@index',
            'as' => 'settings',
            'title' => 'الاعدادات',
            'icon' => '<i class="fa fa-cogs"></i>',
            'child' => [
                'sitesetting',
                'add-social',
                'update-social',
                'delete-social',
                'intro',
                'pages',
            ]
        ]);

        // General Settings
        Route::post('/update-settings', [
            'uses' => 'SettingController@updateSiteInfo',
            'as' => 'sitesetting',
            'title' => 'تعديل بيانات الموقع'
        ]);

        Route::post('/pages', [
            'uses' => 'SettingController@pages',
            'as' => 'pages',
            'title' => 'تعديل الصفحات الثابتة'
        ]);

        Route::post('/app_links', [
            'uses' => 'SettingController@app_links',
            'as' => 'app_links',
            'title' => 'تعديل روابط التطبيق'
        ]);

        Route::post('/intro', [
            'uses' => 'SettingController@intro',
            'as' => 'intro',
            'title' => 'تعديل مقدمة التطبيق'
        ]);

        // Social Sites
        Route::post('/add-social', [
            'uses' => 'SettingController@storeSocial',
            'as' => 'add-social',
            'title' => 'اضافة مواقع التواصل'
        ]);

        Route::post('/update-social', [
            'uses' => 'SettingController@updateSocial',
            'as' => 'update-social',
            'title' => 'تعديل مواقع التواصل'
        ]);

        Route::get('/delete-social/{id}', [
            'uses' => 'SettingController@deleteSocial',
            'as' => 'delete-social',
            'title' => 'حذف مواقع التواصل'
        ]);

        Route::post('/seo', [
            'uses' => 'SettingController@SEO',
            'as' => 'SEO',
            'title' => 'تعديل بيانات ال SEO'
        ]);

        #admins
        Route::get('/admins', [
            'uses' => 'AdminController@index',
            'as' => 'admins',
            'title' => 'المشرفين',
            'icon' => '<i class="fa fa-user-circle"></i>',
            'child' => [
                'addadmin',
                'updateadmin',
                'deleteadmin',
                'deleteadmins',
            ]
        ]);

        Route::post('/add-admin', [
            'uses' => 'AdminController@store',
            'as' => 'addadmin',
            'title' => 'اضافة مشرف'
        ]);

        // Update User
        Route::post('/update-admin', [
            'uses' => 'AdminController@update',
            'as' => 'updateadmin',
            'title' => 'تعديل مشرف'
        ]);

        // Delete User
        Route::post('/delete-admin', [
            'uses' => 'AdminController@delete',
            'as' => 'deleteadmin',
            'title' => 'حذف مشرف'
        ]);

        // Delete Users
        Route::post('/delete-admins', [
            'uses' => 'AdminController@deleteAllAdmins',
            'as' => 'deleteadmins',
            'title' => 'حذف اكتر من مشرف'
        ]);

        #users
        Route::get('/users', [
            'uses' => 'UsersController@index',
            'as' => 'users',
            'title' => 'الاعضاء ',
            'icon' => '<i class="fa fa-users"></i>',
            'child' => [
                'deleteuser',
                'deleteusers',
            ]
        ]);

        // Add User
        Route::post('/add-user', [
            'uses' => 'UsersController@store',
            'as' => 'adduser',
            'title' => 'اضافة عضو'
        ]);

        // Update User
        Route::post('/update-user', [
            'uses' => 'UsersController@update',
            'as' => 'updateuser',
            'title' => 'تعديل عضو'
        ]);

        // Delete User
        Route::post('/delete-user', [
            'uses' => 'UsersController@delete',
            'as' => 'deleteuser',
            'title' => 'حذف عضو'
        ]);

        // Delete Users
        Route::post('/delete-users', [
            'uses' => 'UsersController@deleteAll',
            'as' => 'deleteusers',
            'title' => 'حذف اكتر من عضو'
        ]);

        // Send Notify
        Route::post('/send-notify', [
            'uses' => 'UsersController@sendNotify',
            'as' => 'send-fcm',
            'title' => 'ارسال اشعارات'
        ]);

        #providers
        Route::get('/providers', [
            'uses' => 'providersController@index',
            'as' => 'providers',
            'title' => 'الاطباء ',
            'icon' => '<i class="fa fa-users"></i>',
            'child' => [
                'showprovider',
                'deleteprovider',
                'deleteproviders',
            ]
        ]);

        // Add provider
        Route::post('/add-provider', [
            'uses' => 'providersController@store',
            'as' => 'addprovider',
            'title' => 'اضافة طبيب'
        ]);

        // Update provider
        Route::post('/update-provider', [
            'uses' => 'providersController@update',
            'as' => 'updateprovider',
            'title' => 'تعديل طبيب'
        ]);

        // show provider
        Route::get('/show-provider/{id}', [
            'uses' => 'providersController@show',
            'as' => 'showprovider',
            'title' => 'عرض طبيب'
        ]);

        // Delete provider
        Route::post('/delete-provider', [
            'uses' => 'providersController@delete',
            'as' => 'deleteprovider',
            'title' => 'حذف طبيب'
        ]);

        // Delete providers
        Route::post('/delete-providers', [
            'uses' => 'providersController@deleteAll',
            'as' => 'deleteproviders',
            'title' => 'حذف اكتر من طبيب'
        ]);


        #package
        Route::get('/packages', [
            'uses' => 'packageController@index',
            'as' => 'packages',
            'title' => 'الباقات ',
            'icon' => '<i class="fa fa-dollar"></i>',
            'child' => [
                'addpackage',
                'edit_package',
                'updatepackage',
                'deletepackage',
                'deletepackages',
            ]
        ]);

        // Add package
        Route::post('/add-package', [
            'uses' => 'packageController@store',
            'as' => 'addpackage',
            'title' => 'اضافة باقة'
        ]);

        // Edit package
        Route::get('/edit-package/{id}', [
            'uses' => 'packageController@edit',
            'as' => 'edit_package',
            'title' => 'صفحة تعديل باقة'
        ]);

        // Update package
        Route::post('/update-package', [
            'uses' => 'packageController@update',
            'as' => 'updatepackage',
            'title' => 'تعديل باقة'
        ]);

        // Delete package
        Route::post('/delete-package', [
            'uses' => 'packageController@delete',
            'as' => 'deletepackage',
            'title' => 'حذف باقة'
        ]);

        // Delete packages
        Route::post('/delete-packages', [
            'uses' => 'packageController@deleteAll',
            'as' => 'deletepackages',
            'title' => 'حذف اكتر من باقة'
        ]);


        #specialist
        Route::get('/specialists', [
            'uses' => 'specialistController@index',
            'as' => 'specialists',
            'title' => 'التخصصات',
            'icon' => '<i class="fa fa-briefcase"></i>',
            'child' => [
                'addspecialist',
                'updatespecialist',
                'deletespecialist',
                'deletespecialists',
            ]
        ]);

        // Add specialist
        Route::post('/add-specialist', [
            'uses' => 'specialistController@store',
            'as' => 'addspecialist',
            'title' => 'اضافة تخصص'
        ]);

        // Update specialist
        Route::post('/update-specialist', [
            'uses' => 'specialistController@update',
            'as' => 'updatespecialist',
            'title' => 'تعديل تخصص'
        ]);

        // Delete specialist
        Route::post('/delete-specialist', [
            'uses' => 'specialistController@delete',
            'as' => 'deletespecialist',
            'title' => 'حذف تخصص'
        ]);

        // Delete specialists
        Route::post('/delete-specialists', [
            'uses' => 'specialistController@deleteAll',
            'as' => 'deletespecialists',
            'title' => 'حذف اكتر من تخصص'
        ]);


        #country
        //        Route::get('/countries', [
        //            'uses' => 'countryController@index',
        //            'as' => 'countries',
        //            'title' => 'الدول',
        //            'icon' => '<i class="fa fa-globe"></i>',
        //            'child' => [
        //                'addcountry',
        //                'updatecountry',
        //                'deletecountry',
        //                'deletecountries',
        //            ]
        //        ]);
        //
        //        Route::post('/add-country', [
        //            'uses' => 'countryController@store',
        //            'as' => 'addcountry',
        //            'title' => 'اضافة دولة'
        //        ]);
        //
        //        Route::post('/update-country', [
        //            'uses' => 'countryController@update',
        //            'as' => 'updatecountry',
        //            'title' => 'تعديل دولة'
        //        ]);
        //
        //        Route::post('/delete-country', [
        //            'uses' => 'countryController@delete',
        //            'as' => 'deletecountry',
        //            'title' => 'حذف دولة'
        //        ]);
        //
        //        Route::post('/delete-countries', [
        //            'uses' => 'countryController@deleteAll',
        //            'as' => 'deletecountries',
        //            'title' => 'حذف اكتر من دولة'
        //        ]);


        #city
        Route::get('/cities', [
            'uses' => 'cityController@index',
            'as' => 'cities',
            'title' => 'المدن',
            'icon' => '<i class="fa fa-globe"></i>',
            'child' => [
                'addcity',
                'updatecity',
                'deletecity',
                'deletecities',
            ]
        ]);

        Route::post('/add-city', [
            'uses' => 'cityController@store',
            'as' => 'addcity',
            'title' => 'اضافة مدينة'
        ]);

        Route::post('/update-city', [
            'uses' => 'cityController@update',
            'as' => 'updatecity',
            'title' => 'تعديل مدينة'
        ]);

        Route::post('/delete-city', [
            'uses' => 'cityController@delete',
            'as' => 'deletecity',
            'title' => 'حذف مدينة'
        ]);

        Route::post('/delete-cities', [
            'uses' => 'cityController@deleteAll',
            'as' => 'deletecities',
            'title' => 'حذف اكتر من مدينة'
        ]);

        #neighborhood
        // Route::get('/neighborhoods', [
        //     'uses' => 'neighborhoodController@index',
        //     'as' => 'neighborhoods',
        //     'title' => 'الحي',
        //     'icon' => '<i class="fa fa-globe"></i>',
        //     'child' => [
        //         'addneighborhood',
        //         'updateneighborhood',
        //         'deleteneighborhood',
        //         'deleteneighborhoods',
        //     ]
        // ]);

        // Route::post('/add-neighborhood', [
        //     'uses' => 'neighborhoodController@store',
        //     'as' => 'addneighborhood',
        //     'title' => 'اضافة الحي'
        // ]);

        // Route::post('/update-neighborhood', [
        //     'uses' => 'neighborhoodController@update',
        //     'as' => 'updateneighborhood',
        //     'title' => 'تعديل الحي'
        // ]);

        // Route::post('/delete-neighborhood', [
        //     'uses' => 'neighborhoodController@delete',
        //     'as' => 'deleteneighborhood',
        //     'title' => 'حذف الحي'
        // ]);

        // Route::post('/delete-neighborhoods', [
        //     'uses' => 'neighborhoodController@deleteAll',
        //     'as' => 'deleteneighborhoods',
        //     'title' => 'حذف اكتر من الحي'
        // ]);

        #rate
        Route::get('/rates', [
            'uses' => 'rateController@index',
            'as' => 'rates',
            'title' => 'تعليقات العملاء المسيئة',
            'icon' => '<i class="fa fa-star"></i>',
            'child' => [
                'activerate',
                'deleterate',
                'deleterates',
            ]
        ]);

        // active rate
        Route::post('/active-rate', [
            'uses' => 'rateController@active',
            'as' => 'activerate',
            'title' => 'تغيير حالة التعليق'
        ]);

        // Delete rate
        Route::post('/delete-rate', [
            'uses' => 'rateController@delete',
            'as' => 'deleterate',
            'title' => 'حذف تعليق'
        ]);

        // Delete rates
        Route::post('/delete-rates', [
            'uses' => 'rateController@deleteAll',
            'as' => 'deleterates',
            'title' => 'حذف اكتر من تعليق'
        ]);

        #report_reson
        Route::get('/report_resons', [
            'uses' => 'report_resonController@index',
            'as' => 'report_resons',
            'title' => 'اسباب منع التعليقات',
            'icon' => '<i class="fa fa-list"></i>',
            'child' => [
                'addreport_reson',
                'updatereport_reson',
                'deletereport_reson',
                'deletereport_resons',
            ]
        ]);

        // Add report_reson
        Route::post('/add-report_reson', [
            'uses' => 'report_resonController@store',
            'as' => 'addreport_reson',
            'title' => 'اضافة سبب'
        ]);

        // Update report_reson
        Route::post('/update-report_reson', [
            'uses' => 'report_resonController@update',
            'as' => 'updatereport_reson',
            'title' => 'تعديل سبب'
        ]);

        // Delete report_reson
        Route::post('/delete-report_reson', [
            'uses' => 'report_resonController@delete',
            'as' => 'deletereport_reson',
            'title' => 'حذف سبب'
        ]);

        // Delete report_resons
        Route::post('/delete-report_resons', [
            'uses' => 'report_resonController@deleteAll',
            'as' => 'deletereport_resons',
            'title' => 'حذف اكتر من سبب'
        ]);



        #contact
        Route::get('/contacts', [
            'uses' => 'contactController@index',
            'as' => 'contacts',
            'title' => 'أتصل بنا',
            'icon' => '<i class="fa fa-envelope-open"></i>',
            'child' => [
                'showcontact',
                'sendcontact',
                'deletecontact',
                'deletecontacts',
            ]
        ]);

        Route::get('/show-contact/{id}', [
            'uses' => 'contactController@show',
            'as' => 'showcontact',
            'title' => 'عرض رسالة'
        ]);

        Route::post('/send-contact', [
            'uses' => 'contactController@send',
            'as' => 'sendcontact',
            'title' => 'رد على رسالة'
        ]);

        Route::post('/delete-contact', [
            'uses' => 'contactController@delete',
            'as' => 'deletecontact',
            'title' => 'حذف رسالة'
        ]);

        Route::post('/delete-contacts', [
            'uses' => 'contactController@deleteAll',
            'as' => 'deletecontacts',
            'title' => 'حذف اكتر من رسالة'
        ]);


        // ======== Reports
        Route::get('all-reports', [
            'uses' => 'ReportController@index',
            'as' => 'reports',
            'title' => 'التقارير',
            'icon' => '<i class="fa fa-flag"></i>',
            'child' => [
                'deletereports',
            ]
        ]);

        Route::get('/delete-reports', [
            'uses' => 'ReportController@delete',
            'as' => 'deletereports',
            'title' => 'حذف التقارير'
        ]);
    });
    Route::any('/logout', 'AuthController@logout')->name('logout');
});
