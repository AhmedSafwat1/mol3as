<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Favourite;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * @method where(string $string, int $int)
 * @method static create(array $array)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_ar', 'name_en', 'email', 'password', 'phone', 'code', 'avatar', 'provider', 'device_id',
        'active', 'checked', 'activation', 'complete', 'notify_send', 'city_id', 'role', 'lat', 'lng',
        'gender', 'address_ar', 'specialist_id', 'job_ar', 'price', 'desc_ar', 'qualifaction_ar', 'address_en',
        'desc_en', 'job_en', 'qualifaction_en', 'start_at', 'end_at', 'facebook', 'twitter', 'instagram', 'snapchat',
        'device_type', 'lang', 'total_sales', 'pay_done', 'total_profit'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Role()
    {
        return $this->hasOne('App\Models\Role', 'id', 'role');
    }

    public function City()
    {
        return $this->belongsTo('App\Models\City', 'city_id', 'id');
    }

    public function Package()
    {
        return $this->belongsTo('App\Models\Package', 'package_id', 'id');
    }

    public function Days()
    {
        return $this->hasMany('App\Models\User_day', 'user_id', 'id');
    }

    public function Prices()
    {
        return $this->hasMany('App\Models\User_price', 'user_id', 'id');
    }

    public function Specialist()
    {
        return $this->belongsTo('App\Models\Specialist', 'specialist_id', 'id');
    }

    public function Neighborhood()
    {
        return $this->belongsTo('App\Models\Neighborhood', 'neighborhood_id', 'id');
    }

    public function Devices()
    {
        return $this->hasMany('App\Models\Device', 'user_id', 'id');
    }

    public function Rates()
    {
        return $this->hasMany('App\Models\Rate', 'to_id', 'id');
    }

    static function getDoctorByLocation($lat, $lng)
    {
        $results = self::query();
        $results->having('distance', '<=', 1000000)
            ->select(
                DB::raw("*,
            (3959 * ACOS(COS(RADIANS($lat))
            * COS(RADIANS(lat))
            * COS(RADIANS($lng) - RADIANS(lng))
            + SIN(RADIANS($lat))
            * SIN(RADIANS(lat)))) AS distance")
            );
        $results->where('provider', 1);
        $results->whereDate('end_package', '>=', Carbon::now());
        $doctors_data = $results->get();

        return $doctors_data;
    }
}
