<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'doctor_id', 'user_id'
    ];

    public function Doctor()
    {
        return $this->belongsTo('App\User',  'doctor_id',  'id');
    }

    public function User()
    {
        return $this->belongsTo('App\User',  'user_id',  'id');
    }

    public function Chats()
    {
        return $this->hasMany('App\Models\Room_chat', 'room_id', 'id')->orderBy('id', 'desc');
    }
}
