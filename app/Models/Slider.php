<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'image', 'active'
    ];

    static function activeSlider()
    {
        return self::where('active', '1')->get();
    }
}
