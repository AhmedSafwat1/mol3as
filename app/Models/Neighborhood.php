<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Neighborhood extends Model
{
    protected $table = 'neighborhoods';
    protected $fillable = [
        'title_ar', 'title_en', 'city_id'
    ];

    public function City()
    {
        return $this->belongsTo('App\Models\City', 'city_id', 'id');
    }
}
