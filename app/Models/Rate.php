<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
        'from_id', 'to_id', 'rate', 'comment', 'show', 'offer_id', 'type'
    ];

    public function To()
    {
        return $this->belongsTo('App\User',  'to_id',  'id');
    }

    public function From()
    {
        return $this->belongsTo('App\User',  'from_id',  'id');
    }

    public function Offer()
    {
        return $this->belongsTo('App\Models\Offer',  'offer_id',  'id');
    }
}