<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_price extends Model
{
    protected $fillable = [
        'user_id', 'title_ar', 'title_en', 'price'
    ];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}