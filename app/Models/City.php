<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $fillable = [
        'title_ar', 'title_en', 'country_id'
    ];

    public function Neighborhoods()
    {
        return $this->hasMany('App\Models\Neighborhood', 'city_id', 'id');
    }

    public function Users()
    {
        return $this->hasMany('App\User', 'city_id', 'id');
    }
}