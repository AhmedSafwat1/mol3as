<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
        'image', 'title_ar', 'title_en', 'desc_ar', 'desc_en', 'start_date', 'end_date', 'status', 'user_id', 'old_price', 'new_price'
    ];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function Rates()
    {
        return $this->hasMany('App\Models\Rate', 'offer_id', 'id');
    }
}