<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'title_ar', 'title_en', 'desc_ar', 'desc_en', 'price', 'year', 'month', 'day','background_color','font_color'
    ];
}