<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service_image extends Model
{
    protected $fillable = [
        'image', 'service_id'
    ];
}
