<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room_chat extends Model
{
    protected $fillable = [
        'room_id', 'from_id', 'to_id', 'message', 'type', 'status', 'user_delete', 'doctor_delete', 'seen'
    ];

    public function TO()
    {
        return $this->belongsTo('App\User',  'to_id',  'id');
    }

    public function From()
    {
        return $this->belongsTo('App\User',  'from_id',  'id');
    }

    public function Room()
    {
        return $this->belongsTo('App\Models\Room',  'room_id',  'id');
    }
}
