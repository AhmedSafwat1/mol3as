<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_option extends Model
{
    protected $fillable = [
        'service_id', 'order_id', 'amount', 'price', 'cut_price', 'dealer_id', 'buy_total'
    ];

    public function Order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }

    public function Service()
    {
        return $this->belongsTo('App\Models\Service', 'service_id', 'id');
    }

    public function Cut()
    {
        return $this->belongsTo('App\Models\Cut', 'cut_id', 'id');
    }

    public function Dealer()
    {
        return $this->belongsTo('App\Models\Dealer', 'dealer_id', 'id');
    }
}