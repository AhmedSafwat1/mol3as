<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_day extends Model
{
    protected $fillable = [
        'day_id', 'user_id', 'start_at', 'end_to', 'type'
    ];

    public function Day()
    {
        return $this->belongsTo('App\Models\Day', 'day_id', 'id');
    }

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
