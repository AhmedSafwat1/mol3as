<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected $fillable = [
        'doctor_id', 'user_id'
    ];

    public function Doctor()
    {
        return $this->belongsTo('App\User',  'doctor_id',  'id');
    }

    public function User()
    {
        return $this->belongsTo('App\User',  'user_id',  'id');
    }

    static function checkFavourite($user_id, $service_id)
    {
        $check =  self::where('user_id', (int) $user_id)->where('service_id', (int) $service_id)->first();
        if (isset($check)) return true;
        return false;
    }
}