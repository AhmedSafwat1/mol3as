<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Say extends Model
{
    protected $table = 'says';
    protected $fillable = [
        'user_id', 'rate', 'comment'
    ];
}
