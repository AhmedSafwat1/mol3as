<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'order_number', 'promo_id', 'city_id', 'user_id', 'provider_id', 'neighborhood_id',
        'date', 'month', 'year', 'day_id', 'other_data', 'name', 'phone', 'pay_done', 'price', 'cut',
        'delivery', 'value_added', 'total_before_promo', 'total_after_promo', 'lat', 'lng', 'payment_method',
        'status', 'cancel', 'user_seen', 'provider_seen', 'admin_seen', 'user_finish', 'provider_finish'
    ]; // status = [0=>new , 1=>agree , 2=>refused , 3=>hasProvider , 4=>in-way , 5=>finishByClient , 6=>finishByProvider]

    public function Order_options()
    {
        return $this->hasMany('App\Models\Order_option',  'order_id',  'id');
    }

    public function Day()
    {
        return $this->belongsTo('App\Models\Day', 'day_id', 'id');
    }

    public function Promo_code()
    {
        return $this->belongsTo('App\Models\Promo_code', 'promo_id', 'id');
    }

    public function Neighborhood()
    {
        return $this->belongsTo('App\Models\Neighborhood', 'neighborhood_id', 'id');
    }

    public function City()
    {
        return $this->belongsTo('App\Models\City',  'city_id',  'id');
    }

    public function User()
    {
        return $this->belongsTo('App\User',  'user_id',  'id');
    }

    public function Provider()
    {
        return $this->belongsTo('App\User',  'provider_id',  'id');
    }

    static function provider_debt($id)
    {
        return self::where('provider_id', $id)->where('pay_done', '0')->where('payment_method', '0')->orderBy('id', 'desc')->get();
    }

    static function Provider_orders($id, $status)
    {
        return self::where('provider_seen', '1')->where('provider_id', $id)->whereIn('status', $status)->orderBy('id', 'desc')->get();
    }

    static function User_orders($id, $status)
    {
        return self::where('user_seen', '1')->where('user_id', $id)->whereIn('status', $status)->orderBy('id', 'desc')->get();
    }

    static function Admin_orders($status)
    {
        //refused or finish
        return self::where('admin_seen', '1')->whereIn('status', $status)->orderBy('id', 'desc')->get();
    }
}
