<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = [
        'image', 'title_ar', 'title_en', 'active'
    ];

    public function Services()
    {
        return $this->hasMany('App\Models\Service', 'section_id', 'id');
    }

    static function activeSection()
    {
        return self::where('active', '1')->get();
    }
}
