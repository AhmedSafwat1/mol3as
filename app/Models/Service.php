<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'title_ar', 'title_en', 'short_desc_ar', 'short_desc_en', 'desc_en', 'desc_ar', 'amount', 'price',
        'discount', 'special', 'new', 'rate', 'lat', 'lng', 'request_number', 'active', 'section_id'
    ];

    public function Images()
    {
        return $this->hasMany('App\Models\Service_image', 'service_id', 'id');
    }

    public function Cuts()
    {
        return $this->hasMany('App\Models\Service_cut', 'service_id', 'id');
    }

    public function Orders()
    {
        return $this->hasMany('App\Models\Order_option', 'service_id', 'id');
    }


    public function Prices()
    {
        return $this->hasMany('App\Models\Service_price', 'service_id', 'id');
    }

    public function options()
    {
        return $this->hasMany('App\Models\Service_option', 'service_id', 'id');
    }

    public function Section()
    {
        return $this->belongsTo('App\Models\Section', 'section_id', 'id');
    }

    static function specialService()
    {
        return self::where('special', '1')->get();
    }

    static function newService()
    {
        return self::where('new', '1')->get();
    }
}