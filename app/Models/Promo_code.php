<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promo_code extends Model
{
    protected $table = 'promo_codes';
    protected $fillable = [
        'code', 'discount', 'type', 'section_id', 'used_by'
    ];

    public function Section()
    {
        return $this->belongsTo('App\Models\Section', 'section_id', 'id');
    }
}