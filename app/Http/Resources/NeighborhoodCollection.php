<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App;

class NeighborhoodCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $title = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        return [
            'id'                => (int)    $this->id,
            'title'             => (string) $this->$title,
        ];
    }
}
