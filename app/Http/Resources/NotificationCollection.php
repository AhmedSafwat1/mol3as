<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App;
use Carbon\Carbon;

class NotificationCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $message    = App::getLocale() == 'en' ? 'message_en' : 'message_ar';
        return [
            'id'                 => (int)    $this->id,
            'message'            => (int)    $this->$message,
            'type'               => $this->type == 'order' ? 'order' : 'admin',
            'user_name'          => !is_null($this->From) ? (string) $this->From->name : '',
            'user_avatar'        => !is_null($this->From) &&  !is_null($this->From->avatar) ? url('public/images/users/' . $this->From->avatar) : '',
            'order_id'           => !is_null($this->Order) ? (string)  $this->Order->id : '',
            'order_number'       => !is_null($this->Order) ? (string)  $this->Order->order_number : '',
            'date'               => Carbon::parse($this->created_at)->format('Y-m-d'),
            'date_from'          => $this->created_at->diffForHumans(),
        ];
    }
}
