<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ServiceImageCollection;
use App\Http\Resources\CutCollection;
use App;

class ServiceCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $title       = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        $short_desc  = App::getLocale() == 'en' ? 'short_desc_en' : 'short_desc_ar';
        $desc        = App::getLocale() == 'en' ? 'desc_en' : 'desc_ar';

        return [
            'id'            => (int)    $this->id,
            'title'         => (string) $this->$title,
            'short_desc'    => (string) $this->$short_desc,
            'desc'          => (string) $this->$desc,
            'first_image'   => url('' . $this->Images->first()->image),
            'price'         => (float)  $this->price,
            'rate'          => (int)    serviceRate($this->id),
            'section_id'    => (int)    $this->Section->id,
            'section_title' => (string) $this->Section->$title,
            'cuts'          => CutCollection::collection($this->Cuts),
            'images'        => ServiceImageCollection::collection($this->Images)
        ];
    }
}
