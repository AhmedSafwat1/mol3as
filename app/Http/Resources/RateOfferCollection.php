<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class RateOfferCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'          => (int)    $this->id,
            'rate'        => (string) $this->rate,
            'comment'     => (string) $this->comment,
            'report'      => (int)    $this->report,
            'date'        => date('Y-m-d', strtotime($this->created_at)),
            'user_id'     => (int)    $this->From->id,
            'user_name'   => !is_null($this->From) ? (string) $this->From->name : '',
            // 'user_avatar' => !is_null($this->From) &&  !is_null($this->From->avatar) ? url('public/images/users/' . $this->From->avatar) : '',
        ];
    }
}
