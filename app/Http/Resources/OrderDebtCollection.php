<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App;
use App\Http\Resources\OrderOptionCollection;

class OrderDebtCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $title = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        return [
            'id'                 => (int)    $this->id,
            'order_number'       => !is_null($this->order_number) ? (string) $this->order_number : (string) $this->id,
            'status'             => (int)    $this->status,
            'day'                => !is_null($this->Day) ? (string)  $this->Day->$title : '',
            'date'               => (string) $this->date,
            'total'              => (float)  $this->total_after_promo,

            'user_name'          => !is_null($this->User) && is_null($this->name) ? (string) $this->User->name : (string) $this->name,
            'user_phone'         => !is_null($this->User) && is_null($this->phone) ? (string) $this->User->phone : (string) $this->phone,
            'user_avatar'        => !is_null($this->User) ? url('public/images/users/' . $this->User->avatar) : url('public/images/users/default.png'),
        ];
    }
}
