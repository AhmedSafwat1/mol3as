<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\RateCollection;
use App\Http\Resources\RateOfferCollection;
use App;
use Carbon\Carbon;
use App\Models\Rate;

class userOfferCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $lang     = App::getLocale() == 'en' ? 'en' : 'ar';
        $name     = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        $address  = App::getLocale() == 'en' ? 'address_en' : 'address_ar';
        $title    = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        $desc     = App::getLocale() == 'en' ? 'desc_en' : 'desc_ar';
        return [
            'id'                 => (int)    $this->id,
            'image'              => is_null($this->image) ? '' : url('' . $this->image),
            'title'              => (string) $this->$title,
            'desc'               => (string) $this->$desc,
            'old_price'          => (float)  $this->old_price,
            'new_price'          => (float)  $this->new_price,
            'date'               => (string) Carbon::parse($this->created_at)->format('Y-m-d'),
            'user_avatar'        => !is_null($this->avatar) ? url('public/images/users/' . $this->User->avatar) : url('public/images/users/default.png'),
            'user_name'          => (string) $this->User->$name,
            'user_address'       => (string) $this->User->$address,
            'user_phone'         => (string) $this->User->phone,
            'user_email'         => (string) $this->User->email,
            'user_gender'        => (string) $this->User->gender,
            'user_price'         => (string) $this->User->price,
            'user_lat'           => (string) $this->User->lat,
            'user_lng'           => (string) $this->User->lng,
            'rate'               => (int) Rate::where('to_id', $this->id)->avg('rate'),
            'rate_count'         => (int) Rate::where('to_id', $this->id)->count(),
            'distance'           => (float) round($this->distance, 2),
            'days_desc'          => (string) showDaysDesc($this->user_id, $lang),
            'dates_desc'         => (string) showDatesDesc($this->user_id, $lang),
            'offer_rates'        => RateOfferCollection::collection($this->Rates),
            'doctor_rates'       => RateCollection::collection($this->User->Rates)
        ];
    }
}
