<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use App;

class ChatCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $lang     = App::getLocale() == 'en' ? 'en' : 'ar';
        Carbon::setLocale($lang);
        $name     = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        return [
            'id'                 => (int)    $this->id,
            'type'               => (string) $this->type,
            'message'            => $this->type == 'image' ? url('' . $this->message) : (string)  $this->message,
            'date'               => (string) Carbon::parse($this->created_at)->diffForHumans(),
            'to_id'              => (int)    $this->To->id,
            'from_id'            => (int)    $this->From->id,
            'from_name'          => (string) $this->From->$name,
            'from_avatar'        => !is_null($this->From) && !is_null($this->From->avatar) ? url('public/images/users/' . $this->From->avatar) : url('public/images/users/default.png'),
        ];
    }
}
