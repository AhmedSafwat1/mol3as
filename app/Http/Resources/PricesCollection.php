<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App;

class PricesCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $title   = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        return [
            'id'                => (int)    $this->id,
            'title'             => (string) $this->$title,
            'title_ar'          => (string) $this->title_ar,
            'title_en'          => (string) $this->title_en,
            'price'             => (string) $this->price,
        ];
    }
}