<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\RateOfferCollection;
use App;
use Carbon\Carbon;
use App\Models\Rate;
use App\Models\Report_reson;

class OfferCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $title    = App::getLocale() == 'en' ? 'title_en' : 'title_ar';
        $desc     = App::getLocale() == 'en' ? 'desc_en' : 'desc_ar';
        return [
            'id'                 => (int)    $this->id,
            'title'              => (string) $this->$title,
            'title_ar'           => (string) $this->title_ar,
            'title_en'           => (string) $this->title_en,
            'desc'               => (string) $this->$desc,
            'desc_ar'            => (string) $this->desc_ar,
            'desc_en'            => (string) $this->desc_en,
            'date'               => (string) Carbon::parse($this->created_at)->format('Y-m-d'),
            'old_price'          => (float)  $this->old_price,
            'new_price'          => (float)  $this->new_price,
            'rate'               => (int) Rate::where('offer_id', $this->id)->avg('rate'),
            'rate_count'         => (int) Rate::where('offer_id', $this->id)->count(),
//            'distance'           => (float) round($this->distance, 2),
            'image'              => is_null($this->image) ? '' : url('' . $this->image),
            'report_resaons'     => ReportResonCollection::collection(Report_reson::get()),
            'rates'              => RateOfferCollection::collection($this->Rates)
        ];
    }
}
