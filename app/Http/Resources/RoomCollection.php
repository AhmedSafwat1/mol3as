<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

class RoomCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        $name           = App::getLocale() == 'en' ? 'name_en' : 'name_ar';
        return [
            'id'            => (int)    $this->id,
            'from_id'       => (int)    $this->User->id,
            'from_name'     => !is_null($this->User) ? (string) $this->User->$name : '',
            'to_id'         => (int)    $this->Doctor->id,
            'to_name'       => !is_null($this->Doctor) ? (string) $this->Doctor->$name : '',
            'last_message'  => !is_null($this->Chats) ? lastMsg($this->id) : '',
            // 'user_avatar' => !is_null($this->From) &&  !is_null($this->From->avatar) ? url('public/images/users/' . $this->From->avatar) : '',
        ];
    }
}
