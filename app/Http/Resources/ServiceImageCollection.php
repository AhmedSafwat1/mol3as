<?php

namespace App\Http\Resources;

// use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceImageCollection extends JsonResource
{
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'          => (int)    $this->id,
            'image'       => !is_null($this->image) ? url('' . $this->image) : '',
        ];
    }
}
