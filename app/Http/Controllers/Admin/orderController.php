<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Section;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\Order;
use Carbon\Carbon;

class orderController extends Controller
{

    public function index()
    {
        $roles      = Role::latest()->get();
        return view('dashboard.order.index', compact('roles'));
    }

    public function new()
    {
        $data       = Order::with(['Order_options', 'Day', 'Promo_code', 'Neighborhood', 'City', 'User', 'Provider'])->where('status', '0')->where('admin_seen' , '1')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.order.new', compact('data', 'roles'));
    }

    public function current()
    {
        $search     = isset($_GET["search"]) ? $_GET["search"] : '';
        $data       = Order::with(['Order_options', 'Day', 'Promo_code', 'Neighborhood', 'City', 'User', 'Provider'])->whereIn('status', ['3', '4'])->where('admin_seen' , '1')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.order.current', compact('data', 'roles', 'search'));
    }

    public function today()
    {
        $data       = Order::with(['Order_options', 'Day', 'Promo_code', 'Neighborhood', 'City', 'User', 'Provider'])->whereDate('date', Carbon::now())->orderBy('status', 'asc')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.order.today', compact('data', 'roles'));
    }

    public function tomorrow()
    {
        $data       = Order::with(['Order_options', 'Day', 'Promo_code', 'Neighborhood', 'City', 'User', 'Provider'])->whereDate('date', Carbon::now()->addDay())->orderBy('status', 'asc')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.order.tomorrow', compact('data', 'roles'));
    }

    public function finish()
    {
        $data       = Order::with(['Order_options', 'Day', 'Promo_code', 'Neighborhood', 'City', 'User', 'Provider'])->whereIn('status', ['5', '6'])->where('admin_seen' , '1')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.order.finish', compact('data', 'roles'));
    }

    public function show($id)
    {
        $data       = Order::with(['Order_options', 'Day', 'Promo_code', 'Neighborhood', 'City', 'User', 'Provider'])->where('admin_seen' , '1')->whereId($id)->first();
        $roles      = Role::latest()->get();
        return view('dashboard.order.show', compact('data', 'roles'));
    }

    public function agree($id, Request $request)
    {
        $order  = Order::whereId($id)->first();
        if (!is_null($request->order_number)) $order->order_number = $request->order_number;
        $order->provider_id = $request->provider_id;
        $order->status      = 3;
        $order->save();

        addReport(auth()->user()->id, 'بتعين طلب لمندوب', $request->ip());
        Session::flash('success', 'تم بنجاح');
        return back();
    }

    public function delete(Request $request)
    {

        $order = Order::findOrFail($request->delete_id);
        if ($order->status == 0) $order->status = 2;
        $order->admin_seen = '0';
        $order->save();
        addReport(auth()->user()->id, 'بحذف طلب', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }
}