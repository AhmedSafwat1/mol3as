<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Specialist;

class specialistController extends Controller
{
    // ============= Admins ==============

    /**
     * All Admins
     *
     * @param User $user
     * @param Role $role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *          view => dashboard/users/admins.blade.php
     */
    public function index()
    {
        $data       = Specialist::OrderBy('title_ar', 'asc')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.specialist.index', compact('data', 'roles'));
    }

    /**
     * Add new Admin
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'title_ar'              => 'required|max:255',
            'title_en'              => 'required|max:255',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'الاسم بالعربية مطلوب',
            'title_ar.max'          => 'الاسم بالعربية لابد ان يكون اصغر من 255 حرف',
            'title_en.required'     => 'الاسم بالأنجليزية مطلوب',
            'title_en.max'          => 'الاسم بالأنجليزية لابد ان يكون اصغر من 255 حرف',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store specialist
        $add = new Specialist;
        $add->title_ar     = $request->title_ar;
        $add->title_en     = $request->title_en;
        $add->save();

        addReport(auth()->user()->id, 'باضافة تخصص جديد', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return back();
    }

    public function update(Request $request)
    {

        // Validation rules
        $rules = [
            'title_ar'              => 'required|max:255',
            'title_en'              => 'required|max:255',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'الاسم بالعربية مطلوب',
            'title_ar.max'          => 'الاسم بالعربية لابد ان يكون اصغر من 255 حرف',
            'title_en.required'     => 'الاسم بالأنجليزية مطلوب',
            'title_en.max'          => 'الاسم بالأنجليزية لابد ان يكون اصغر من 255 حرف',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store specialist
        $add = Specialist::findOrFail($request->id);
        $add->title_ar     = $request->title_ar;
        $add->title_en     = $request->title_en;
        $add->save();

        addReport(auth()->user()->id, 'بتعديل بيانات التخصص', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return back();
    }

    public function delete(Request $request)
    {

        Specialist::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف تخصص', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Specialist::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من المدن', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
