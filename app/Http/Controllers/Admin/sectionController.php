<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Section;
use App\Models\Service;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class sectionController extends Controller
{

    public function index()
    {
        $data       = Section::get();
        $roles      = Role::latest()->get();
        return view('dashboard.section.index', compact('data', 'roles'));
    }

    public function showservices($id)
    {
        $data = Service::where('section_id', $id)->get();
        $roles      = Role::latest()->get();
        return view('dashboard.service.index', compact('data', 'roles', 'id'));
    }

    public function change_special(Request $request)
    {
        //check data
        $service = Service::find($request->id);
        if (!isset($service)) return 0;
        //update data
        $service->special = !$service->special;
        $service->save();
        //add report
        $service->special == 1 ?
            addReport(auth()->user()->id, 'بتمييز خدمة', $request->ip()) : addReport(auth()->user()->id, 'بالغاء تمييز خدمة', $request->ip());
        return 1;
    }

    public function change_active(Request $request)
    {
        //check data
        $section = Section::find($request->id);
        if (!isset($section)) return 0;
        //update data
        $section->active = !$section->active;
        $section->save();
        //add report
        $section->active == 1 ?
            addReport(auth()->user()->id, 'باعادة ظهور قسم', $request->ip()) : addReport(auth()->user()->id, 'بأخفاء ظهور قسم', $request->ip());
        return 1;
    }

    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'title_ar'              => 'required|min:2|max:255',
            'title_en'              => 'required|min:2|max:255',
            'image'                 => 'required',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'الاسم بالعربية مطلوب',
            'title_ar.min'          => 'الاسم بالعربية لابد ان يكون اكبر من حرفين',
            'title_ar.max'          => 'الاسم بالعربية لابد ان يكون اصغر من 255 حرف',
            'title_en.required'     => 'الاسم بالأنجليزية مطلوب',
            'title_en.min'          => 'الاسم بالأنجليزية لابد ان يكون اكبر من حرفين',
            'title_en.max'          => 'الاسم بالأنجليزية لابد ان يكون اصغر من 255 حرف',
            'image.required'        => 'الصورة مطلوبة',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store Section
        $add = new Section;
        $add->title_ar  = $request->title_ar;
        $add->title_en  = $request->title_en;
        $add->image     = uploadImage($request->file('image'), 'public/images/section');
        $add->save();

        addReport(auth()->user()->id, 'باضافة قسم جديد', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return back();
    }

    public function update(Request $request)
    {

        // Validation rules
        $rules = [
            'title_ar'              => 'required|min:2|max:255',
            'title_en'              => 'required|min:2|max:255',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'الاسم بالعربية مطلوب',
            'title_ar.min'          => 'الاسم بالعربية لابد ان يكون اكبر من حرفين',
            'title_ar.max'          => 'الاسم بالعربية لابد ان يكون اصغر من 255 حرف',
            'title_en.required'     => 'الاسم بالأنجليزية مطلوب',
            'title_en.min'          => 'الاسم بالأنجليزية لابد ان يكون اكبر من حرفين',
            'title_en.max'          => 'الاسم بالأنجليزية لابد ان يكون اصغر من 255 حرف',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store Section
        $add = Section::findOrFail($request->id);
        $add->title_ar  = $request->title_ar;
        $add->title_en  = $request->title_en;
        if ($request->has('image')) $add->image = uploadImage($request->file('image'), 'public/images/section');
        $add->save();

        addReport(auth()->user()->id, 'بتعديل بيانات القسم', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return back();
    }

    public function delete(Request $request)
    {

        Section::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف قسم', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Section::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من الأقسام', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}