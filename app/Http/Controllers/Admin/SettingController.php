<?php

namespace App\Http\Controllers\Admin;

use App\Models\AppSetting;
use App\Models\Social;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function index()
    {
        $setting = AppSetting::all();
        $socials = Social::all();
        return view('dashboard.settings.index', [
            'setting'  => $setting,
            'socials'  => $socials,
        ]);
    }

    public function updateSiteInfo(Request $request)
    {

        $sitePhone = AppSetting::where('key', 'phone')->first();
        $sitePhone->value = $request->phone;
        $sitePhone->save();

        $sitemobile = AppSetting::where('key', 'mobile')->first();
        $sitemobile->value = $request->mobile;
        $sitemobile->save();

        $sitelimit_kg = AppSetting::where('key', 'limit_kg')->first();
        $sitelimit_kg->value = $request->limit_kg;
        $sitelimit_kg->save();

        $sitevalue_added = AppSetting::where('key', 'value_added')->first();
        $sitevalue_added->value = $request->value_added;
        $sitevalue_added->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث اعدادات الموقع', $ip);
        Session::flash('success', 'تم تعديل اعدادات الموقع');
        return back();
    }

    public function intro(Request $request)
    {
        $sitefirst_intro_ar = AppSetting::where('key', 'first_intro_ar')->first();
        $sitefirst_intro_ar->value = $request->first_intro_ar;
        $sitefirst_intro_ar->save();

        $sitefirst_intro_en = AppSetting::where('key', 'first_intro_en')->first();
        $sitefirst_intro_en->value = $request->first_intro_en;
        $sitefirst_intro_en->save();


        $sitesecond_intro_ar = AppSetting::where('key', 'second_intro_ar')->first();
        $sitesecond_intro_ar->value = $request->second_intro_ar;
        $sitesecond_intro_ar->save();

        $sitesecond_intro_en = AppSetting::where('key', 'second_intro_en')->first();
        $sitesecond_intro_en->value = $request->second_intro_en;
        $sitesecond_intro_en->save();

        $sitethird_intro_ar = AppSetting::where('key', 'third_intro_ar')->first();
        $sitethird_intro_ar->value = $request->third_intro_ar;
        $sitethird_intro_ar->save();

        $sitethird_intro_en = AppSetting::where('key', 'third_intro_en')->first();
        $sitethird_intro_en->value = $request->third_intro_en;
        $sitethird_intro_en->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث بيانات مقدمة التطبيق', $ip);
        Session::flash('success', 'تم تعديل بيانات مقدمة التطبيق');
        return back();
    }

    public function pages(Request $request)
    {
        $siteabout_us_ar = AppSetting::where('key', 'about_us_ar')->first();
        $siteabout_us_ar->value = $request->about_us_ar;
        $siteabout_us_ar->save();

        $siteabout_us_en = AppSetting::where('key', 'about_us_en')->first();
        $siteabout_us_en->value = $request->about_us_en;
        $siteabout_us_en->save();


        $sitecondition_ar = AppSetting::where('key', 'condition_ar')->first();
        $sitecondition_ar->value = $request->condition_ar;
        $sitecondition_ar->save();

        $sitecondition_en = AppSetting::where('key', 'condition_en')->first();
        $sitecondition_en->value = $request->condition_en;
        $sitecondition_en->save();

        $sitepolicy_ar = AppSetting::where('key', 'policy_ar')->first();
        $sitepolicy_ar->value = $request->policy_ar;
        $sitepolicy_ar->save();

        $sitepolicy_en = AppSetting::where('key', 'policy_en')->first();
        $sitepolicy_en->value = $request->policy_en;
        $sitepolicy_en->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث بيانات الصفحات الثابتة', $ip);
        Session::flash('success', 'تم تعديل بيانات الصفحات الثابتة');
        return back();
    }

    public function app_links(Request $request)
    {
        $siteclient_android_url = AppSetting::where('key', 'client_android_url')->first();
        $siteclient_android_url->value = $request->client_android_url;
        $siteclient_android_url->save();

        $siteprovider_android_url = AppSetting::where('key', 'provider_android_url')->first();
        $siteprovider_android_url->value = $request->provider_android_url;
        $siteprovider_android_url->save();


        $siteclient_ios_url = AppSetting::where('key', 'client_ios_url')->first();
        $siteclient_ios_url->value = $request->client_ios_url;
        $siteclient_ios_url->save();

        $siteprovider_ios_url = AppSetting::where('key', 'provider_ios_url')->first();
        $siteprovider_ios_url->value = $request->provider_ios_url;
        $siteprovider_ios_url->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث روابط التطبيق', $ip);
        Session::flash('success', 'تم تعديل روابط التطبيق');
        return back();
    }

    public function about(Request $request)
    {
        $siteAbout = AppSetting::where('key', 'about_us')->first();
        $siteAbout->value = $request->about_us;
        $siteAbout->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث بيانات من نحن', $ip);
        Session::flash('success', 'تم تعديل بيانات من نحن');
        return back();
    }

    public function SEO(Request $request)
    {
        $siteDescription = AppSetting::where('key', 'description')->first();
        $siteDescription->value = $request->description;
        $siteDescription->save();

        $siteKeyWords = AppSetting::where('key', 'key_words')->first();
        $siteKeyWords->value = $request->key_words;
        $siteKeyWords->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث بيانات ال SEO', $ip);
        Session::flash('success', 'تم تعديل بيانات ال SEO');
        return back();
    }

    public function storeSocial(Request $request)
    {
        $rules = [
            'site_name' => 'required|min:2|max:190',
            'icon'      => 'required|min:2|max:190',
            'url'       => 'required|url',
        ];
        $messages = [
            'site_name.required'    => 'اسم الموقع مطلوب',
            'site_name.min'         => 'اسم الموقع لابد ان يكون اكتر من حرفين',
            'site_name.max'         => 'اسم الموقع لابد ان يكون اقل من 190 حرف',
            'icon.required'         => 'الشعار مطلوب',
            'icon.min'              => 'الشعار لابد ان يكون اكبر من حرفين',
            'icon.max'              => 'الشعار لابد ان يكون اقل من 190 حرف',
            'url.required'          => 'الرابط مطلوب',
            'url.url'               => 'الرابط غير صحيح',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $social = new Social();
        $social->site_name = $request->site_name;
        $social->icon = $request->icon;
        $social->url = $request->url;
        $social->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'باضافة موقع تواصل جدبد', $ip);
        Session::flash('success', 'تم اضافة الموقع');
        return back();
    }

    public function updateSocial(Request $request)
    {
        if(isset($request->edit_name) && $request->edit_name == 'snapchat'){
            $rules = [
                'edit_name' => 'required|min:2|max:190',
                'edit_icon' => 'required|min:2|max:190',
                'edit_url'  => 'required',
                'id'        => 'required',
            ];
            $messages = [
                'edit_name.required'    => 'اسم الموقع مطلوب',
                'edit_name.min'         => 'اسم الموقع لابد ان يكون اكتر من حرفين',
                'edit_name.max'         => 'اسم الموقع لابد ان يكون اقل من 190 حرف',
                'edit_icon.required'         => 'الشعار مطلوب',
                'edit_icon.min'              => 'الشعار لابد ان يكون اكبر من حرفين',
                'edit_icon.max'              => 'الشعار لابد ان يكون اقل من 190 حرف',
                'edit_url.required'          => 'الرابط مطلوب',
            ];
        }else{
            $rules = [
                'edit_name' => 'required|min:2|max:190',
                'edit_icon' => 'required|min:2|max:190',
                'edit_url'  => 'required|url',
                'id'        => 'required',
            ];
            $messages = [
                'edit_name.required'    => 'اسم الموقع مطلوب',
                'edit_name.min'         => 'اسم الموقع لابد ان يكون اكتر من حرفين',
                'edit_name.max'         => 'اسم الموقع لابد ان يكون اقل من 190 حرف',
                'edit_icon.required'         => 'الشعار مطلوب',
                'edit_icon.min'              => 'الشعار لابد ان يكون اكبر من حرفين',
                'edit_icon.max'              => 'الشعار لابد ان يكون اقل من 190 حرف',
                'edit_url.required'          => 'الرابط مطلوب',
                'edit_url.url'               => 'الرابط غير صحيح',
            ];
        }


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $social = Social::findOrFail($request->id);
        $social->site_name = $request->edit_name;
        $social->icon = $request->edit_icon;
        $social->url = $request->edit_url;
        $social->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتحديث موقع تواصل', $ip);
        Session::flash('success', 'تم تحديث الموقع');
        return back();
    }

    public function deleteSocial($id, Request $request)
    {
        Social::where('id', $id)->delete();

        $ip = $request->ip();
        addReport(auth()->user()->id, 'بحذف موقع تواصل', $ip);
        Session::flash('success', 'تم حذف الموقع');
        return back();
    }
}
