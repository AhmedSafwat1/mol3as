<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Rate;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Mail\replayMail;
use Mail;

class rateController extends Controller
{
    //all msgs
    public function index()
    {
        $data   = Rate::where('report', 1)->OrderBy('id', 'desc')->get();
        $roles  = Role::latest()->get();
        return view('dashboard.rate.index', compact('data', 'roles'));
    }

    //send msg
    public function active(Request $request)
    {
        //check data
        $slider = Rate::find($request->id);
        if (!isset($slider)) return 0;
        //update data
        $slider->show = !$slider->show;
        $slider->save();
        //add report
        $slider->show == 1 ?
            addReport(auth()->user()->id, 'بظهور التعليق', $request->ip()) : addReport(auth()->user()->id, 'بإلغاء ظهور التعليق', $request->ip());
        return 1;
    }

    public function delete(Request $request)
    {
        Rate::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف تعليق', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Rate::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من تعليق', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
