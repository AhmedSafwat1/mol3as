<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\Country;

class UsersController extends Controller
{
    // ============= Users ==============

    public function index(User $user)
    {
        $users = $user->where('role', 0)->where('provider', '0')->latest()->get();
        $countries = Country::get();
        return view('dashboard.users.index', compact('users', 'countries'));
    }


    public function store(Request $request)
    {
        // Validation rules
        $rules = [
            'name_ar'    => 'required|min:2|max:255',
            'last_name'     => 'required|min:2|max:255',
            'phone'         => 'required|digits_between:7,20',
            'email'         => 'nullable|email',
            'password'      => 'required|min:6|max:255',
            'avatar'        => 'nullable|image'
        ];
        // Validation
        $validator = validator($request->all(), $rules);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }
        if ($request->hasFile('avatar')) {
            $avatar = UploadFile::uploadImage($request->file('avatar'), 'users');
        } else {
            $avatar = 'default.png';
        }

        $code = Country::find($request['country_id'])->code;

        //checkPhone
        $check = User::where('phone', $code . convert2english($request['phone']))->first();
        if (isset($check)) {
            Session::flash('success', 'رقم الهاتف مستخدم من قبل');
            return back();
        }

        // Save User
        User::create([
            'name'          => $request['name_ar'] . ' ' . $request['last_name'],
            'name_ar'    => $request['name_ar'],
            'last_name'     => $request['last_name'],
            'main_phone'    => convert2english($request['phone']),
            'phone'         => $code . convert2english($request['phone']),
            'email'         => $request['email'],
            'country_id'    => $request['country_id'],
            'password'      => bcrypt($request['password']),
            'avatar'        => $avatar,
            'provider'      => 0,
            'add_service'   => 0,
            'checked'       => 1,
        ]);

        $ip = $request->ip();

        addReport(auth()->user()->id, 'باضافة عضو جديد', $ip);
        Session::flash('success', 'تم اضافة العضو بنجاح');
        return back();
    }

    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'name_ar'    => 'required|min:2|max:255',
            'last_name'     => 'required|min:2|max:255',
            'phone'         => 'required|digits_between:7,20',
            'email'         => 'nullable|email',
            'password'      => 'nullable|min:6|max:255',
            'avatar'        => 'nullable|image'
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $code = Country::find($request['country_id'])->code;

        //checkPhone
        $check = User::where('id', '!=', $request->id)->where('phone', $code . convert2english($request['phone']))->first();
        if (isset($check)) {
            Session::flash('success', 'رقم الهاتف مستخدم من قبل');
            return back();
        }

        $user = User::findOrFail($request->id);

        if ($request->has('avatar')) {
            if ($user->avatar != 'default.png') {
                File::delete(public_path('images/users/' . $user->avatar));
            }

            $user->avatar = UploadFile::uploadImage($request->file('avatar'), 'users');
        }
        if (isset($request->password) || $request->password != null) {
            $user->password = bcrypt($request->password);
        }

        $user->name             = $request['name_ar'] . ' ' . $request['last_name'];
        $user->name_ar       = $request['name_ar'];
        $user->last_name        = $request['last_name'];
        $user->main_phone       = convert2english($request['phone']);
        $user->phone            = $code . convert2english($request['phone']);
        $user->email            = $request['email'];
        $user->country_id       = $request['country_id'];
        $user->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتعديل بيانات العضو', $ip);
        Session::flash('success', 'تم تعديل العضو بنجاح');
        return back();
    }

    public function delete(Request $request)
    {
        $user = User::findOrFail($request->delete_id);
        if (isset($user) && !is_null($user->device_id)) {
            $device_id          = $user->device_id;
            $device_type        = $user->device_type;
            $lang = $user->lang;
            $data = [
                'title_ar'    => 'اشعار',
                'title_en'    => 'notification',
                'active_ar'   => 'تم حذف حسابك الان من قبل الادارة',
                'active_en'   => 'your account is deleted now by admin',
            ];
            $all_data   = [];
            $all_data['title']      = $data['title_' . $lang];
            $all_data['msg']        = $data['active_' . $lang];
            $all_data['status']     = 4; //deleted user

            Send_FCM_Badge($device_id, $all_data, $device_type);
        }
        User::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف العضو', $request->ip());
        Session::flash('success', 'تم حذف العضو بنجاح');
        return back();
    }

    public function change_checked(Request $request)
    {
        //check data
        $user = User::find($request->id);
        if (!isset($user)) return 0;
        //update data
        $user->checked = !$user->checked;
        $user->save();

        if (isset($user) && !is_null($user->device_id)) {
            $device_id          = $user->device_id;
            $device_type        = $user->device_type;
            $lang = $user->lang;
            $data = [
                'title_ar'    => 'اشعار',
                'title_en'    => 'notification',
                'active_ar'   => 'تم تفعيل حسابك الان من قبل الادارة',
                'active_en'   => 'your account is activted now by admin',
                'blocked_ar'  => 'تم حظر حسابك الان من قبل الادارة',
                'blocked_en'  => 'your account is blocked now by admin',
            ];
            $all_data   = [];
            $all_data['title']      = $data['title_' . $lang];
            $all_data['msg']        = $user->checked == 1 ? $data['active_' . $lang] : $data['blocked_' . $lang];
            $all_data['status']     = 4; //blocked user

            Send_FCM_Badge($device_id, $all_data, $device_type);
        }
        //add report
        $user->checked == 1 ?
            addReport(auth()->user()->id, 'بفك حظر عضو', $request->ip()) : addReport(auth()->user()->id, 'بحظر عضو', $request->ip());
        return 1;
    }


    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
            $user = User::findOrFail($id->id);
            if (isset($user) && !is_null($user->device_id)) {
                $device_id          = $user->device_id;
                $device_type        = $user->device_type;
                $lang = $user->lang;
                $data = [
                    'title_ar'    => 'اشعار',
                    'title_en'    => 'notification',
                    'active_ar'   => 'تم حذف حسابك الان من قبل الادارة',
                    'active_en'   => 'your account is deleted now by admin',
                ];
                $all_data   = [];
                $all_data['title']      = $data['title_' . $lang];
                $all_data['msg']        = $data['active_' . $lang];
                $all_data['status']     = 4; //deleted user

                Send_FCM_Badge($device_id, $all_data, $device_type);
            }
        }
        if (User::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من الاعضاء', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }

    public function sendNotify(Request $request)
    {
        addReport(auth()->user()->id, 'قام بارسال اشعار', $request->ip());
        Session::flash('success', 'تم الارسال بنجاح');
        return back();
    }
}
