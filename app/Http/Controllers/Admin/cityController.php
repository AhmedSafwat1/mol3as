<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\City;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\City_day;

class cityController extends Controller
{
    // ============= Admins ==============

    /**
     * All Admins
     *
     * @param User $user
     * @param Role $role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *          view => dashboard/users/admins.blade.php
     */
    public function index()
    {
        $data       = City::OrderBy('title_ar', 'asc')->get();
        $roles      = Role::latest()->get();
        return view('dashboard.city.index', compact('data', 'roles'));
    }

    /**
     * Add new Admin
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'title_ar'              => 'required|max:255',
            'title_en'              => 'required|max:255',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'الاسم بالعربية مطلوب',
            'title_ar.max'          => 'الاسم بالعربية لابد ان يكون اصغر من 255 حرف',
            'title_en.required'     => 'الاسم بالأنجليزية مطلوب',
            'title_en.max'          => 'الاسم بالأنجليزية لابد ان يكون اصغر من 255 حرف',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store City
        $add = new City;
        $add->title_ar     = $request->title_ar;
        $add->title_en     = $request->title_en;
        $add->save();

        addReport(auth()->user()->id, 'باضافة مدينة جديدة', $request->ip());
        Session::flash('success', 'تم الأضافة بنجاح');
        return back();
    }

    public function update(Request $request)
    {

        // Validation rules
        $rules = [
            'title_ar'              => 'required|max:255',
            'title_en'              => 'required|max:255',
        ];

        // Validator messages
        $messages = [
            'title_ar.required'     => 'الاسم بالعربية مطلوب',
            'title_ar.max'          => 'الاسم بالعربية لابد ان يكون اصغر من 255 حرف',
            'title_en.required'     => 'الاسم بالأنجليزية مطلوب',
            'title_en.max'          => 'الاسم بالأنجليزية لابد ان يكون اصغر من 255 حرف',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        //store City
        $add = City::findOrFail($request->id);
        $add->title_ar     = $request->title_ar;
        $add->title_en     = $request->title_en;
        $add->save();

        addReport(auth()->user()->id, 'بتعديل بيانات المدينة', $request->ip());
        Session::flash('success', 'تم التعديل بنجاح');
        return back();
    }

    public function delete(Request $request)
    {

        City::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف مدينة', $request->ip());
        Session::flash('success', 'تم الحذف بنجاح');
        return back();
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (City::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من المدن', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}