<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\Country;
use App\Models\Order;
use App\Models\Notification;
use Auth;

class providersController extends Controller
{
    // ============= providers ==============

    public function index(User $user)
    {
        $search     = isset($_GET["gender"]) ? $_GET["gender"] : '';
        if (!empty($search))
            $search     = $search == 'female' ? 'انثى' : 'ذكر';
        $users      = $user->where('role', 0)->where('provider', '1')->latest()->get();
        $countries  = Country::get();
        return view('dashboard.provider.index', compact('users', 'countries', 'search'));
    }

    public function show($id)
    {
        $user     = User::whereId($id)->firstOrFail();
        return view('dashboard.provider.show', compact('user'));
    }

    public function store(Request $request)
    {
        // Validation rules
        $rules = [
            'name'          => 'required|min:2|max:255',
            'phone'         => 'required|min:10|unique:users,phone',
            'email'         => 'nullable|email|unique:users,email',
            'password'      => 'required|min:6|max:255',
            'avatar'        => 'nullable|image'
        ];
        // Validation
        $validator = validator($request->all(), $rules);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }
        if ($request->hasFile('avatar')) {
            $avatar = UploadFile::uploadImage($request->file('avatar'), 'users');
        } else {
            $avatar = 'default.png';
        }

        // Save User
        User::create([
            'name'    => $request['name'],
            'phone'         => convert2english($request['phone']),
            'email'         => $request['email'],
            'city_id'       => $request['city_id'],
            'password'      => bcrypt($request['password']),
            'avatar'        => $avatar,
            'provider'      => 1,
            'add_service'   => 1,
            'checked'       => 1,
        ]);

        $ip = $request->ip();

        addReport(auth()->user()->id, 'باضافة مزود جديد', $ip);
        Session::flash('success', 'تم اضافة المزود بنجاح');
        return back();
    }

    public function update(Request $request)
    {
        // Validation rules
        $rules = [
            'name'          => 'required|min:2|max:190',
            'phone'         => 'required|min:10|unique:users,phone,' . $request->id,
            'email'         => 'nullable|email',
            'password'      => 'nullable|min:6',
            'avatar'        => 'nullable|image'
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules);

        // If failed
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $user = User::findOrFail($request->id);

        if ($request->has('avatar')) {
            if ($user->avatar != 'default.png') {
                File::delete(public_path('images/users/' . $user->avatar));
            }

            $user->avatar = UploadFile::uploadImage($request->file('avatar'), 'users');
        }
        if (isset($request->password) || $request->password != null) {
            $user->password = bcrypt($request->password);
        }

        $user->name             = $request['name'];
        $user->phone            = convert2english($request['phone']);
        $user->email            = $request['email'];
        $user->city_id          = $request['city_id'];
        $user->save();

        $ip = $request->ip();

        addReport(auth()->user()->id, 'بتعديل بيانات المزود', $ip);
        Session::flash('success', 'تم تعديل المزود بنجاح');
        return back();
    }

    public function delete(Request $request)
    {
        $user = User::findOrFail($request->delete_id);
        if (isset($user) && !is_null($user->device_id)) {
            $device_id          = $user->device_id;
            $device_type        = $user->device_type;
            $lang = $user->lang;
            $data = [
                'title_ar'    => 'اشعار',
                'title_en'    => 'notification',
                'active_ar'   => 'تم حذف حسابك الان من قبل الادارة',
                'active_en'   => 'your account is deleted now by admin',
            ];
            $all_data   = [];
            $all_data['title']      = $data['title_' . $lang];
            $all_data['msg']        = $data['active_' . $lang];
            $all_data['status']     = 4; //deleted user

            Send_FCM_Badge($device_id, $all_data, $device_type);
        }
        User::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف المزود', $request->ip());
        Session::flash('success', 'تم حذف المزود بنجاح');
        return back();
    }

    public function showProviderDetails(Request $request)
    {
        $user = User::find($request->id);
        if (!isset($user)) return 0;

        $new                = Order::where('provider_id', $user->id)->where('status', '0')->count();
        $current            = Order::where('provider_id', $user->id)->where('status', '1')->count();
        $providerCancel     = Order::where('provider_id', $user->id)->whereIn('status', ['2', '4'])->count();
        $clientCancel       = Order::where('provider_id', $user->id)->where('status', '3')->count();
        $finish             = Order::where('provider_id', $user->id)->where('status', '5')->count();
        $data = [
            'new'            => $new,
            'current'        => $current,
            'providerCancel' => $providerCancel,
            'clientCancel'   => $clientCancel,
            'finish'         => $finish,
        ];
        return view('ajax.showProviderDetails', $data);
    }

    public function showProviderBill(Request $request)
    {
        $user = User::find($request->id);
        if (!isset($user)) return 0;

        $total                  = $user->total_sales;
        $adminBenfit            = $user->total_profit;
        $providerBenfit         = $total - $adminBenfit;
        $data = [
            'total'             => $total,
            'adminBenfit'       => $adminBenfit,
            'providerBenfit'    => $providerBenfit,
        ];
        return view('ajax.showProviderBill', $data);
    }

    public function change_checked(Request $request)
    {
        //check data
        $user = User::find($request->id);
        if (!isset($user)) return 0;
        //update data
        $user->checked = !$user->checked;
        $user->save();

        if (isset($user) && !is_null($user->device_id)) {
            $device_id          = $user->device_id;
            $device_type        = $user->device_type;
            $lang = $user->lang;
            $data = [
                'title_ar'    => 'اشعار',
                'title_en'    => 'notification',
                'active_ar'   => 'تم تفعيل حسابك الان من قبل الادارة',
                'active_en'   => 'your account is activted now by admin',
                'blocked_ar'  => 'تم حظر حسابك الان من قبل الادارة',
                'blocked_en'  => 'your account is blocked now by admin',
            ];
            $all_data   = [];
            $all_data['title']      = $data['title_' . $lang];
            $all_data['msg']        = $user->checked == 1 ? $data['active_' . $lang] : $data['blocked_' . $lang];
            $all_data['status']     = 4; //blocked user

            Send_FCM_Badge($device_id, $all_data, $device_type);
        }
        //add report
        $user->checked == 1 ?
            addReport(auth()->user()->id, 'بفك حظر مزود', $request->ip()) : addReport(auth()->user()->id, 'بحظر مزود', $request->ip());
        return 1;
    }

    public function change_addService(Request $request)
    {
        //check data
        $user = User::find($request->id);
        if (!isset($user)) return 0;
        //update data
        $user->add_service = $user->add_service == '0' ? '2' : '0';
        $user->save();

        if (isset($user) && !is_null($user->device_id)) {
            $device_id          = $user->device_id;
            $device_type        = $user->device_type;
            $lang = $user->lang;
            $data = [
                'title_ar'    => 'اشعار',
                'title_en'    => 'notification',
                'active_ar'   => 'تم الموافقة على طلبك لرفع خدماتك من قبل الادارة',
                'active_en'   => 'your request is agreed about add new services by admin',
                'blocked_ar'  => ' تم منعك من اضافة خدمة جديدة من قبل الادارة',
                'blocked_en'  => 'admin prevent you to add new service',
            ];
            $all_data   = [];
            $all_data['title']      = $data['title_' . $lang];
            $all_data['msg']        = $user->add_service == 2 ? $data['active_' . $lang] : $data['blocked_' . $lang];
            $all_data['status']     = 5; //blocked user

            Send_FCM_Badge($device_id, $all_data, $device_type);
        }
        //add report
        $user->add_service == '2' ?
            addReport(auth()->user()->id, 'بالسماح لعضو بأضافة خدمات', $request->ip()) : addReport(auth()->user()->id, 'بمنع مزود بأضافة خدمات', $request->ip());
        return 1;
    }

    public function change_addService_notfiy(Request $request)
    {
        //check data
        $user = User::find($request->id);
        if (!isset($user)) return 0;
        //update data
        $user->add_service = $user->add_service == '0' ? '2' : '0';
        $user->save();

        if (isset($user) && !is_null($user->device_id)) {
            $device_id          = $user->device_id;
            $device_type        = $user->device_type;
            $lang = $user->lang;
            $data = [
                'title_ar'    => 'اشعار',
                'title_en'    => 'notification',
                'active_ar'   => 'تم الموافقة على طلبك لرفع خدماتك من قبل الادارة',
                'active_en'   => 'your request is agreed about add new services by admin',
                'blocked_ar'  => ' تم منعك من اضافة خدمة جديدة من قبل الادارة',
                'blocked_en'  => 'admin prevent you to add new service',
            ];
            $all_data   = [];
            $all_data['title']      = $data['title_' . $lang];
            $all_data['msg']        = $user->add_service == 2 ? $data['active_' . $lang] : $data['blocked_' . $lang];
            $all_data['status']     = 5; //blocked user

            Send_FCM_Badge($device_id, $all_data, $device_type);
        }

        //notify
        $add = new Notification;
        $add->from_id    = Auth::id();
        $add->to_id      = $request->id;
        $add->order_id   = null;
        $add->message_ar = $user->add_service == '0' ? 'تم حظرك من اضافة خدمات جديدة' : 'تم السماح لك باضافة خدمات جديدة';
        $add->message_en = $user->add_service == '0' ? 'you was stoped to store new services by admin' : 'you was allowed store new services by admin';
        $add->type       = 'replay';
        $add->save();
        //add report
        $user->add_service == '2' ?
            addReport(auth()->user()->id, 'بالسماح لعضو بأضافة خدمات', $request->ip()) : addReport(auth()->user()->id, 'بمنع مزود بأضافة خدمات', $request->ip());
        return 1;
    }

    public function deleteAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
            $user = User::findOrFail($id->id);
            if (isset($user) && !is_null($user->device_id)) {
                $device_id          = $user->device_id;
                $device_type        = $user->device_type;
                $lang = $user->lang;
                $data = [
                    'title_ar'    => 'اشعار',
                    'title_en'    => 'notification',
                    'active_ar'   => 'تم حذف حسابك الان من قبل الادارة',
                    'active_en'   => 'your account is deleted now by admin',
                ];
                $all_data   = [];
                $all_data['title']      = $data['title_' . $lang];
                $all_data['msg']        = $data['active_' . $lang];
                $all_data['status']     = 4; //deleted user

                Send_FCM_Badge($device_id, $all_data, $device_type);
            }
        }
        if (User::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من المزودين', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }

    public function sendNotify(Request $request)
    {
        addReport(auth()->user()->id, 'قام بارسال اشعار', $request->ip());
        Session::flash('success', 'تم الارسال بنجاح');
        return back();
    }
}
