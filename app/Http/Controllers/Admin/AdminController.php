<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UploadFile;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    // ============= Admins ==============

    /**
     * All Admins
     *
     * @param User $user
     * @param Role $role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *          view => dashboard/users/admins.blade.php
     */
    public function index(User $user, Role $role)
    {
        $users = $user->where('role', '!=', 0)->with('Role')->latest()->get();
        $roles = $role->latest()->get();
        return view('dashboard.admins.index', compact('users'), compact('roles'));
    }

    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'name_ar'     => 'required|max:190',
            'phone'    => 'required|min:10|unique:users,phone',
            'email'    => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'avatar'   => 'nullable',
            'role'     => 'required',
        ];

        // Validator messages
        $messages = [
            'name_ar.required'     => 'الاسم مطلوب',
            'name_ar.max'          => 'الاسم لابد ان يكون اصغر من 190 حرف',
            'phone.required'    => 'رقم الهاتف مطلوب',
            'phone.min'         => 'رقم الهاتف لا يقل عن 10 ارقام',
            'phone.unique'      => 'رقم الهاتف موجود بالفعل',
            'email.required'    => 'البريد الالكتروني مطلوب',
            'email.unique'      => 'البريد الالكتروني موجود بالفعل',
            'email.email'       => 'تحقق من صحة البريد الالكتروني',
            'password.required' => 'كلمة السر مطلوبة',
            'password.min'      => 'كلمة السر لا يقل عن 6 حروف او ارقام',
            'role.required'     => 'الصلاحية مطلوبة',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return redirect('admin/admins')->withErrors($validator);
        }

        if ($request->hasFile('avatar')) {
            $avatar = UploadFile::uploadImage($request->file('avatar'), 'users');
        } else {
            $avatar = 'default.png';
        }

        // Save User
        User::create([
            'name_ar'  => $request['name_ar'],
            'phone'    => convert2english($request['phone']),
            'email'    => $request['email'],
            'role'     => $request['role'],
            'password' => bcrypt($request['password']),
            'avatar'   => $avatar,
            'checked'  => 1,
        ]);

        addReport(auth()->user()->id, 'باضافة مشرف جديد', $request->ip());
        Session::flash('success', 'تم اضافة المشرف بنجاح');
        return redirect('admin/admins');
    }

    public function update(Request $request)
    {

        // Validation rules
        $rules = [
            'edit_name_ar'  => 'required|max:190',
            'edit_phone' => 'required|min:10',
            'edit_email' => 'required|email|unique:users,email,' . $request->id,
            'avatar'     => 'nullable',
            'password'   => 'nullable|min:6',
            'role'       => 'required',
        ];

        // Validator messages
        $messages = [
            'edit_name_ar.required'     => 'الاسم مطلوب',
            'edit_name_ar.max'          => 'الاسم لابد ان يكون اصغر من 190 حرف',
            'edit_phone.required'    => 'رقم الهاتف مطلوب',
            'edit_phone.min'         => 'رقم الهاتف لا يقل عن 10 ارقام',
            'edit_phone.unique'      => 'رقم الهاتف موجود بالفعل',
            'edit_email.required'    => 'البريد الالكتروني مطلوب',
            'edit_email.unique'      => 'البريد الالكتروني موجود بالفعل',
            'edit_email.email'       => 'تحقق من صحة البريد الالكتروني',
            'password.min'           => 'كلمة السر لا يقل عن 6 حروف او ارقام',
            'role.required'          => 'الصلاحية مطلوبة',
        ];

        // Validation
        $validator = Validator::make($request->all(), $rules, $messages);

        // If failed
        if ($validator->fails()) {
            return redirect('admin/admins')->withErrors($validator);
        }

        $user = User::findOrFail($request->id);
        $role = false;

        if ($request->hasFile('avatar')) {
            if ($user->avatar != 'default.png') {
                File::delete(public_path('images/users/' . $user->avatar));
            }
            $avatar = UploadFile::uploadImage($request->file('avatar'), 'users');
        }

        if (isset($request->password) || $request->password != null) {
            $user->password = bcrypt($request->password);
        }

        $user->name_ar  = $request->edit_name_ar;
        $user->phone = convert2english($request->edit_phone);
        $user->email = $request->edit_email;
        if ($request->id != 1) {
            $role = $user->role != $request->role ? true : false;
            $user->role = $request->role;
        }
        $user->save();

        addReport(auth()->user()->id, 'بتعديل بيانات المشرف', $request->ip());
        Session::flash('success', 'تم تعديل المشرف بنجاح');
        if($role) return redirect('admin');
        return redirect('admin/admins');
    }

    public function delete(Request $request)
    {
        if ($request->delete_id == 1) {
            Session::flash('danger', 'لا يمكن حذف  هذا المشرف');
            return redirect('admin/admins');
        }

        if ($request->delete_id == Auth::id()) {
            Session::flash('danger', 'لا يمكن حذف العضو صاحب جلسة الدخول');
            return redirect('admin/admins');
        }

        User::findOrFail($request->delete_id)->delete();
        addReport(auth()->user()->id, 'بحذف مشرف', $request->ip());
        Session::flash('success', 'تم حذف المشرف بنجاح');
        return redirect('admin/admins');
    }

    public function deleteAllAdmins(Request $request)
    {
        $requestIds = json_decode($request->data);
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (in_array(Auth::id() , $ids)) {
            $key = array_search(Auth::id(), $ids);
            unset($ids[$key]);
        }
        if (User::whereIn('id', $ids)->delete()) {
            addReport(auth()->user()->id, 'قام بحذف العديد من المشرفين', $request->ip());
            Session::flash('success', 'تم الحذف بنجاح');
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
