<?php

namespace App\Http\Controllers\api;

use File;
use Hash;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;

class UserController extends Controller
{
	public $lang;
	public $title;

	public function __construct(Request $request)
	{
		SetLang($request->lang);
		$this->lang = isset($request->lang) && $request->lang == 'en' ? 'en' : 'ar';
		$this->title = isset($request->lang) && $request->lang == 'en' ? 'title_en' : 'title_ar';
	}

	#show profile
	public function showProfile(Request $request)
	{
		/** Validate Request **/
		$validate = Validator::make($request->all(), [
			'user_id'   => 'required|exists:users,id',
		]);

		/** Send Error Massages **/
		if ($validate->fails())
			return apiResponse('0', $validate->errors()->first());

		return apiResponse('1', trans('api.send'), new UserCollection(User::whereId($request->user_id)->first()));
	}

	#update profile
	public function updateProfile(Request $request)
	{
		/** Validate Request **/
		$validate = Validator::make($request->all(), [
			'user_id'   	=> 'required|exists:users,id',
			'name_ar'		=> 'nullable',
			'name_en'	    => 'nullable',
			'phone'			=> 'nullable|unique:users,phone,' . $request->user_id,
			'email'			=> 'nullable|unique:users,email,' . $request->user_id,
			'city_id'	    => 'nullable',
			'user_lang'	    => 'nullable',
			'avatar'	    => 'nullable',
		]);

		/** Send Error Massages **/
		if ($validate->fails())
			return apiResponse('0', $validate->errors()->first());

		/** Update User **/
		$user = User::whereId($request->user_id)->first();
		if (!is_null($request->name_ar)) $user->name_ar  = $request->name_ar;
		if (!is_null($request->name_en)) $user->name_en  = $request->name_en;
		if (!is_null($request->email)) 	 $user->email    = $request->email;
		if (!is_null($request->phone))   $user->phone    = convert2english($request->phone);
		if (!is_null($request->city_id)) $user->city_id  = $request->city_id;
		if (!is_null($request->user_lang)) $user->lang  = $request->user_lang;
		if (!is_null($request->avatar))
		    $user->avatar = uploadAvatar($request->file('avatar'), 'public/images/users');
		/*{
			// delete old image
			if ($user->avatar != 'default.png') {
                if (file_exists(public_path('images/users/' . $user->avatar)))
                    File::delete('images/users/' . $user->avatar);
            }

			//base-64
			$photo 	= $request->avatar;
			$name 	= time() . '_' . rand(11111, 99999);
			$path   = 'public/images/users';
			save_img($photo, $name, $path);
			$user->avatar   = $name . '.' . 'png';
		}*/
		$user->save();

		return apiResponse('1', trans('api.save'), new UserCollection($user));
	}

	# update old password from profile
	public function updatePassword(Request $request)
	{

		/** Validate Request **/
		$validate = Validator::make($request->all(), [
			'user_id'       => 'required|exists:users,id',
			'old_password'  => 'required',
			'new_password'  => 'required',
		]);

		/** Send Error Massages **/
		if ($validate->fails())
			return apiResponse('0', $validate->errors()->first());

		$user = User::whereId($request->user_id)->first();
		if (Hash::check(request('old_password'), $user->password)) {
			/** update Password **/
			$user->password  = bcrypt($request->new_password);
			$user->save();
			/** send data **/
			return apiResponse('1', trans('api.passwordUpdated'));
		}

		/** If Code wrong **/
		return apiResponse('0', trans('api.wrongPassword'));
	}

	# edit lang & notify_allow
	public function editLang(Request $request)
	{
		$validate = Validator::make($request->all(), [
			'user_id'      => 'required|exists:users,id',
			'lang'         => 'required|in:ar,en',
		]);

		/** Send Error Massages **/
		if ($validate->fails())
			return apiResponse('0', $validate->errors()->first());

		$user   = User::whereId($request->user_id)->first();
		$user->lang     	 = request('lang');
		$user->save();

		/** send data **/
		return apiResponse('1', trans('api.Updated'));
	}
}
