<?php

namespace App\Http\Controllers\siteAuth;

use App\User;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function Logout(Request $request){
        Auth::logout();
        return redirect('/');
    }

    public function Login(){
        $countries = Country::get();
        return view('site.login',compact('countries'));
    }

    public function PostLogin(Request $request){

        $validator        = Validator::make($request->all(), [
            'phone'         => 'required',
            'country_id'    => 'required|exists:countries,id',
            'password'      => 'required',
        ]);

        if ($validator->passes()) {

            $code   = Country::find($request['country_id'])->code;
            $phone  = $code . convert2english($request->phone);
        
            $user = User::where('phone',$phone)->first();

            /** If Login Success But Not Active User **/
            if ($user && $user->checked == 0) {
                $massage  = trans('api.blocked');
                return response()->json(['massage' => $massage , 'status' => 0]);
            }

            /** if not verified after register send him to verification page **/
            if ($user && $user->activation == 0) {
                $user->code         = rand(11111, 99999);
                $user->save();
                /** Send Code To User's Using SMS **/
                sendSms($user->phone, trans('api.activeCode') . $user->code);

                $msg = trans('api.needActive');
                session()->put('error',$msg);
                return response()->json(['message' => $msg, 'status' => 2]);
            }

            /** if verified and active account **/
            if (Auth::attempt(['phone' => $phone, 'password' => request('password')])) {

                $msg = App::isLocale('ar') ? "مرحبا بك  يا " . Auth::user()->name : "Welcome " . Auth::user()->name;
                session()->put('success',$msg);
                return response()->json(['message' => 'correct data', 'status' => 1]);
                            
            }else{
                $msg = trans('api.wrongLogin');
                return response()->json(['message' => $msg, 'status' => 0]);
            }
        }else{
            $msg = $validator->errors()->first();
            return response()->json(['message' => $msg, 'status' => 0]);
        }

    }

    public function ForgetPassword(){
        $countries = Country::get();
        return view('site.forgetPassword',compact('countries'));
    }

    public function postForgetPassword(Request $request){
        
        $validator        = Validator::make($request->all(), [
            'phone'         => 'required',
            'country_id'    => 'required|exists:countries,id',
        ]);

        if ($validator->passes()) {

            $code   = Country::find($request['country_id'])->code;
            $phone  = $code . convert2english($request->phone);

            //update code
            $user = User::where('phone', $phone)->first();

            /** If Wrong User Data **/
            if(!$user){
                $msg = trans('api.wrongPhone');
                //session()->put('error',$msg);
                return response()->json(['message' => $msg, 'status' => 0]);
                //return redirect('/');
            }

            /** If User But Not Active User **/
            if ($user && $user->checked == 0) {
                $massage  = trans('api.blocked');
                return response()->json(['massage' => $massage , 'status' => 0]);
            }

            $user->code = rand(11111, 99999);
            $user->save();

            /** Send Code To User's Phone **/
            $msg = trans('api.activeCode');
            sendSms($user->phone, $msg . $user->code);

            $msg = trans('api.enterCode');
            session()->put(['phone'=>$phone,'user_id' => $user->id]);
            return response()->json(['message' => $msg, 'status' => 1]);
        }else{
            $msg = $validator->errors()->first();
            return response()->json(['message' => $msg, 'status' => 0]);
        }

    }

    // verification for forget password code - diff call back from register verification
    public function Verfication(){
        return view('site.verfication');
    }

    public function postVerfication(Request $request){

        $validator        = Validator::make($request->all(), [
            'user_id'   => 'required',
            'code'      => 'required',
        ]);

        if ($validator->passes()) {

            $user = User::find($request->user_id);

            /** Check Code **/
            if ($user->code == $request->code) {
                /** update checked **/
                $user->code         = '';
                $user->checked      = '1';
                $user->activation   = '1';
                $user->save();

                $msg = trans('api.rightCode');            
                return response()->json(['message' => $msg, 'status' => 1]);
                
            }else{
                $msg = trans('api.wrongCode');
                return response()->json(['message' => $msg, 'status' => 0]);
            }

        }else{
            $msg = $validator->errors()->first();
            return response()->json(['message' => $msg, 'status' => 0]);
        }

    }

    public function NewPassword(){
        return view('site.newPassword');
    }

    public function postNewPassword(Request $request){
                
        $validator        = Validator::make($request->all(), [
            'user_id'        => 'required',
            'password'       => 'required|min:6|max:255|confirmed',
        ]);

        if ($validator->passes()) {

            $user = User::find($request->user_id);            

            /** If Not Active User **/
            if($user->checked == 0){
                $msg = trans('api.blocked');
                return response()->json(['message' => $msg, 'status' => 0]);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            $msg = trans('api.passwordUpdated');
            session()->put('success',$msg);
            Auth::loginUsingId($user->id, TRUE);
            return response()->json(['message' => $msg, 'status' => 1]);

        }else{
            $msg = $validator->errors()->first();
            return response()->json(['message' => $msg, 'status' => 0]);
        }

    }

    public function ResendCode(){


        $phone  = Session::get('phone');

        //update code
        $user = User::where('phone', $phone)->first();

        if(!$user){
            $msg = trans('api.wrongPhone');
            session()->put('error',$msg);
            //return response()->json(['message' => $msg, 'status' => 0]);
            return redirect('/');
        }

        $user->code = rand(11111, 99999);
        $user->save();

        /** Send Code To User's Phone **/
        $msg = trans('api.activeCode');
        sendSms($user->phone, $msg . $user->code);

        return back();
    }

}