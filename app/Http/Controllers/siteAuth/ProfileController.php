<?php

namespace App\Http\Controllers\siteAuth;

use Hash;
use App\User;
use App\Models\Country;
use App\Models\Favourite;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function Profile($id=null){

        /*
         * id is for which tab to active in profile page
         * 1 -> user data
         * 2 -> user favourite
         * 3 -> provider services
         * 4 -> my orders
         * */

        $user = Auth::user();
        $countries = Country::get();
        $userFavServices = [];
        $userOrders = [];

        if(Auth::check() && Auth::user()->provider == 0){

            $userFavServices = Favourite::where('user_id', Auth::id())->get();
            $userOrders = Order::User_orders($user->id);

        }elseif(Auth::check() && Auth::user()->provider == 1){

            $userOrders = Order::Provider_orders($user->id);
        }
        //dd($userOrders);



        return view('site.profile',compact('user','countries','userFavServices','userOrders','id'));
    }

    public function postProfile(Request $request){
                
        $validator        = Validator::make($request->all(), [
            'first_name'	=> 'required|min:2|max:255',
			'last_name'		=> 'required|min:2|max:255',
			'phone'			=> 'required|numeric|digits_between:7,20',
			'country_id'    => 'required|exists:countries,id',
			'email'			=> 'nullable|email',
			'address'	    => 'nullable|max:255',
        ]);

        if ($validator->passes()) {

            // get country code for phone use
            $code = Country::find($request['country_id'])->code;
            
            //checkPhone
            $check = User::where('phone', $code . convert2english($request['phone']))->first();
            if (isset($check) && $check->id != Auth::id()) {
                $msg = trans('api.usedPhone');
                return response()->json(['message' => $msg, 'status' => 0]);
            }

            /** Update User **/
            $user = Auth::user();

            $user->name         	= $request->first_name . ' ' . $request->last_name;
            $user->first_name       = $request->first_name;
            $user->last_name        = $request->last_name;
            $user->main_phone   	= convert2english($request->phone);
            $user->phone        	= $code . convert2english($request->phone);
            $user->country_id       = $request->country_id;
            $user->email            = $request->email;
            $user->address          = $request->address;
            $user->save();

            $data = [
                'name'          => $user->name,
                'first_name'    => $user->first_name,
                'last_name'     => $user->last_name,
                'mainPhone'     => $user->main_phone,
                'phone'         => $user->phone,
                'country_id'    => $user->country_id,
                'email'         => $user->email,
                'address'       => $user->address,
            ];

            $msg = trans('api.Updated');
            return response()->json(['message' => $msg,'userData'=>$data, 'status' => 1]);

        }else{
            $msg = $validator->errors()->first();
            return response()->json(['message' => $msg, 'status' => 0]);
        }

    }

    # update old password from profile
	public function UpdatePassword(Request $request)
	{
		/** Validate Request **/
		$validator = Validator::make($request->all(), [
			'old_password'  => 'required',
            'password'      => 'required|min:6|max:255|confirmed',
		]);

		/** Send Error Massages **/
		if ($validator->fails()) {
			$msg = $validator->errors()->first();
            return response()->json(['message' => $msg, 'status' => 0]);
		}

		$user = User::find(Auth::id());
		if (Hash::check(request('old_password'), $user->password)) {

			/** update Password **/
			$user->password  = bcrypt($request->password);
            $user->save();         
            
            $msg = trans('api.passwordUpdated');
            return response()->json(['message' => $msg, 'status' => 1]);

		} else {
            
			/** If old password wrong **/			
            $msg = trans('api.wrongPassword');
            return response()->json(['message' => $msg, 'status' => 0]);
		}
    }   
}