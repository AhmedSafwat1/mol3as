<?php

namespace App\Http\Controllers\siteAuth;

use Auth;
use File;

use Hash;
use View;
use Image;
use Session;
use App\User;
use Validator;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    public function register(){
        $countries = Country::get();
        return view('site.signup',compact('countries'));
    }

    public function PostRegister(Request $request)
    {
        
        $validator        = Validator::make($request->all(), [
            'first_name'     => 'required|min:2|max:255',
            'last_name'      => 'required|min:2|max:255',
            'phone'          => 'required|numeric|digits_between:7,20',
            'password'       => 'required|min:6|max:255|confirmed',
            'provider'       => 'required|in:0,1',
            'country_id'     => 'required|exists:countries,id',
            'terms'          => 'required|in:1',
        ]);

        if ($validator->passes()) {

            // get country code for phone use
            $code = Country::find($request['country_id'])->code;
            //checkPhone unique
            $check = User::where('phone', $code . convert2english($request['phone']))->first();
            if (isset($check)) {
                $msg = trans('api.usedPhone');
                return response()->json(['message' => $msg, 'status' => 0]);
            }
            
            /** Save data to users **/
            $user               = new User;
            $user->name         = $request->first_name . ' ' . $request->last_name;
            $user->first_name   = $request->first_name;
            $user->last_name    = $request->last_name;
            $user->main_phone   = convert2english($request->phone);
            $user->phone        = $code . convert2english($request->phone);
            $user->password     = bcrypt($request->password);
            $user->provider     = $request->provider;
            $user->country_id   = $request->country_id;
            $user->code         = rand(11111, 99999);
            $user->checked      = 1; // ban or not from admin
            $user->activation   = 0; // not active account after confirm code -> 1
            # 0=>not allow , 1=>newProviderHasMaxServiceCount , 2=>allowStoreByAdminWithoutMaxCount 
            $user->add_service  = $request->provider; // 0,1
            $user->save();

            /** Send Code To User's Using SMS **/
            sendSms($user->phone, trans('api.activeCode') . $user->code);

            //$msg = trans('api.successfullyRegistered');
            //session()->put('success',$msg);
            //Auth::loginUsingId($user->id, TRUE);
            //return redirect('/');
            session()->put(['phone'=>$user->phone,'user_id' => $user->id]);
            return response()->json(['message' => 'account created', 'status' => 1]);

        }else{
            $msg = $validator->errors()->first();
            return response()->json(['message' => $msg, 'status' => 0]);
        }




    }

    public function RegisterVerfication(){
        return view('site.registerVerfication');
    }

    public function PostRegisterVerfication(Request $request){

        $validator        = Validator::make($request->all(), [
            'user_id'   => 'required',
            'code'      => 'required',
        ]);

        if ($validator->passes()) {

            $user = User::find($request->user_id);

            
            /** Check Code **/
            if ($user->code == $request->code) {

                /** update checked **/
                $user->code         = '';
                $user->checked      = '1';
                $user->activation   = '1';
                $user->save();

                Auth::loginUsingId($user->id, TRUE);
                return response()->json(['message' => 'account created', 'status' => 1]);

            }else{
                $msg = trans('api.wrongCode');
                return response()->json(['message' => $msg, 'status' => 0]);
            }

        }else{
            $msg = $validator->errors()->first();
            return response()->json(['message' => $msg, 'status' => 0]);
        }

    }
}