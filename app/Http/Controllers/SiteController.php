<?php

namespace App\Http\Controllers;
use App\Models\Contact;
use App\Models\Country;
use App\Models\Section;
use App\Models\Service;
use App\Models\Favourite;
use App\Models\Notification;
use App\Models\Order;
use App\User;
use File;
use Illuminate\Http\Request;
use App\Models\Service_image;
use App\Models\Service_price;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SiteController extends Controller
{

    public function __construct(){
        
    }

    ##################### GENERAL WORK #####################

    # site language
    public function language($lang){
        Session()->has('language') ? Session()->forget('language') : '';
        if ($lang == 'ar') {
            Session()->put('language', 'ar');
        }else{
            Session()->put('language', 'en');
        }
        return back();
    }

    # about us page
    public function AboutUs(){
        return view('site.aboutUs');
    }

    # contact us page
    public function Contact(){
        $countries   = Country::get();
        return view('site.contactUs',compact('countries'));
    }

    # post contact message
    public function PostContact(Request $request){
                
        $validator        = Validator::make($request->all(), [
            'name'    		=> 'required|min:2|max:255',
			'email'    		=> 'required|min:5|max:255|email',
            'phone'			=> 'required|numeric|digits_between:7,20',
			'message' 		=> 'required',
        ]);

        if ($validator->passes()) {

            $code   = Country::find($request['country_id'])->code;
            $phone  = $code . convert2english($request->phone);

            /** Save Contact **/
            $contact   = new Contact;
            $contact->name    = $request->name;
            $contact->email   = $request->email;
            $contact->phone   = $phone;
            $contact->message = $request->message;
            $contact->save();

            $msg = trans('api.sendSuccess');
            return response()->json(['message' => $msg, 'status' => 1]);

        }else{
            $msg = $validator->errors()->first();
            return response()->json(['message' => $msg, 'status' => 0]);
        }

    }

    # get all Secdtions page
    public function AllSections(){
                
        $sections    = Section::activeSection();
        return view('site.allSections',compact('sections'));

    }

    # section services page
    public function SectionServices($id,$provider_id = null){

        $providerName = ""; // for specific provider details from fav link

        /* if provider id != null this from fav page to link to this provider serveces only */
        $section    = Section::with('Services')->findOrFail($id);
        if(Auth::check() && Auth::user()->provider == 1){
            $services   = Service::where('user_id', Auth::id())->where('section_id', $id)->paginate(12);
        }else{

            if($provider_id == null){
                # get all section services order by most use as default filter
                $services   = Service::where('section_id', $id)->orderBy('rent_count', 'desc')->paginate(12);
            }else{

                # get all section services order by most use as default filter -> for specific provider from favourite
                $services   = Service::where('user_id', $provider_id)->where('section_id', $id)->orderBy('rent_count', 'desc')->paginate(12);
                $providerName = '-' . User::findOrFail($provider_id)->name;
            }

        }

        # data for filter - high price and low price
        if($provider_id == null){
            $servicesByHighPrice   = Service::where('section_id', $id)->orderBy('price', 'desc')->paginate(12);
            $servicesByLowPrice    = Service::where('section_id', $id)->orderBy('price', 'asc')->paginate(12);
        }else{

            # for specific provider from favourite
            $servicesByHighPrice   = Service::where('user_id', $provider_id)->where('section_id', $id)->orderBy('price', 'desc')->paginate(12);
            $servicesByLowPrice    = Service::where('user_id', $provider_id)->where('section_id', $id)->orderBy('price', 'asc')->paginate(12);

        }

        return view('site.services',compact('services','section','servicesByHighPrice','servicesByLowPrice','providerName'));
    }

    # show services details page
    public function ServiceDetails($id){
        $service   = Service::find($id);
        return view('site.serviceDetails',compact('service'));
    }

    # search
    public function Search(Request $request){
        //dd($request->all());
        /** Get Data **/
        $results 		= Service::query();
        if (!is_null($request->lat) && !is_null($request->lng)) {
            $lat  = $request->lat;
            $lng  = $request->lng;
            $results->having('distance', '<=', '500')
                ->select(
                    DB::raw("*,
                (3959 * ACOS(COS(RADIANS($lat))
                * COS(RADIANS(lat))
                * COS(RADIANS($lng) - RADIANS(lng))
                + SIN(RADIANS($lat))
                * SIN(RADIANS(lat)))) AS distance")
                );
        }

        if (!is_null($request->section_id))
            $results->where('section_id', $request->section_id);

        if (!is_null($request->title))
            $results->where('title_ar', 'like', '%' . $request->title . '%')->orWhere('title_en', 'like', '%' . $request->title . '%');

        //$searchServices = $results->get();

        $searchServices = [];
        foreach($results->get() as $service){
            # filter if service section is active or not from admin
            if($service->Section->active == 1){
                $searchServices[] = $service;
            }
        }
        //dd($searchServices);


        // for similar results after search in service details page
        session()->put('$searchServices',$searchServices);

        return view('site.search',compact('searchServices'));
        

    }

    # get all services for filter if vue
    public function AllServices(){
        $allServices = Service::get();
        return response()->json(['data' => $allServices]);
    }


    ##################### USER WORK #####################

    # section services page
    public function Reserve($id){

        if(Auth::check() && Auth::user()->provider == 0){
            $service   = Service::findOrFail($id);
            return view('site.reservation',compact('service'));
        }else{

            if(Auth::check() && Auth::user()->provider == 1){

                $msg = trans('api.mustUser');
            }else{

                $msg = trans('api.needLogin');
            }
            session()->put('error',$msg);
            return back();
        }

    }

    # post reservation and make order
    public function PostReserve(Request $request){
        //dd($request->all());
        $validator        = Validator::make($request->all(), [
            'service_id'        => 'required',
            'payment_method'    => 'required',
            'from'   		    => 'required|date|after:today',
            'to'   		        => 'required|date|after_or_equal:from',

            "id_num"            => "required_if:payment_method,1",
            "id_mm"             => "required_if:payment_method,1",
            "id_yy"             => "required_if:payment_method,1",
            "id_cvv"            => "required_if:payment_method,1",


        ]);
        if ($validator->passes()) {

            //check date//

            # get old dates already taken
            $service = Service::find($request->service_id);
            $count   = $service->quantity; //all service quantity
            $orders = Order::where('service_id', $request->service_id)->where('status', '<', '2')->get();
            $old_dates = [];
            foreach ($orders as $order) {
                $arr = createDateRangeArray($order->start_date, $order->end_date);
                foreach ($arr as $value) {
                    array_push($old_dates, $value);
                }
            }

            # check with old dates
            $dates = createDateRangeArray($request->from, $request->to);
            foreach ($dates as $value) {
                if (in_array($value, $old_dates)) {
                    if ($count > 1) {
                        $orderCount = getOrdersCount($request->service_id, $value);
                        //if there is service at stock not using
                        if ($orderCount < $count)
                            continue;
                    }
                    //if this is already taken
                    $msg = $value . trans('api.takenDate');
                    return response()->json(['message' => $msg, 'status' => 0]);
                }
            }

            //count total price//

            #specific price from prices table
            $offers = Service_price::where('service_id', $request->service_id)->get();
            $all_prices = [];
            foreach ($offers as $offer) {
                $arr = createDateRangeArray($offer->start_date, $offer->end_date);
                foreach ($arr as $value) {
                    $all_prices[$value] = $offer->price;
                }
            }
            //count total price -> get price
            $total = 0;
            $price = Service::find($request->service_id)->price;
            $dates = createDateRangeArray($request->from, $request->to);
            foreach ($dates as $value) {
                if (isset($all_prices[$value]))
                    $total += $all_prices[$value];
                else
                    $total += $price;
            }


            // save order
            $service = Service::find($request->service_id);

            /** store Data **/
            $order = new Order;
            $order->price 			= $total;
            $order->start_date 		= $request->from;
            $order->end_date 		= $request->to;
            $order->user_id 		= Auth::id();
            $order->provider_id 	= $service->User->id;
            $order->service_id 		= $service->id;
            $order->payment_method 	= $service->payment_method;
            $order->status 			= '0'; //new order
            $order->save();

            /** send notification to provider **/
            sendNotify($order->user_id, $order->provider_id, $order->id);

            #Send FCM To Client


            $msg = trans('api.send');
            session()->put('success',$msg);
            return response()->json(['message' => $msg, 'status' => 1]);

        }else{
            $msg = $validator->errors()->first();
            return response()->json(['message' => $msg, 'status' => 0]);
        }

    }

    # get reservation price on change dates ajax
    public function GetPrice($service_id,$from,$to){

        //count total price//

        #specific price from prices table
        $offers = Service_price::where('service_id', $service_id)->get();
        $all_prices = [];
        foreach ($offers as $offer) {
            $arr = createDateRangeArray($offer->start_date, $offer->end_date);
            foreach ($arr as $value) {
                $all_prices[$value] = $offer->price;
            }
        }
        //count total price -> get price
        $total = 0;
        $price = Service::find($service_id)->price;
        $dates = createDateRangeArray($from, $to);
        foreach ($dates as $value) {
            if (isset($all_prices[$value]))
                $total += $all_prices[$value];
            else
                $total += $price;
        }
        return response()->json(['message' => 'order price counted','price'=> $total,'status' => 1]);
    }

    # add to favourite
    public function AddToFav($id){
        if(Auth::check() && Auth::user()->provider == 0){
            /** Get Data **/
            $check = Favourite::where('user_id', Auth::id())->where('service_id', $id)->first();
            if (isset($check)){
                $check->delete();

                $msg = trans('removed from fav');
                return response()->json(['message' => $msg,'status'=>1]);
            }else {
                $add = new Favourite;
                $add->user_id = Auth::id();
                $add->service_id = $id;
                $add->save();

                $msg = trans('added to fav');
                return response()->json(['message' => $msg,'status'=>1]);
            }

        }else{
            $msg = trans('api.needLogin');
            return response()->json(['message' => $msg,'status' => 0]);
        }

    }

    # get user fav for icon in service details
    public function GetUserFav(){
        if(Auth::check() && Auth::user()->provider == 0){
            /** Get Data **/
            $favouriteIds = Favourite::where('user_id', Auth::id())->pluck('service_id');
            return response()->json(['data' => $favouriteIds]);

        }else{
            $msg = trans('api.needLogin');
            return response()->json(['message' => $msg,'status' => 0]);
        }
    }

    # hide & cancel order from my orders in profile
    public function CancelUserOrder($id){
        $order = Order::find($id);
        $order->status = 3; // 3 -> delete by user
        $order->save();
        return response()->json(['message' => 'order canceled success']);
    }


    ##################### MIX WORK #####################
    # hide user or provider order from my orders in profile
    public function HideUserOrder($id){
        $order = Order::find($id);
        if(Auth::user()->provider == 0){

            $order->user_seen = 0;
        }elseif(Auth::user()->provider == 1){
            $order->provider_seen = 0;

        }
        $order->save();
        return response()->json(['message' => 'order hidden success']);
    }

    ##################### PROVIDER WORK #####################


    # add services page -> provider
    public function ProviderAddService(){
        $sections    = Section::activeSection();
        $countries   = Country::get();
        return view('site.provider.providerAddService',compact('sections','countries'));
    }

    # add services data -> provider
    public function PostProviderAddService(Request $request){
        //dd($request->all());

        $user = Auth::user();

        /** If Not Active User **/
        if($user->checked == 0){
            $msg = trans('api.needActive');
            session()->put('error',$msg);
            return back();
            //return response()->json(['message' => $msg, 'status' => 0]);
        }

        /** If Not allow to add from dashboard add_service = 0 **/
        /** If provider has permission to store this service total service < max from admin **/
        if (!checkProviderServicesCount($user->id) || $user->add_service == 0) {
            $msg = trans('api.needAdminAgree');
            session()->put('confirm',$msg);
            return back();
            //return response()->json(['message' => $msg, 'status' => 0]);
        }
                
        $validator        = Validator::make($request->all(), [
			'title_ar'   	 => 'required|max:255',
			'title_en'   	 => 'required|max:255',
            'price'   		 => 'required',
			'lat'   		 => 'required|max:255',
			'country_id'   	 => 'required|exists:countries,id',
			'phone'			=> 'required|numeric|digits_between:7,20',
			'section_id'   	 => 'required|exists:sections,id',
			// 'city_id'   	 => 'required|exists:cities,id',
			'quantity'       => 'required|numeric|max:255',
			'desc_ar'   	 => 'required',
			'desc_en'   	 => 'required',
			'payment_method' => 'required',
            // 'options'   	 => 'required',
                        
			'prices'   		 => '',
			'from'   		 => '',
            'to'   		     => '',
            
			'images'   		 => 'required',
        ],[
            //'images.max'=> App::isLocale('en')?'max images 6 images':'الحد الاقصى للصور 6 صور',
        ]);

        if ($validator->passes()) {

            # get specific prices for choosen days - mix 3 arrays in one
            $prices = request('prices');
            $from = request('from');
            $to = request('to');
            $data = [];

            foreach ($prices as $k => $v){
                $data[] = array();
            }
            foreach ($prices as $k => $v){
                array_push($data[$k],$v);
            }
            foreach ($from as $k => $v){
                array_push($data[$k],$v);
            }
            foreach ($to as $k => $v){
                array_push($data[$k],$v);
            }

            //dd($data);

            /***
             * now all  data in one array $data
             * foreach $data as price to log in it
             * price[0] => price
             * price[1] => price from
             * price[2] => price to
             ***/

            // get country code for phone use
            $code = Country::find($request['country_id'])->code;

            /** store Data **/
            $service = new Service;
            $service->title_ar 		 = $request->title_ar;
            $service->title_en 		 = $request->title_en;
            $service->desc_ar 		 = $request->desc_ar;
            $service->desc_en 		 = $request->desc_en;
            $service->address_ar 	 = $request->address_ar;
            $service->address_en 	 = $request->address_en;
            $service->lat 			 = $request->lat;
            $service->lng 			 = $request->lng;
            $service->price 		 = $request->price;
            $service->main_phone 	 = $request->phone;
            $service->phone 		 = $code . $request->phone;
            $service->quantity 		 = $request->quantity;
            $service->user_id 		 = $user->id;
            $service->country_id 	 = $request->country_id;
            $service->section_id 	 = $request->section_id;        
            $service->payment_method = $request->payment_method;
            $service->save();

            //store images
            if($request->images && count($request->images) > 0){
                
                foreach($request->images as $photo){
                    if($photo != null){
                        // get image base64
                        $photo = substr($photo, strpos($photo, ",")+1,-1 );

                        $service_image = new Service_image;
                        $service_image->service_id = $service->id;
                         //$service_image->image = uploadImage($photo, 'public/images/services');
                        
                        //base-64
                        $name 	=  time() . '_' . rand(11111, 99999);
                        $path   = 'public/images/services';
                        save_img($photo, $name, $path);
                        $service_image->image   	  = 'public/images/services/' . $name . '.' . 'png';                        
                        $service_image->save();
                    }
                    
                }
            }            

            # store prices
            foreach ($data as $price) {
                if(empty($price[0]) || empty($price[1]) ||  empty($price[2]))
                    continue;

                // get old service prices
                $servicePrices = Service_price::where('service_id', $service->id)->get();
                $old_prices = [];
                foreach ($servicePrices as $servicePrice) {
                    $arr = createDateRangeArray($servicePrice->start_date, $servicePrice->end_date);
                    foreach ($arr as $value) {
                        array_push($old_prices, $value);
                    }
                }
                #check with old prices
                $dates = createDateRangeArray($price[1], $price[2]);
                foreach ($dates as $value) {
                    if (in_array($value, $old_prices)) {

                        //if this is already taken
                        $msg = trans('api.usedPriceDates');
                        session()->put('error',$msg);
                        return redirect('/provider/edit-service/'. $service->id);
                    }
                }
                
                $service_price = new Service_price;
                $service_price->service_id  = $service->id;
                $service_price->price 		= $price[0];
                $service_price->start_date 	= $price[1];
                $service_price->end_date 	= $price[2];
                $service_price->save();
            	
            }
                
                $msg = trans('api.save');
                session()->put('success',$msg);
                return redirect('/show-service/'. $service->id);
                //return response()->json(['message' => $msg, 'status' => 1]);

        }else{
            $msg = $validator->errors()->first();
            session()->put('error',$msg);
            
            return redirect()->back()->withInput();
            //return response()->json(['message' => $msg, 'status' => 0]);
        }

    }

    # send noti to admin for add new service -> provider
    public function sendRequestToAdmin(Request $request){
        //dd($request->all());
        $user = Auth::user();
        sendNotify($user->id);
        $msg = trans('api.send');
        return response()->json(['message' => $msg, 'status' => 1]);
    }

    # edit services page -> provider
    public function EditService($id){        
        $service   = Service::find($id);
        $sections    = Section::activeSection();
        $countries   = Country::get();
        return view('site.provider.providerEditService',compact('service','sections','countries'));
    }

    # edit services post data -> provider
    public function ProviderEditService(Request $request){

        //dd($request->all());
        $user = Auth::user();

        /** If Not Active User **/
        if($user->checked == 0){
            $msg = trans('api.needActive');
            session()->put('error',$msg);
            return back();
        }
                
        $validator        = Validator::make($request->all(), [
			'title_ar'   	 => 'required|max:255',
			'title_en'   	 => 'required|max:255',
            'price'   		 => 'required',
			'lat'   		 => 'required|max:255',
			'country_id'   	 => 'required|exists:countries,id',
			'phone'			=> 'required|numeric|digits_between:7,20',
			'section_id'   	 => 'required|exists:sections,id',
			// 'city_id'   	 => 'required|exists:cities,id',
			'quantity'       => 'required|numeric|max:255',
			'desc_ar'   	 => 'required',
			'desc_en'   	 => 'required',
			'payment_method' => 'required',
            // 'options'   	 => 'required',
                        
			'prices'   		 => 'nullable',
			'from'   		 => 'nullable',
            'to'   		     => 'nullable',
            
            'service_id'   	 => 'required|exists:services,id',

            //'images'   		 => 'required|max:6',
        ],[
            //'images.max'=> App::isLocale('en')?'max images 6 images':'الحد الاقصى للصور 6 صور',
        ]);

        if ($validator->passes()) {

            # get specific prices for choosen days - mix 3 arrays in one
            $prices = request('prices');
            $from = request('from');
            $to = request('to');
            $data = [];

            foreach ($prices as $k => $v){
                $data[] = array();
            }
            foreach ($prices as $k => $v){
                array_push($data[$k],$v);
            }
            foreach ($from as $k => $v){
                array_push($data[$k],$v);
            }
            foreach ($to as $k => $v){
                array_push($data[$k],$v);
            }

            //dd($data);

            /***
             * now all  data in one array $data
             * foreach $data as price to log in it
             * price[0] => price
             * price[1] => price from
             * price[2] => price to
             ***/

            // get country code for phone use
            $code = Country::find($request['country_id'])->code;

            /** store Data **/
            $service = Service::find($request->service_id);

            /** If Not Owner  **/
            if($user->id != $service->user_id){
                return back();
            }

            $service->title_ar 		 = $request->title_ar;
            $service->title_en 		 = $request->title_en;
            $service->desc_ar 		 = $request->desc_ar;
            $service->desc_en 		 = $request->desc_en;
            $service->address_ar 	 = $request->address_ar;
            $service->address_en 	 = $request->address_en;
            $service->lat 			 = $request->lat;
            $service->lng 			 = $request->lng;
            $service->price 		 = $request->price;
            $service->main_phone 	 = $request->phone;
            $service->phone 		 = $code . $request->phone;
            $service->quantity 		 = $request->quantity;
            $service->user_id 		 = $user->id;
            $service->country_id 	 = $request->country_id;
            $service->section_id 	 = $request->section_id;        
            $service->payment_method = $request->payment_method;
            $service->save();


            //store images
            if($request->images && count($request->images) > 0){

                # if max number of images
                /*
                    $oldServiceImages = Service_image::where('service_id',$service->id)->count();
                    $newImages = count($request->images);
                    if($oldServiceImages + $newImages > 6){

                        $msg = App::isLocale('en')?'max images for one service is 6 images':'الحد الاقصى لمجموع الصور للخدمة 6 صور فقط يمكنك حذف صور قديمة واضافة مكانها ';
                        session()->put('error',$msg);
                        return back();
                    }
                */

                foreach($request->images as $photo){
                    if($photo != null){
                        // get image base64
                        $photo = substr($photo, strpos($photo, ",")+1,-1 );

                        $service_image = new Service_image;
                        $service_image->service_id = $service->id;
                        //$service_image->image = uploadImage($photo, 'public/images/services');

                        //base-64
                        $name 	=  time() . '_' . rand(11111, 99999);
                        $path   = 'public/images/services';
                        save_img($photo, $name, $path);
                        $service_image->image   	  = 'public/images/services/' . $name . '.' . 'png';
                        $service_image->save();
                    }

                }
            }


            //store prices
            if($request->prices && count($request->prices) > 0){

                foreach ($data as $price) {
                    if(empty($price[0]) || empty($price[1]) ||  empty($price[2]))
                        continue;

                    // get old service prices
                    $servicePrices = Service_price::where('service_id', $service->id)->get();
                    $old_prices = [];
                    foreach ($servicePrices as $servicePrice) {
                        $arr = createDateRangeArray($servicePrice->start_date, $servicePrice->end_date);
                        foreach ($arr as $value) {
                            array_push($old_prices, $value);
                        }
                    }

                    #check with old prices
                    $dates = createDateRangeArray($price[1], $price[2]);
                    foreach ($dates as $value) {
                        if (in_array($value, $old_prices)) {

                            //if this is already taken
                            $msg = trans('api.usedPriceDates');
                            session()->put('error',$msg);
                            return redirect()->back()->withInput();
                        }
                    }
                    //dd($old_prices);

                    $service_price = new Service_price;
                    $service_price->service_id  = $service->id;
                    $service_price->price 		= $price[0];
                    $service_price->start_date 	= $price[1];
                    $service_price->end_date 	= $price[2];
                    $service_price->save();

                }
            }
                
            $msg = trans('api.save');
            session()->put('success',$msg);
            return redirect('/show-service/'. $service->id);

        }else{
            $msg = $validator->errors()->first();
            session()->put('error',$msg);
            return redirect()->back();
            //return response()->json(['message' => $msg, 'status' => 0]);
        }

    }

    # delete service image from edit page
    public function ProviderDeleteImage($id){

        $image = Service_image::find($id);
        if(File::exists($image->image)) {
            File::delete($image->image);
        }
        $image->delete();

        $countImages = Service_image::where('service_id',$image->service_id)->count();


        $msg = trans('api.deleted');
        return response()->json(['message' => $msg, 'countServiceImages' => $countImages]);

    }

    # delete service price from edit page
    public function ProviderDeletePrice($id){

        $price = Service_price::find($id);
        $price->delete();

        $msg = trans('api.deleted');
        return response()->json(['message' => $msg]);

    }

    # delete service -> provider
    public function ProviderDeleteService($id){

        //delete service
        $service = Service::find($id);
        foreach($service->Images as $image){
            if(File::exists($image->image)) {
                File::delete($image->image);
            }
        }

        $service->delete();

        $msg = trans('api.deleted');
        return response()->json(['message' => $msg, 'status' => 1]);
    }

    # show provider notifications page
    public function ProviderNotifications(){
        $notifications = Notification::getUserNotification(Auth::id());
        foreach($notifications as $notification){
            $notification->seen = 1;
            $notification->save();
        }
        return view('site.provider.notifications',compact('notifications'));
    }

    # refuse order from notification page
    public function ProviderRefuseOrder($id){
        $order = Order::find($id);
        $order->status = 2;
        $order->save();
        $msg = 'order refused';
        return response()->json(['message' => $msg,'status'=>2]);
    }

    # accept order from notification page
    public function ProviderAcceptOrder($id){
        $order = Order::find($id);
        $order->status = 1;
        $order->save();
        $msg = 'order accepted';
        return response()->json(['message' => $msg,'status'=>1]);
    }

    # provider delete one notification
    public function ProviderDeleteNotification($id){

        $notify = Notification::find($id);
        $notify->delete();

        $notificationsCount = Notification::getUserNotification(Auth::id())->count();


        $msg = 'notification deleted';
        return response()->json(['message' => $msg,'count'=>$notificationsCount]);
    }

    # provider delete all notifications
    public function ProviderDeleteAllNotification(){

        $notifications = Notification::getUserNotification(Auth::id());

        foreach($notifications as $notification){
            $notification->delete();
        }

        $msg = 'all notifications deleted';
        return response()->json(['message' => $msg]);
    }


}