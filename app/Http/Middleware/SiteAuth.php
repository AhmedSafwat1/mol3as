<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SiteAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->checked == 1) {
            return $next($request);
        } else {
            $msg = trans('api.needLogin');
            if(Auth::check() && Auth::user()->checked == 0){
                $msg = trans('api.needActive');
            }
            session()->put('error',$msg);
            return redirect('/');
        }
    }
}
