<?php

use Illuminate\Support\Facades\Route;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\User;
use App\Models\Role;
use App\Models\AppSetting;
use App\Models\Report;
use App\Models\Social;
use App\Models\Notification;
use App\Models\Section;
use App\Models\Order;
use App\Models\City_day;
use App\Models\City;
use App\Models\Car;
use App\Models\Day;
use Carbon\Carbon;
use App\Models\Service_rate;
use App\Models\Rate;
use App\Models\Cut;
use App\Models\Service;
use App\Models\Order_option;
use App\Models\Dealer;
use App\Models\Favourite;
use App\Models\Room;
use App\Models\Room_chat;
use App\Models\User_day;
use App\Models\Offer;

#home
function Home()
{

    $colors = [
        '#8cc759', '#8c6daf', '#ef5d74', '#f9a74b', '#60beeb', '#fbef5a', '#FC600A', '#0247FE', '#FCCB1A',
        '#EA202C', '#448D76', '#AE0D7A', '#7FBD32', '#FD4D0C', '#66B032', '#091534', '#8601AF', '#C21460',
        '#FFA500', '#800080', '#008000', '#964B00', '#D2B48C', '#f5f5dc', '#4281A4', '#48A9A6',
    ];
    $home = [
        [
            'name' => 'الاعضاء',
            'count' => User::count() - 1,
            'icon' => '<i style="font-size: 90px;" class="fa fa-users"></i>',
            'color' => $colors[array_rand($colors)],
        ],
        [
            'name' => 'المشرفين',
            'count' => User::where('role', '>', 0)->count(),
            'icon' => '<i style="font-size: 90px;" class="fa fa-user-circle"></i>',
            'color' => $colors[array_rand($colors)],
        ],
        [
            'name' => 'التقارير',
            'count' => Report::count(),
            'icon' => '<i style="font-size: 90px" class="fa fa-flag-checkered"></i>',
            'color' => $colors[array_rand($colors)],
        ],
    ];

    return $blocks[] = $home;
}

function showDaysData($user,$title){
    $days = Day::get();
    $data = [];
    foreach($days as $i=>$day){
        $item = User_day::where('user_id',$user->id)->where('day_id',$day->id)->first();
        $data[$i]['id'] = $day->id;
        $data[$i]['title'] = $day->$title;
        $data[$i]['title_ar'] = $day->title_ar;
        $data[$i]['title_en'] = $day->title_en;
        $data[$i]['start_at'] = isset($item) ? (string) Carbon::parse($item->start_at)->format('h:i a') : '0';
        $data[$i]['end_at'] = isset($item) ? (string) Carbon::parse($item->end_at)->format('h:i a') : '0';
        $data[$i]['checked']  = isset($item) ? true : false;
    }
    return $data;
}

#convert phone to soudi arabia format
function convert_phone_to_sa_format($phone)
{
    $withoutZero  = ltrim($phone, '0');
    $filter_zero  = ltrim($withoutZero, '9660');
    $filter_code  = ltrim($filter_zero, '966');
    $full_phone   = '966' . $filter_code;
    return $full_phone;
}

#convert arabic number to english format
function convert2english($string)
{
    $newNumbers = range(0, 9);
    $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
    $string =  str_replace($arabic, $newNumbers, $string);
    return $string;
}

# set lang
function SetLang($lang)
{
    /** Set Lang **/
    $lang == 'en' ? App::setLocale('en') : App::setLocale('ar');
    /** Set Carbon Lang **/
    $lang == 'en' ? Carbon::setLocale('en') : Carbon::setLocale('ar');
}

#role name
function Role()
{
    $role = Role::findOrFail(Auth::user()->role);
    if (count($role) > 0) {
        return $role->role;
    } else {
        return 'عضو';
    }
}

#reports
function reports()
{
    $reports = Report::orderBy('id', 'desc')->take(8)->get();

    return $reports;
}

#report
function addReport($user_id, $event, $ip)
{
    if ($ip == "127.0.0.1") {
        $ip = "" . mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255);
    }

    $location = \Location::get($ip);
    $report = new Report;
    $user = User::findOrFail($user_id);
    if ($user->role > 0) {
        $report->user_id = $user->id;
        $report->event   = 'قام ' . $user->name . ' ' . $event;
        $report->supervisor = 1;
        $report->ip = $ip;
        // $report->country = (isset($location) && $location->countryCode != null) ? $location->countryCode : '';
        // $report->area = (isset($location) && $location->regionName != null) ? $location->regionName : '';
        // $report->city = (isset($location) && $location->cityName != null) ? $location->cityName : '';
        $report->save();
    } else {
        $report->user_id = $user->id;
        $report->event   = 'قام ' . $user->name . ' ' . $event;
        $report->supervisor = 0;
        $report->ip = $ip;
        // $report->country = (isset($location) && $location->countryName != null) ? $location->countryName : 'localhost';
        // $report->area = (isset($location) && $location->regionName != null) ? $location->regionName : 'localhost';
        // $report->city = (isset($location) && $location->cityName != null) ? $location->cityName : 'localhost';
        $report->save();
    }
}

#current route
function currentRoute()
{
    $routes = Route::getRoutes();
    foreach ($routes as $value) {
        if ($value->getName() === Route::currentRouteName()) {
            echo $value->getAction()['title'];
        }
    }
}

#check if unique
function is_unique($key, $value)
{
    $user                = User::where($key, $value)->first();
    if ($user) {
        return 1;
    }
    return 0;
}

#genrate random code
function generate_code()
{
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $token = '';
    $length = 6;
    for ($i = 0; $i < $length; $i++) {
        $token .= $characters[rand(0, $charactersLength - 1)];
    }
    return $token;
}

#get basic path
function appPath()
{
    return url('/');
}

#get data from app_settings DB
function settings($key)
{
    $setting = AppSetting::where('key', $key)->first();
    $value   = isset($setting) ? $setting->value : '';
    return $value;
}

#upload multi-part image 
function uploadImage($photo, $dir)
{
    #upload image
    if (!is_dir($dir))
        mkdir($dir, 0777);
    $name = date('d-m-y') . time() . rand() . '.' . $photo->getClientOriginalExtension();
    $photo->move($dir . '/', $name);
    return '/' . $dir . '/' . $name;
}

#upload multi-part image
function uploadAvatar($photo, $dir)
{
    #upload image
    if (!is_dir($dir))
        mkdir($dir, 0777);
    $name = date('d-m-y') . time() . rand() . '.' . $photo->getClientOriginalExtension();
    $photo->move($dir . '/', $name);
    return $name;
}

#upload image base64
function save_img($base64_img, $img_name, $path)
{
    $image = base64_decode($base64_img);
    $pathh = $_SERVER['DOCUMENT_ROOT'] . '/' . $path . '/' . $img_name . '.png';
    file_put_contents($pathh, $image);
}

// our sms package
function sendSms($mobileNumber, $messageContent)
{
    $user = '';
    $password = '';
    $sendername = '';
    $text = urlencode($messageContent);
    $to = '+' . $mobileNumber;
    // auth call
    //$url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=full";

    //لارجاع القيمه json
    $url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=json";
    // لارجاع القيمه xml
    //$url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=xml";
    // لارجاع القيمه string
    //$url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E";

    // Call API and get return message
    //fopen($url,"r");
    //return $url;
    $ret = file_get_contents($url);
    //echo nl2br($ret);
}

//zain package
function send_sms_zain($myphone, $active)
{

    $phones = $myphone;      // Should be like 966530007039

    $msg = urlencode($active . '   ');


    $link = "https://www.zain.im/index.php/api/sendsms/?user=user&pass=123456&to=$phones&message=$msg&sender=sender";

    /*
     *  return  para      can be     [ json , xml , text ]
     *  username  :  your username on safa-sms
     *  passwpord :  your password on safa-sms
     *  sender    :  your sender name
     *  numbers   :  list numbers delimited by ,     like    966530007039,966530007039,966530007039
     *  message   :  your message text
     */

    /*
     * 100   Success Number
     */

    if (function_exists('curl_init')) {
        $curl = @curl_init($link);
        @curl_setopt($curl, CURLOPT_HEADER, FALSE);
        @curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        @curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        @curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        $source = @curl_exec($curl);
        @curl_close($curl);
        if ($source) {
            return $source;
        } else {
            return @file_get_contents($link);
        }
    } else {
        return @file_get_contents($link);
    }
}

//mobily package
function send_mobile_sms_mobily($numbers, $msg)
{
    $url      = 'http://api.yamamah.com/SendSMS';
    $mobile   = '00966567477771';
    $password = '777711';
    $msg      = $msg;
    //$msg = urlencode($msg . '   ');
    $sender   = '0567477771';
    $sender   = urlencode($sender);
    $fields   = array(
        "Username"        => $mobile,
        "Password"        => $password,
        "Tagname"         => '0567477771',
        "Message"         => $msg,
        "RecepientNumber" => $numbers,
    );
    $fields_string = json_encode($fields);
    //open connection
    $ch = curl_init($url);
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => $fields_string
    ));

    $result = curl_exec($ch);
    curl_close($ch);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

#api response format
function apiResponse($key, $msg, $data = null, $anotherKey = [])
{
    $all_response['key'] = $key;
    $all_response['msg'] = $msg;
    if (!empty($anotherKey)) {
        foreach ($anotherKey as $key => $value) {
            $all_response[$key] = $value;
        }
    }
    if (!is_null($data)) $all_response['data'] = $data;
    return response()->json($all_response);
}

function userStatus($user)
{
    $status = 'active';
    if ($user->checked == 0) $status = 'blocked';
    if ($user->activation == 0) $status = 'non-active';
    return $status;
}

#show order status msg
function showStatusMsg($status)
{
    $status_msgs = [
        '0' => trans('api.newOrder'),
        '1' => trans('api.agreeOrder'),
        '2' => trans('api.refusedOrder'),
        '3' => trans('api.deleteOrderByClient'),
        '4' => trans('api.deleteOrderByProvider'),
        '5' => trans('api.finishOrder'),
        '6' => trans('api.delayedOrder'),
    ];

    return $status_msgs[$status];
}

#convert from arabic day to english format
function convertDayFromEnglishToArabic($day)
{
    $dayArray = [
        'Saturday'  => 'السبت',
        'saturday'  => 'السبت',
        'Sunday'    => 'الاحد',
        'sunday'    => 'الاحد',
        'Monday'    => 'الاثنين',
        'monday'    => 'الاثنين',
        'Tuesday'   => 'الثلاثاء',
        'tuesday'   => 'الثلاثاء',
        'Wednesday' => 'الاربعاء',
        'wednesday' => 'الاربعاء',
        'Thursday'  => 'الخميس',
        'thursday'  => 'الخميس',
        'Friday'    => 'الجمعة',
        'friday'    => 'الجمعة',
    ];

    return isset($dayArray[$day]) ? $dayArray[$day] : '';
}

#show day by date
function showDay($start_date, $lang = 'ar')
{
    $englishDay = date('l', strtotime($start_date));
    $arabicDay  = convertDayFromEnglishToArabic($englishDay);
    return $lang == 'en' ? $englishDay : $arabicDay;
}

#send FCM
function Send_FCM_Badge($device_id, $all_data, $type, $setBadge = 0)
{
    $priority = 'high'; // or 'normal'

    $optionBuilder = new OptionsBuilder();
    $optionBuilder->setTimeToLive(60 * 20);
    $optionBuilder->setPriority($priority);
    $notificationBuilder = new PayloadNotificationBuilder($all_data['title']);
    $notificationBuilder->setBody($all_data['msg'])->setSound('default');
    //    $notificationBuilder->setBody($all_data['message'])->setSound('default')->setBadge($setBadge);

    $option = $optionBuilder->build();
    $notification = $notificationBuilder->build();
    $dataBuilder = new PayloadDataBuilder();
    $dataBuilder->addData($all_data);

    $data = $dataBuilder->build();
    $token = $device_id;
    if ($token) {

        if ($type == 'ios') {
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        } else {
            $downstreamResponse = FCM::sendTo($token, $option, null, $data);
        }

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();

        $downstreamResponse->numberModification();
    }
}

#remove html tags
function fixText($text)
{
    $text = str_ireplace(array("\r", "\n", "\t"), '', $text);
    $text = str_ireplace(array("&nbsp;", "&hellip;", "&ndash;"), '', $text);
    $text = strip_tags($text);
    $text = stripslashes($text);
    return $text;
}

#get social links
function social($key)
{
    $social   = Social::where('site_name', $key)->first();
    $value    = isset($social) ? $social->url : '';
    return $value;
}

#get date range from start_date to end_date
function createDateRangeArray($strDateFrom, $strDateTo)
{
    $aryRange = array();

    $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2),     substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
    $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2),     substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

    if ($iDateTo >= $iDateFrom) {
        array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
        while ($iDateFrom < $iDateTo) {
            $iDateFrom += 86400; // add 24 hours
            array_push($aryRange, date('Y-m-d', $iDateFrom));
        }
    }
    return $aryRange;
}

#send notification
function sendNotify($from_id = null, $to_id, $message_ar, $message_en, $order_id = null)
{
    $notify = new Notification;
    $notify->from_id    = $from_id;
    $notify->to_id      = $to_id;
    $notify->message_ar = $message_ar;
    $notify->message_en = $message_ar;
    $notify->order_id   = $order_id;
    $notify->type       = !is_null($order_id) ? 'order' : 'admin';
    $notify->seen       = 0;
    $notify->save();
}

#check if value in key,value array
function is_in_array($array, $key, $key_value)
{

    $within_array = false;
    $result = [];
    foreach ($array as $k => $v) {

        if (is_array($v)) {
            $within_array = is_in_array($v, $key, $key_value);
            if ($within_array == true) {
                break;
            }
        } else {
            if ($v == $key_value && $k == $key) {
                $within_array = true;
                break;
            }
        }
    }
    return $within_array;
}

#orders count
function getOrdersCount($service_id, $date)
{
    $orders = Order::where('service_id', $service_id)->where('status', '<', '2')->get();
    $total = 0;
    foreach ($orders as $order) {
        $arr = createDateRangeArray($order->start_date, $order->end_date);
        foreach ($arr as $value) {
            if ($date == $value)
                $total++;
        }
    }

    return $total;
}

# get avaiable next days_id to city - return as a array
function getDays($city_id, $day_id)
{
    if ($day_id <= 6) { //if today before friday for this city
        $available_in_arr =  City_day::with('Day')->where('city_id', $city_id)
            ->where('day_id', '>', $day_id)
            ->pluck('id')->toArray();

        return $available_in_arr;
    }

    //else get all days for this city
    $available_in_arr =  City_day::with('Day')->where('city_id', $city_id)
        ->pluck('id')->toArray();

    return $available_in_arr;
}

#service Rate average
function serviceRate($service_id)
{
    return round(Service_rate::where('service_id', $service_id)->avg('rate'));
}

#user Rate average
function userRate($user_id)
{
    return round(Rate::where('to_id', $user_id)->avg('rate'));
}

#user Rate average
function getUserRate($user)
{
    return round($user->Rates->avg('rate'));
}

#all city
function getCity()
{
    return City::orderBy('title_ar', 'asc')->get();
}

#all sections
function getSection()
{
    return Section::orderBy('title_ar', 'asc')->get();
}

#all sections
function getService()
{
    return Service::orderBy('title_ar', 'asc')->get();
}

#all providers
function getProvider()
{
    return User::where('provider', '1')->orderBy('name', 'asc')->get();
}

#all dealers
function getDealer()
{
    return Dealer::orderBy('name', 'asc')->get();
}

function getProviders($city_id = null)
{
    return User::where('provider', '1')->where('city_id', $city_id)->orderBy('name', 'asc')->get();
}


#all cuts
function getCut()
{
    return Cut::orderBy('title_ar', 'asc')->get();
}

#all Day
function getDay()
{
    return Day::get();
}

function notify_count($user_id)
{
    return Room_chat::where('to_id', $user_id)->where('seen', 0)->count();
}

function hasFavourite($user_id, $doctor_id)
{
    $favourite = Favourite::where('user_id', $user_id)->where('doctor_id', $doctor_id)->first();
    return isset($favourite);
}

function showDaysDesc($user_id, $lang = 'ar',$color = '00ADBA')
{
    $user               = User::whereId($user_id)->first();
    $title              = $lang == 'en' ? 'title_en' : 'title_ar';

    $days_avaliable     = $user->Days->pluck('day_id')->toArray();
    $days_not_avaliable = Day::whereNotIn('id', $days_avaliable)->pluck($title)->toArray();
    $day_desc           = '';
    foreach ($days_not_avaliable as $day) {
        $day_desc = $day_desc . '- ' . $day . ' ';
    }
    $day_desc_without_first_dash = ltrim($day_desc, '-');
    $day_desc_without_first_dash = '( ' . $day_desc_without_first_dash . ' )';

    $desc               = $lang == 'en' ? 'Every Day' : 'كل يوم ';
    if ($lang == 'en')
        $except         = count($days_not_avaliable) <= 0 ? ' ' : ' except ';
    else
        $except         = count($days_not_avaliable) <= 0  ? ' ' : ' ما عدا ';

    $full_desc          = '<p>' . $desc . ' ' . $except . ' <span style="color:'. $color .'">' . $day_desc_without_first_dash . '</span></p>';
    return $full_desc;
}

function showDatesDesc($user_id, $lang = 'ar',$color = '00ADBA')
{
    $user                       = User::whereId($user_id)->first();
    $title                      = $lang == 'en' ? 'title_en' : 'title_ar';

    $days_has_special_time      = User_day::where('user_id', $user_id)->where('type', 1)->get();
    $dates_desc           = '';
    foreach ($days_has_special_time as $user_day) {
        $time       = $lang == 'en' ? ' from ' . ' <span style="color:'. $color .'">' . Carbon::parse($user_day->start_at)->format('h:i a') . '</span>' . ' to ' . ' <span style="color:'. $color .'">' . Carbon::parse($user_day->end_at)->format('h:i a') . '</span>'
                                    : ' من ' . ' <span style="color:'. $color .'">' . Carbon::parse($user_day->start_at)->format('h:i a') . '</span>' . ' الى ' . ' <span style="color:'. $color .'">' . Carbon::parse($user_day->end_at)->format('h:i a') . '</span>';
        $dates_desc = $dates_desc . '- ' . $user_day->Day->$title . ' ' . $time . ' ';
    }
    $dates_desc_without_first_dash = ltrim($dates_desc, '-');

    $desc               = $lang == 'en' ? 'Every Day from ' . ' <span style="color:'. $color .'">' . Carbon::parse($user->start_at)->format('h:i a') . '</span>' . ' to ' . ' <span style="color:'. $color .'">' . Carbon::parse($user->end_at)->format('h:i a') . '</span>'
                                        : 'يوميا من ' . ' <span style="color:'. $color .'">' . Carbon::parse($user->start_at)->format('h:i a') . '</span>' . ' الى ' . ' <span style="color:'. $color .'">' . Carbon::parse($user->end_at)->format('h:i a') . '</span>';
    if ($lang == 'en')
        $except         = is_null($dates_desc_without_first_dash) ? ' ' : ' except ';
    else
        $except         = is_null($dates_desc_without_first_dash) ? ' ' : ' ما عدا ';

    $full_desc          = '<p>' . $desc . ' ' . $except . ' <span style="color:'. $color .'">' . $dates_desc_without_first_dash . '</span></p>';
    return $full_desc;
}

function lastMsg($room_id)
{
    $room           = Room::whereId($room_id)->first();
    $last_chat      = $room->Chats->first();
    $last_message   = $last_chat->type == 'image' ? url('' . $last_chat->message) : (string)  $last_chat->message;
    return $last_message;
}

function has_offer($id){
    $offer = Offer::where('user_id',$id)->first();
    return isset($offer);
}
