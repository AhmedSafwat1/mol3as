(function () {

    $(window).on('scroll',function() {
        var scroll = $(this).scrollTop();
        
        if(scroll >= 10) {
            $('header').addClass('affix');
            $('.mob-collaps').css('bottom' , '0');
        } else {
            $('header').removeClass('affix');
            $('.mob-collaps').removeAttr('style');
        }
    });

    // Start Loader

    $(window).on('load',function () {

        $("body").css("overflow-y", "auto");

        $(".loader").fadeOut();

        $(".loading").fadeOut().css({
            'transituin-delay' : '1s'
        });

        $(".wave-1").addClass("left");

        $(".wave-2").addClass("right");

    });

    // Start Navbar 

    $('.overlay').fadeOut();
    
    $(".mob-collaps").click(function() {
        $(this).parent().find(".nav-links > ul").toggleClass('nav-open');

        $('.overlay').fadeToggle();

        $(this).find("span:first-child").toggleClass('rotate');
        $(this).find("span:nth-child(2)").toggleClass('none');
        $(this).find("span:nth-child(3)").toggleClass('rotate2');
    });

    $(".overlay").click(function() {
        $(".nav-links ul").removeClass('nav-open');
        $(this).fadeOut();

        $(".mob-collaps span:first-child").removeClass('rotate');
        $(".mob-collaps span:nth-child(2)").removeClass('none');
        $(".mob-collaps span:nth-child(3)").removeClass('rotate2');
    });

    // Start Owl Carousel

    $('#index-slider.owl-carousel').owlCarousel({
        rtl: true,
        items: 1,
        loop:true,
        dots: true,
        autoplay: true,
        animateOut: 'slideOutDown',
        animateIn: 'flipInX'
    });

    // Start Services Slider

    $('.services-slider.owl-carousel').owlCarousel({
        rtl: true,
        items: 1,
        loop:true,
        dots: true,
        autoplay: true
    });

    // Start Services Slider

    $('.bottom-slider.owl-carousel').owlCarousel({
        rtl: true,
        items: 1,
        loop:true,
        dots: true,
        autoplay: true
    });

    // Start Mobile Profile Links

    $('.prof-links').on('click', function(){
        $(this).parents('body').find('.profile-links').css({
            'transform' : 'scaleY(1)'
        });

        $('.N-close').on('click' , function(){
            $(this).parent().removeAttr('style');
        });
    });

    // Start N-Slider

    $('#owl-hash-URL-2.owl-carousel').owlCarousel({
        items:1,
        loop:true,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        URLhashListener:true,
        autoplayHoverPause:true,
        startPosition: 'URLHash'
    });

    $('.owl-hash-nav-2 li.item ').click(function () {

        $(this).addClass('active').siblings().removeClass('active');

    });

    // Start Add Favourite Button

    // $(".buttons-serv span").on("click" , function(){
    //     $(this).addClass('like').html('<i class="fas fa-heart"></i>');
    // });

    // Start Favourite Remove Icon

    $('.accept-remove').on('click', function(){
        $(this).parents('.favourites').fadeOut();
    });

    // Remove All
    $('.no-noty-alert').fadeOut();

    // $('.removeAll').on('click', function(){
    //     $(this).parents('.N-noty-all').fadeOut(function(){
    //         $('.no-noty-alert').fadeIn();
    //     });
    // });

    $('.accept-remove-all').on('click', function() {
        $(this).parents('.N-noty-all').fadeOut(function(){
            $('.no-noty-alert').fadeIn();
        });
    });


    $(window).on('load',function(){
        new WOW().init();
    });

    // Start Office Images
    $('.office-img i').click(function() {
        $(this).parents('.office-img').fadeOut();
    });

    // Start  Add Specify Price
    $('.add-price').on('click', function(){
        $('.specify-price').append('<div class="row specify-price-content">' +
            '<div class="col-md-4 col-sm-12">'+
                '<div class="form-group">'+
                    // '<label>@lang(\'site.priceInDate\')</label>'+
                    '<input type="number" class="form-control"  value="" name="prices[]">'+
                '</div>'+
            '</div>'+

            '<div class="col-md-4 col-sm-6">'+
                '<div class="form-group">'+
                    // '<label>@lang(\'site.from\')</label>'+
                    '<input type="date" name="from[]" class="form-control startDate"  value="">'+
                '</div>'+
            '</div>'+

            '<div class="col-md-4 col-sm-6">'+
                '<div class="form-group">'+
                    // '<label>@lang(\'site.to\')</label>'+
                    '<input type="date" name="to[]" class="form-control endDate"  value="">'+
                '</div>'+
            '</div>'+

            '<div class="close-btn">'+
                '<i class="fas fa-times"></i>'+
            '</div>'+
        '</div>');

        $('.close-btn').on('click', function(){
            $(this).parent().remove();
        });
    });

    // See More

    $('.p-more').hide();
    if($('.details p').height() > 150) {
        $('.p-more').show();
    }
    $('.p-more').on('click', function (e) {
        e.preventDefault();
        $(this).parent().find('p').toggleClass('p-show');
    });

})(jQuery);

// Start Select Images

$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        // max images in edit 6 images if more stop add image div - stop for now
        // var oldImages = $('.oldImages').length
        // var newImages = $('.image').length
        // console.log(newImages + oldImages);
        // if((newImages + oldImages ) < 6){

            if (input.files) {


                var filesAmount = input.files.length;


                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        var image = document.createElement('img');
                        image.setAttribute('src',event.target.result);
                        var body = document.createElement('div');
                        // var input  = '<input name="images[]" multiple type="hidden">';
                        var button  = document.createElement('button');



                        var input = document.createElement('input');
                        input.setAttribute('name', 'images[]');
                        input.setAttribute('type', 'hidden');
                        input.setAttribute('class', 'image');
                        input.setAttribute('value', event.target.result);

                        button.setAttribute('class','close');
                        // button.setAttribute('type','button');
                        button.innerHTML = '<i class="fas fa-minus-square"></i>';
                        body.appendChild(image);
                        body.appendChild(input);
                        body.appendChild(button);

                        body.setAttribute('class','images');


                        //console.log(body);
                        $('.gallery').append(body);

                        ($($.parseHTML(body)).appendTo(placeToInsertImagePreview));
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }
        // }else{
            // var msg = 'الحد الاقصي 6 صور - max 6 images';
            // toastr.info(msg);
        // }


    };

    $(document).on('click', '.close', function(event){
            event.preventDefault();
            $(this).parent().remove();
    });

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});